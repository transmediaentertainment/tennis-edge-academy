using UnityEngine;
using System.Collections;
using TransTech.Delegates.Unity;

public class ActionPanelEvents : MonoBehaviour 
{
    public static event Vector2Delegate PanelDragEvent;

    void OnDrag(Vector2 delta)
    {
        if (PanelDragEvent != null)
            PanelDragEvent(delta); 
    }
}
