using UnityEngine;
using MotionEdge.Language;

namespace MotionEdge
{
    public enum IPhoneGen
    {
        Third,
        Fourth,
        Fifth
    }

    public class SystemSettings
    {
        public bool FirstRun { get; set; }

        private static SystemSettings m_Instance;

        public static SystemSettings Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new SystemSettings();
                }
                return m_Instance;
            }
        }

        private SystemSettings()
        {
            FirstRun = true;
            
            Screen.autorotateToLandscapeLeft = true;
            Screen.autorotateToLandscapeRight = true;
            Screen.autorotateToPortrait = false;
            Screen.autorotateToPortraitUpsideDown = false;
            Screen.orientation = ScreenOrientation.AutoRotation;
        }
    }
}

