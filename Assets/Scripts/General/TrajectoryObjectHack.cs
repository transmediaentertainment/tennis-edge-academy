﻿using UnityEngine;

public class TrajectoryObjectHack : MonoBehaviour
{
    private Vector3 distantPosition = new Vector3(0f, 0f, 1000f);
    
    void Update()
    {
        transform.LookAt(distantPosition); // Apply the world space orientation.
    }
}