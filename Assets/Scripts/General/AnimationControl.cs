using UnityEngine;
//using System.Collections;
using TransTech.System.Debug;
using MotionEdge.UI;
using MotionEdge.UI.Engines;
using MotionEdge.Metadata;
using System;

namespace MotionEdge.Controls
{
    public class AnimationControl : MonoBehaviour
    {
        [SerializeField]
        private string[] m_AnimationList;

        [SerializeField]
        private CameraControl m_CameraControl;

        [SerializeField]
        private UIController m_UIController;

        [SerializeField]
        private string m_AnimationPrefix;

        private string m_CurrentGlobalAnimation = "";
        private string m_CurrentLocalAnimation = "";

        public string CurrentAnimation
        {
            get
            {
                return m_CurrentGlobalAnimation;
            }
            set
            {
                m_CurrentGlobalAnimation = value;
                m_CurrentLocalAnimation = m_AnimationPrefix + value;
            }
        }
        public float CurrentAnimationLength { get { return animation[m_CurrentLocalAnimation].length; } }

        private const int AnimationCount = 15;

        private const float DefaultSpeed = 1f;
        private const float SlowMotionSpeed = 0.33f;
        private const float PauseSpeed = 0f;
        public float CurrentSpeed { get; private set; }

        public const float SlowTime = 0.25f;
        //private float m_StartSlowSpeed;
        private float m_SlowTimer;
        private bool m_IsSlowing;

        public float NormalizedTime { get { return animation[m_CurrentLocalAnimation].normalizedTime; } }

        public event Action<AnimationControl> SlowAnimationFinishedEvent;

        private void Awake()
        {
            var keys = MetaDataController.Instance.GetOrderedDataKeys(); 
            CurrentAnimation = keys[0];

            SpeedControl.Instance.SpeedChangedEvent += SpeedChanged;
            m_UIController.ActionButtonPressedEvent += SetAnimation;
        }

        private void OnDestroy()
        {
            SpeedControl.Instance.SpeedChangedEvent -= SpeedChanged;
        }

        public void SetAnimation(string anim)
        {
            if (anim == null)
            {
                TTDebug.LogError("AnimationControl.cs : No string passed in");
                return;
            }

            //if (anim == CurrentAnimation)
            //{
                // Already on this animation
            //    return;
            //}

            CurrentAnimation = anim;
            m_CameraControl.FadeOut();
            RegisterForFadeOut();

            //animation.CrossFade(m_AnimationList[CurrentAnimation], 0.01f);
        }

        public void RestartAnimation()
        {
            m_CameraControl.FadeOut();
            RegisterForFadeOut();
        }

        private bool m_IsRegistered = false;

        private void RegisterForFadeOut()
        {
            if (!m_IsRegistered)
            {
                CameraFadeFSM.FadeOutCompleteEvent += RewindAndResetParameters;
                m_IsRegistered = true;
            }
        }

        private void Update()
        {
            if (animation[m_CurrentLocalAnimation].time >= animation[m_CurrentLocalAnimation].length - (CameraControl.CameraFadeDuration * SpeedControl.Instance.Speed))
            {
                RestartAnimation();
            }
        }

        private void RewindAndResetParameters()
        {
            animation.Play(m_CurrentLocalAnimation);
            animation.Rewind();
            CurrentSpeed = SpeedControl.Instance.Speed;
            animation[m_CurrentLocalAnimation].speed = CurrentSpeed;
            transform.position = MetaDataController.Instance.GetPosition(m_CurrentGlobalAnimation);
            transform.rotation = Quaternion.Euler(MetaDataController.Instance.GetRotation(m_CurrentGlobalAnimation));
            //animation.CrossFade(m_AnimationList[CurrentAnimation], 0.01f);
            //animation.Rewind();
            //m_CameraControl.FadeIn();
            CameraFadeFSM.FadeOutCompleteEvent -= RewindAndResetParameters;
            m_IsRegistered = false;
        }

        public void SetNormalizedAnimationTime(float time)
        {
            Debug.Log("Setting normalized time current: " + animation[m_CurrentLocalAnimation].normalizedTime + " : new : " + time);
            animation[m_CurrentLocalAnimation].normalizedTime = Mathf.Clamp01(time);
        }

        private void SpeedChanged(float newSpeed)
        {
            if (newSpeed <= 0.01f)
                Pause();
            else if (newSpeed >= 0.99f)
                PlayFullSpeed();
            else
                PlaySlowMo();
        }

        public void PlayFullSpeed()
        {
            // We need to cancel any slow down.  If we don't, the slow down can override the new speed setting.
            //CancelSlowAnimation();
            CurrentSpeed = SpeedControl.Instance.Speed;
            animation[m_CurrentLocalAnimation].speed = CurrentSpeed;
        }

        public void PlaySlowMo()
        {
			
            // We need to cancel any slow down.  If we don't, the slow down can override the new speed setting.
            //CancelSlowAnimation();
            CurrentSpeed = SpeedControl.Instance.Speed;
            animation[m_CurrentLocalAnimation].speed = CurrentSpeed;
        }

        public void Pause()
        {
            //CancelSlowAnimation();
            CurrentSpeed = SpeedControl.Instance.Speed;
            animation[m_CurrentLocalAnimation].speed = CurrentSpeed;
            // This is for the nice slow down, however requires more work to get precise - James Murchison
            /*if (m_IsSlowing)
                return;
            CurrentSpeed = animation[m_CurrentLocalAnimation].speed;
            m_StartSlowSpeed = CurrentSpeed;
            m_SlowTimer = 0f;
            m_IsSlowing = true;
            SystemEventCenter.Instance.UpdateEvent += SlowAnimation;*/
        }
		
		/*
        private void SlowAnimation(float deltaTime)
        {
            m_SlowTimer += deltaTime;
            CurrentSpeed = Mathf.Lerp(m_StartSlowSpeed, PauseSpeed, Mathf.Clamp01(m_SlowTimer / SlowTime));
            animation[m_CurrentLocalAnimation].speed = CurrentSpeed;
            Debug.Log("Slow Time : " + animation[m_CurrentLocalAnimation].normalizedTime + " : " + CurrentSpeed);
            if (m_SlowTimer >= SlowTime)
            {
                CancelSlowAnimation();
                if (SlowAnimationFinishedEvent != null)
                    SlowAnimationFinishedEvent(this);
            }
        }

        private void CancelSlowAnimation()
        {
            if (!m_IsSlowing)
                return;

            m_IsSlowing = false;
            SystemEventCenter.Instance.UpdateEvent -= SlowAnimation;
        }
        */
    }
}
