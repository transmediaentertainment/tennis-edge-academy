using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using TransTech.System.Debug;
using UnityExtensions;
using MotionEdge.Language;
using System;

namespace MotionEdge.Metadata
{
    public class MetaDataController : MonoSingleton<MetaDataController>
    {
        private const float TriggerTimeThreshold = 0.0005f;
        private Dictionary<string, MoveMetadata> m_Data = new Dictionary<string, MoveMetadata>();

        public override void Init()
        {
            base.Init();
            LoadData();
        }

        public void LoadData()
        {
            if (m_Data.Count > 0)
            {
                m_Data.Clear();
            }
            // Load up the data files
            var dataFiles = Resources.LoadAll("MoveDataSets", typeof(TextAsset));
			Debug.Log("Data Files : " + dataFiles.Length);
            var serializer = new XmlSerializer(typeof(MoveMetadata));
            for (int i = 0; i < dataFiles.Length; i++)
            {
                // In editor we are getting the meta files as well, so we need to check it hasn't been loaded already.
                if (m_Data.ContainsKey(dataFiles[i].name))
                    continue;
				//Debug.Log("Data File : " + dataFiles[i].name);
                string encryptedString = (((TextAsset)dataFiles[i]).text);

                // We only want to be able to read in unencrypted files in the editor!
                // Don't want a hacker to work out how to put in unencrypted files. - JM
#if UNITY_EDITOR
                string xmlString;
                try
                {
                    xmlString = Encryption.Decrypt(encryptedString, Encryption.m_Salt);
					//Debug.Log("XML String : " + xmlString);
                }
                catch (Exception e)
                {
                    // Was already decrypted
                    Debug.Log("Caught Exception : " + e.Message);
                    xmlString = encryptedString;
                }
#else
                string xmlString = Encryption.Decrypt(encryptedString, Encryption.m_Salt);
#endif
                using (var sr = new StringReader(xmlString))
                {
					try
					{
	                    MoveMetadata move = (MoveMetadata)serializer.Deserialize(sr);
	                    m_Data.Add(dataFiles[i].name, move);
					}
					catch(Exception e)
					{
						Debug.LogWarning("Error deserializing : " + e.Message);
					}
					finally
					{
                    	sr.Close();
					}
                }
            }
        }

        public MoveMetadata GetData(string dataName)
        {
            if (!m_Data.ContainsKey(dataName))
            {
                TTDebug.LogError("No data found for key : " + dataName);
                return null;
            }

            return m_Data[dataName];
        }

        public Vector3 GetPosition(string animName)
        {
            var data = m_Data[animName];
            return data.Position;
        }

        public Vector3 GetRotation(string animName)
        {
            var data = m_Data[animName];
            return data.Rotation;
        }

        public string GetLocalizedName(string animName)
        {
            var data = m_Data[animName];
            switch (LanguageController.Instance.CurrentLanguage)
            {
                case LanguageType.EnglishUK:
                case LanguageType.EnglishUS:
                    return data.EnglishName;
                case LanguageType.French:
                    return data.FrenchName;
                case LanguageType.Spanish:
                    return data.SpanishName;
                default:
                    return data.EnglishName;
			}
        }

        public string GetLocalizedOverview(string animName)
        {
            var data = m_Data[animName];
            switch (LanguageController.Instance.CurrentLanguage)
            {
                case LanguageType.EnglishUK:
                case LanguageType.EnglishUS:
                    return data.EnglishOverviewInfo;
                case LanguageType.French:
                    return data.FrenchOverviewInfo;
                case LanguageType.Spanish:
                    return data.SpanishOverviewInfo;
                default:
                    return data.EnglishOverviewInfo;
            }
        }

        public string GetLocalizedTips(string animName)
        {
            var data = m_Data[animName];
            switch (LanguageController.Instance.CurrentLanguage)
            {
                case LanguageType.EnglishUK:
                case LanguageType.EnglishUS:
                    return data.EnglishTipsInfo;
                case LanguageType.French:
                    return data.FrenchTipsInfo;
                case LanguageType.Spanish:
                    return data.SpanishTipsInfo;
                default:
                    return data.EnglishTipsInfo;
            }
        }

        public CurrentNodeType[] GetNodesForAnimationAtTime(string animName, float time)
        {
            var data = m_Data[animName];
            for (int i = 0; i < data.TriggerTimes.Length; i++)
            {
                if (data.TriggerTimes[i].IsApproximately(time, TriggerTimeThreshold))
                {
                    return data.Nodes[i].ToArray();
                }
            }
            TTDebug.LogError("Didn't find node type : " + animName + " : " + time);
            return new CurrentNodeType[] { CurrentNodeType.None };
        }

        public string[] GetStepTechniques(string animName, float time)
        {
            var data = m_Data[animName];
            for (int i = 0; i < data.TriggerTimes.Length; i++)
            {
                if (data.TriggerTimes[i].IsApproximately(time, TriggerTimeThreshold))
                {
                    switch (LanguageController.Instance.CurrentLanguage)
                    {
                        case LanguageType.EnglishUK:
                        case LanguageType.EnglishUS:
                            return data.EnglishInfo[i].ToArray();
                        case LanguageType.Spanish:
                            return data.SpanishInfo[i].ToArray();
                        case LanguageType.French:
                            return data.FrenchInfo[i].ToArray();
                        default:
                            TTDebug.LogError("MetaDataController.cs : Unsupported language : " + LanguageController.Instance.CurrentLanguage);
                            return new string[] { "ERROR GETTING TECHNIQUE" };
                    }
                }
            }

            return new string[] { "Couldn't find matchine technique step" };
        }

        public GripType[] GetStepGrips(string animName, float time)
        {
            var data = m_Data[animName];
            for (int i = 0; i < data.TriggerTimes.Length; i++)
            {
                if(data.TriggerTimes[i].IsApproximately(time, TriggerTimeThreshold))
                {
                    if (data.Grips[i].Count > 0)
                        return data.Grips[i].ToArray();
                    else
                        return null;
                }
            }
            return null;
        }

        public string[] GetOrderedDataKeys()
        {
            var keys = new string[m_Data.Keys.Count];
            // We can't use Linq style querying here as it causes runtime JIT issues on iOS
            var orderedList = new List<KeyValuePair<string, MoveMetadata>>();
			foreach(var kvp in m_Data)
			{
				orderedList.Add(kvp);
			}
            orderedList.Sort((firstPair, nextPair) =>
                {
                    return firstPair.Value.MoveOrder.CompareTo(nextPair.Value.MoveOrder);
                }
            );

            for (int i = 0; i < keys.Length; i++)
            {
                keys[i] = orderedList[i].Key;
            }
            //m_Data.Keys.CopyTo(keys, 0);
            return keys;
        }

        private const string HelpInfoKey = "HelpInfo";

        public string GetHelpText()
        {
            return LanguageController.Instance.GetValueForKey(HelpInfoKey);
        }

    }
}
