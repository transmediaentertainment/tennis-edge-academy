using System;
using System.IO;
using MotionEdge;
using MotionEdge.Language;
using MotionEdge.Metadata;
using TransTech.System.Debug;
using UnityEngine;
using UnityExtensions;

public class Instruction
{
    public float Time { get; set; }
    public string Text { get; set; }
    public string TextFrench { get; set; }
    public string TextDutch { get; set; }
    public string TextSpanish { get; set; }
    public string TextPortugese { get; set; }

    public CurrentNodeType Node { get; set; }

    public bool IsLast { get; set; }
}

public class MetaData : MonoBehaviour
{
    public TextAsset[] m_TextBank;
    public TextAsset[] m_TimeBank;
    public TextAsset[] m_NodeBank;

    public TextAsset[] m_TextBankTechniques;

    public TextAsset[] m_TextBankFrench;
    public TextAsset[] m_TextBankDutch;
    public TextAsset[] m_TextBankSpanish;
    public TextAsset[] m_TextBankPortugese;

    public TextAsset[] m_TextHelp;

    private const int MaxInstructions = 10;
    private const int MaxTechniques = 16;

    private string[] m_SeriesTechnique;
    private Instruction[,] m_Series;
    private int[] m_AmountInstructions = new int[MaxTechniques];


    void Awake()
    {
        m_SeriesTechnique = new string[MaxTechniques];
        m_Series = new Instruction[MaxInstructions, MaxTechniques];
        m_AmountInstructions = new int[MaxTechniques];

        for (int i = 0; i < MaxTechniques; i++)
        {
            using (StringReader readerText = new StringReader(m_TextBank[i].text),
                readerTextFrench = new StringReader(m_TextBankFrench[i].text),
                readerTextDutch = new StringReader(m_TextBankDutch[i].text),
                readerTextSpanish = new StringReader(m_TextBankSpanish[i].text),
                readerTextPortugese = new StringReader(m_TextBankPortugese[i].text),
                readerTime = new StringReader(m_TimeBank[i].text),
                readerNode = new StringReader(m_NodeBank[i].text),
                readerTextTechniques = new StringReader(m_TextBankTechniques[i].text))
            {
                m_SeriesTechnique[i] = readerTextTechniques.ReadLine();

                for (int j = 0; j < MaxInstructions; j++)
                {
                    var text = readerText.ReadLine();

                    if (text == null)
                    {
                        m_AmountInstructions[i] = j;
                        break;
                    }
                    m_Series[j, i] = new Instruction();

                    m_Series[j, i].Text = text;
                    m_Series[j, i].TextFrench = readerTextFrench.ReadLine();
                    m_Series[j, i].TextDutch = readerTextDutch.ReadLine();
                    m_Series[j, i].TextSpanish = readerTextSpanish.ReadLine();
                    m_Series[j, i].TextPortugese = readerTextPortugese.ReadLine();

                    var time = readerTime.ReadLine();
                    var node = readerNode.ReadLine();
                    float timeFloat;
                    if (time != null && time != "")
                    {
                        if (!float.TryParse(time, out timeFloat))
                        {
                            TTDebug.LogError("MetaData.cs : Couldn't Parse float correctly : " + time);
                        }
                        else
                        {
                            m_Series[j, i].Time = timeFloat;
                        }
                    }

                    if (node != null && node != "")
                    {
                        try
                        {
                            m_Series[j, i].Node = (CurrentNodeType)Enum.Parse(typeof(CurrentNodeType), node);
                        }
                        catch (Exception e)
                        {
                            TTDebug.LogError("MetaData.cs : Couldn't Parse enum correctly : " + node + " : Error : " + e.Message);
                        }
                    }
                }
            }
        }
    }


    public string GetTechniqueStep(int moveIndex, float normalizedAnimationTime)
    {
        for (int i = 0; i < MaxInstructions; i++)
        {
            if (m_Series[i, moveIndex].Time.IsApproximately(normalizedAnimationTime, 0.05f))
            {
                switch (LanguageController.Instance.CurrentLanguage)
                {
                    case LanguageType.EnglishUK:
                    case LanguageType.EnglishUS:
                        return m_Series[i, moveIndex].Text;
                    case LanguageType.Dutch:
                        return m_Series[i, moveIndex].TextDutch;
                    case LanguageType.French:
                        return m_Series[i, moveIndex].TextFrench;
                    case LanguageType.Portugues:
                        return m_Series[i, moveIndex].TextPortugese;
                    case LanguageType.Spanish:
                        return m_Series[i, moveIndex].TextSpanish;
                    default:
                        return "ERROR GETTING TECHNIQUE";
                }
            }
        }
        return "Couldn't find matching technique step";
    }

    public CurrentNodeType GetNodeType(int moveIndex, float normalizedAnimationTime)
    {
        for (int i = 0; i < MaxInstructions; i++)
        {
            if (m_Series[i, moveIndex].Time.IsApproximately(normalizedAnimationTime, 0.05f))
            {
                return m_Series[i, moveIndex].Node;
            }
        }
        TTDebug.LogError("Didn't find node type : " + moveIndex + " : " + normalizedAnimationTime);
        return CurrentNodeType.Ball;
    }

    public Instruction GetInstruction(int moveIndex, int stepIndex)
    {
        return m_Series[stepIndex, moveIndex];
    }

    public string GetHelpText()
    {
        switch (LanguageController.Instance.CurrentLanguage)
        {
            case LanguageType.EnglishUK:
            case LanguageType.EnglishUS:
                return m_TextHelp[0].text;
            case LanguageType.French:
                return m_TextHelp[1].text;
            case LanguageType.Spanish:
                return m_TextHelp[2].text;
            case LanguageType.Portugues:
                return m_TextHelp[3].text;
            default:
                return m_TextHelp[0].text;
        }
    }
}
