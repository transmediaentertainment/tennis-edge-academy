﻿using UnityEngine;
using System.Collections;

public class LaunchScene : MonoBehaviour
{
	void Awake()
    {
        StartCoroutine(LoadLevel());
#if UNITY_IPHONE
        Handheld.PlayFullScreenMovie("TennisIntro.mov", Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFit);
#elif UNITY_ANDROID
        Handheld.PlayFullScreenMovie("TennisIntro.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFit);
#endif
	}

    IEnumerator LoadLevel()
    {
        var a = Application.LoadLevelAsync("MainScene");
        yield return a;
    }
	
}
