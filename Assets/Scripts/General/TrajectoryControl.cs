using UnityEngine;
using System.Collections;
using MotionEdge.Controls;

public class TrajectoryControl : MonoBehaviour 
{
	private TrailRenderer m_TrailRendererComponent;
    public AnimationControl m_AnimControl;

    private float m_ExtraTime = 0f;
    private const float m_StandardDisplayTime = 1f;

	void Start () 
    {
		m_TrailRendererComponent = this.GetComponent<TrailRenderer>();
        m_TrailRendererComponent.time = m_StandardDisplayTime;
		m_TrailRendererComponent.startWidth = 0.005f;
		m_TrailRendererComponent.endWidth = 0.005f;
	}

	void Update () 
    {
        if (m_AnimControl == null)
            return;
		// reset trail at beginning and end of animation loops
		if(m_AnimControl.NormalizedTime > 0.95f || m_AnimControl.NormalizedTime < 0.03f)
		{
			m_TrailRendererComponent.time = 0f;
            m_ExtraTime = 0f;
		}
		else
		{
            m_ExtraTime += Time.deltaTime * (1f - SpeedControl.Instance.Speed);
            m_TrailRendererComponent.time = m_StandardDisplayTime + m_ExtraTime;
		}
	}
}
