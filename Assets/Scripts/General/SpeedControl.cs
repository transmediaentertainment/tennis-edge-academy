using TransTech.Delegates;
using TransTech.System.Debug;
using UnityExtensions;

public class SpeedControl
{

    private static SpeedControl m_Instance;
    public static SpeedControl Instance
    {
        get
        {
            if (m_Instance == null)
                m_Instance = new SpeedControl();
            return m_Instance;
        }
    }

    private float m_Speed;
    public float Speed
    { 
        get 
        { 
            return m_Speed; 
        } 
        private set
        {
            m_Speed = value;
            if(SpeedChangedEvent != null)
                SpeedChangedEvent(m_Speed);
        }
    }
    public event FloatDelegate SpeedChangedEvent;

    private SpeedControl()
    {
        SetFullSpeed();
    }

    public void SetSlowMotion()
    {
		if(Speed.IsApproximately(0.5f, 0.01f))
		{
			Speed = 0.25f;
		}
		else
			Speed = 0.5f;
    }

    public void SetPaused()
    {
        Speed = 0f;
    }

    public void SetFullSpeed()
    {
        Speed = 1f;
    }
}
