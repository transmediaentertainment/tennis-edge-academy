using UnityEngine;
using System.Collections;
using MotionEdge;
using MotionEdge.Metadata;
using TransTech.System.Debug;
using MotionEdge.UI.Engines;

namespace MotionEdge.Controls
{
    public class EffectsControl : MonoBehaviour
    {
        [SerializeField]
        private AnimationControl m_AnimationControl;
        [SerializeField]
        private Transform[] m_AnchorObjs;
        [SerializeField]
        private Transform[] m_StepLabels;
        [SerializeField]
        private Camera m_UICamera;
        // Camera
        private Camera CameraComponent;

        // Line Renderer
        [SerializeField]
        private LineRenderer[] m_LineRenderers;

        // Intermediate values for start / end positions for line

        private Vector3 startPosition = new Vector3(0f, 0f, 0f);
        private Vector3 endPosition = new Vector3(0f, 0f, 0f);

        // A Ray used in calculating the start position of the line drawing

        private Ray startRay;

        // slots for nodes
        [SerializeField]
        private Transform m_NodeLeftFoot;
        [SerializeField]
        private Transform m_NodeRightFoot;
        [SerializeField]
        private Transform m_NodeBall;
        [SerializeField]
        private Transform m_NodeBody;
        [SerializeField]
        private Transform m_NodeRightKnee;
        [SerializeField]
        private Transform m_NodeLeftKnee;
        [SerializeField]
        private Transform m_NodeHead;
        [SerializeField]
        private Transform m_NodeHips;
        [SerializeField]
        private Transform m_NodeLeftHand;
        [SerializeField]
        private Transform m_NodeRightHand;
        [SerializeField]
		private Transform m_NodeLeftElbow;
		[SerializeField]
		private Transform m_NodeRightElbow;
		[SerializeField]
		private Transform m_NodeLeftShoulder;
        [SerializeField]
        private Transform m_NodeRightShoulder;
        [SerializeField]
        private Transform m_NodeRacket;
		[SerializeField]
		private Transform m_NodeRacketTop;
		[SerializeField]
		private Transform m_NodeRacketBase;

        private CurrentNodeType[] m_CurrentNodes;
		private float m_CurrentLookupTime;
		private bool m_WasShowing;
		
        void Start()
        {
            for (int i = 0; i < m_LineRenderers.Length; i++)
            {
                m_LineRenderers[i].enabled = false;
            }
            // cache camera
            CameraComponent = Camera.main;
			
			UIFSMEngine.MainMenuStateEvent += HandleMainMenuState;
			UIFSMEngine.SceneStateEvent += HandleSceneUIState;
        }
		
		private void HandleMainMenuState()
		{
			m_WasShowing = false;
			for (int i = 0; i < m_LineRenderers.Length; i++)
			{
				if(m_LineRenderers[i].enabled)
				{
					m_WasShowing = true;
				}
			}
			TurnOffLineEffect();
		}
		
		private void HandleSceneUIState()
		{
			if(m_WasShowing)
			{
				TurnOnLineEffect(m_CurrentLookupTime);
				m_WasShowing = false;
			}
		}

        public void TurnOnLineEffect(float lookupTime)
        {
			m_CurrentLookupTime = lookupTime;
            m_CurrentNodes = MetaDataController.Instance.GetNodesForAnimationAtTime(m_AnimationControl.CurrentAnimation, m_CurrentLookupTime);
            // Align anchors with labels.
            for (int i = 0; i < m_AnchorObjs.Length; i++)
            {
                var pos = m_AnchorObjs[i].transform.localPosition;
                // Quick hack to set both arrows from the same position when the second label isn't used
                if (m_StepLabels[i].gameObject.activeSelf)
                    pos.y = m_StepLabels[i].transform.localPosition.y;
                else
                    pos.y = m_StepLabels[0].transform.localPosition.y;
                m_AnchorObjs[i].transform.localPosition = pos;
            }
            SystemEventCenter.Instance.LateUpdateEvent += UpdatePosition;
        }

        public void TurnOffLineEffect()
        {
            for (int i = 0; i < m_LineRenderers.Length; i++)
            {
                m_LineRenderers[i].enabled = false;
            }
            SystemEventCenter.Instance.LateUpdateEvent -= UpdatePosition;
        }

        private void UpdatePosition(float deltaTime)
        {
            if (m_CurrentNodes.Length > 0)
            {
                for (int i = 0; i < m_CurrentNodes.Length; i++)
                {
                    if (i >= 2)
                    {
                        TTDebug.LogError("More than 2 nodes provided!  App does not support more than 2.");
                        return;
                    }

                    var screenPos = m_UICamera.WorldToScreenPoint(m_AnchorObjs[i].position);
                    startRay = CameraComponent.ScreenPointToRay(screenPos);
                    startPosition = startRay.origin;
                    m_LineRenderers[i].enabled = true;
                    switch (m_CurrentNodes[i])
                    {
                        case CurrentNodeType.LeftFoot:
                            endPosition = m_NodeLeftFoot.position;
                            break;
                        case CurrentNodeType.RightFoot:
                            endPosition = m_NodeRightFoot.position;
                            break;
                        case CurrentNodeType.Ball:
                            endPosition = m_NodeBall.position;
                            break;
                        case CurrentNodeType.Body:
                            endPosition = m_NodeBody.position;
                            break;
                        case CurrentNodeType.RightKnee:
                            endPosition = m_NodeRightKnee.position;
                            break;
                        case CurrentNodeType.LeftKnee:
                            endPosition = m_NodeLeftKnee.position;
                            break;
                        case CurrentNodeType.Head:
                            endPosition = m_NodeHead.position;
                            break;
                        case CurrentNodeType.Hips:
                            endPosition = m_NodeHips.position;
                            break;
                        case CurrentNodeType.LeftHand:
                            endPosition = m_NodeLeftHand.position;
                            break;
                        case CurrentNodeType.RightHand:
                            endPosition = m_NodeRightHand.position;
                            break;
						case CurrentNodeType.LeftElbow:
							endPosition = m_NodeLeftElbow.position;
							break;
						case CurrentNodeType.RightElbow:
							endPosition = m_NodeRightElbow.position;
							break;
						case CurrentNodeType.LeftShoulder:
                            endPosition = m_NodeLeftShoulder.position;
                            break;
                        case CurrentNodeType.RightShoulder:
                            endPosition = m_NodeRightShoulder.position;
                            break;
                        case CurrentNodeType.Racket:
                            endPosition = m_NodeRacket.position;
                            break;
						case CurrentNodeType.RacketTop:
							endPosition = m_NodeRacketTop.position;
							break;
						case CurrentNodeType.RacketBase:
							endPosition = m_NodeRacketBase.position;
							break;
                    }
                    m_LineRenderers[i].SetPosition(0, startPosition);
                    m_LineRenderers[i].SetPosition(1, endPosition);
                }
            }
        }
    }
}
