using UnityEngine;
using System.Collections;

public enum WhichObject
{
	Player,
	Ball,
	Racket
}

public class AnimationSpeedControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
        foreach (AnimationState state in animation) {
            state.speed = 0.25f;
        }
	
	}
}
