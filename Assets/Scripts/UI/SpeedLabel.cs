﻿using UnityEngine;
using UnityExtensions;

public class SpeedLabel : MonoBehaviour
{
    public UILabel m_Label;

    private void Awake()
    {
        SpeedControl.Instance.SpeedChangedEvent += SpeedChanged;
        m_Label.gameObject.SetActive(false);
    }

    private void SpeedChanged(float speed)
    {
        if (speed.IsApproximately(1f, 0.01f) || speed.IsApproximately(0f, 0.01f))
        {
            m_Label.gameObject.SetActive(false);
            return;
        }

        m_Label.gameObject.SetActive(true);

        if (speed.IsApproximately(0.5f, 0.01f))
        {
            m_Label.text = "1/2";
        }
        else if (speed.IsApproximately(0.25f, 0.01f))
        {
            m_Label.text = "1/4";
        }
        else
        {
            Debug.LogError("Didn't recognize speed!");
            m_Label.gameObject.SetActive(false);
        }
    }
	
}
