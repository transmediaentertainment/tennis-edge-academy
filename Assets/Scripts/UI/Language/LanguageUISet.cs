using System.Collections.Generic;
using System.Xml.Serialization;

namespace MotionEdge.Language
{
    public class LanguageUISet : IXmlSerializable
    {
        public Dictionary<string, string> KeyValues = new Dictionary<string,string>();


        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            var serializer = new XmlSerializer(typeof(string));

            bool wasEmpty = reader.IsEmptyElement;
            reader.Read();

            if (wasEmpty)
                return;

            KeyValues = new Dictionary<string, string>();

            while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
            {
                reader.ReadStartElement("item");
                reader.ReadStartElement("key");
                var key = (string)serializer.Deserialize(reader);
                reader.ReadEndElement();

                reader.ReadStartElement("value");
                var value = (string)serializer.Deserialize(reader);
                reader.ReadEndElement();

                KeyValues.Add(key, value);

                reader.ReadEndElement();
                reader.MoveToContent();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            var serializer = new XmlSerializer(typeof(string));

            foreach (var kvp in KeyValues)
            {
                writer.WriteStartElement("item");

                writer.WriteStartElement("key");
                serializer.Serialize(writer, kvp.Key);
                writer.WriteEndElement();

                writer.WriteStartElement("value");
                serializer.Serialize(writer, kvp.Value);
                writer.WriteEndElement();

                writer.WriteEndElement();
            }
        }
    }
}
