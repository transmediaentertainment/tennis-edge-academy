using UnityEngine;
using MotionEdge.Language;
using System.Collections;
using UnityExtensions;

/// <summary>
/// Component for a UILabel that automatically updates on Language Change.
/// </summary>
[RequireComponent(typeof(UILabel))]
public class UILabelLanguageSet : MonoBehaviour 
{
    /// <summary>
    /// The Key for the label this UILabel displays.
    /// We serialize the private field so it is editable in the Unity editor.
    /// </summary>
    [SerializeField]
    private string m_LabelKey;
    [SerializeField]
    private bool m_ShouldResizeButton;
    [SerializeField]
    private UISprite m_ButtonBackground;
    [SerializeField]
    private UITable m_Table;

    public string LabelKey { get { return m_LabelKey; } }

    /// <summary>
    /// Cached reference to the UILabel
    /// </summary>
    private UILabel m_Label;

    /// <summary>
    /// Called by Unity when the object is created.
    /// </summary>
    private void Awake()
    {
        // If no key has been setup, throw an error.
        if (m_LabelKey == null || m_LabelKey == "")
        {
            Debug.LogError("Label key not setup on object : " + name);
            return;
        }

        m_Label = GetComponent<UILabel>();

        UpdateLabel();

        // Subscribe to language change events
        LanguageController.Instance.NewLanguageSetEvent += UpdateLabel;
    }

    /// <summary>
    /// Called by Unity when the object is Destroyed
    /// </summary>
    private void OnDestroy()
    {
        // If the Language Controller exists, remove out event subscription
        if (LanguageController.Exists)
            LanguageController.Instance.NewLanguageSetEvent -= UpdateLabel;
    }

    private const float m_ButtonLineHeight = 55f;
    private const float m_ButtonWidth = 340f;

    /// <summary>
    /// Updates the label to the value for the current language setting
    /// </summary>
    private void UpdateLabel()
    {
        m_Label.text = LanguageController.Instance.GetValueForKey(m_LabelKey);
        // This section is REALLY hacky and should be better thought out. - JM
        if (m_ShouldResizeButton)
        {
            m_ButtonBackground.transform.localScale = new Vector3(m_ButtonWidth, m_ButtonLineHeight * m_Label.relativeSize.y, 1f);
            var coll = m_ButtonBackground.transform.parent.GetComponent<BoxCollider>();
            coll.size = new Vector3(m_ButtonWidth, m_ButtonLineHeight * m_Label.relativeSize.y, 0f);
            m_Table.Reposition();
            var offset = m_ButtonBackground.transform.parent.GetComponent<UIButtonOffset>();
            offset.ResetmPos();
        }

        // We can't use a coroutine on disabled labels, so we use
        // system event center instead - JM
        //StartCoroutine(MoveText());  
        m_FrameWait = 0;
        SystemEventCenter.Instance.UpdateEvent += MoveText;
    }

    private int m_FrameWait;

    private void MoveText(float deltaTime)
    {
        m_FrameWait++;
        if (m_FrameWait < 3)
            return;
        var localPos = m_Label.transform.localPosition;
        localPos.z = -1f;
        m_Label.transform.localPosition = localPos;
        SystemEventCenter.Instance.UpdateEvent -= MoveText;
    }

    /*private IEnumerator MoveText()
    {
        // We need to wait two frame for this positioning to work.
        yield return null;
        yield return null;
        var localPos = m_Label.transform.localPosition;
        localPos.z = -1f;
        m_Label.transform.localPosition = localPos;
    }*/
}
