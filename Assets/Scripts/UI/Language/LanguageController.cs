using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

namespace MotionEdge.Language
{
    public enum LanguageType
    {
        NotSet,
        EnglishUK,
		EnglishUS,
        French,
        Dutch,
        Spanish,
        Portugues
    }

    public class LanguageController
    {
        private static LanguageController m_Instance;
        public static LanguageController Instance
        {
            get
            {
                if (m_Instance == null)
                    m_Instance = new LanguageController();
                return m_Instance;
            }
        }

        public static bool Exists
        {
            get
            {
                return m_Instance != null;
            }
        }

        public const string LanguagePref = "LanguagePref";
        public const string EnglishFileName = "EnglishLanguage.txt";
        public const string FrenchFileName = "FrenchLanguage.txt";
        public const string SpanishFileName = "SpanishLanguage.txt";
        public const string PortugeseFileName = "PortugeseLanguage.txt";
        private const string FolderPath = "LanguageUISets";

        private LanguageController()
        {
            m_English = LoadLanguage(EnglishFileName);
            m_French = LoadLanguage(FrenchFileName);
            m_Spanish = LoadLanguage(SpanishFileName);
            m_Portugese = LoadLanguage(PortugeseFileName);
			SetLanguage((LanguageType)PlayerPrefs.GetInt(LanguagePref, (int)LanguageType.NotSet));
        }

        private LanguageUISet LoadLanguage(string fileName)
        {
            var splits = fileName.Split('.');
            // NOTE : Path.Combine results don't work with Resource.Load paths
            var textAsset = (TextAsset)Resources.Load(FolderPath + "/" + splits[0]);
            var serializer = new XmlSerializer(typeof(LanguageUISet));
            LanguageUISet language;
            using (var textReader = new StringReader(textAsset.text))
            {
                language = (LanguageUISet)serializer.Deserialize(textReader);
            }
            return language;
        }

        private LanguageUISet m_English;
        private LanguageUISet m_French;
        private LanguageUISet m_Spanish;
        private LanguageUISet m_Portugese;

        public LanguageUISet Current
        {
            get
            {
                switch (m_CurrentLanguage)
                {
                    case LanguageType.EnglishUK:
					case LanguageType.EnglishUS:
                        return m_English;
                    case LanguageType.French:
                        return m_French;
                    case LanguageType.Portugues:
                        return m_Portugese;
                    case LanguageType.Spanish:
                        return m_Spanish;
                    default:
                        return null;
                }
            }
        }

        private LanguageType m_CurrentLanguage;
		public LanguageType CurrentLanguage { get { return m_CurrentLanguage; } }

        public delegate void VoidDelegate();
        public event VoidDelegate NewLanguageSetEvent;
		
		public void SetLanguage(LanguageType language)
        {
			// We use the NotSet enumeration as a sign we should get the OS language.
            if (language == LanguageType.NotSet)
            {
				if(Application.platform == RuntimePlatform.Android)
				{
#if UNITY_ANDROID
					var localeClass = new AndroidJavaClass("java.util.Locale");
					var locale = localeClass.CallStatic<AndroidJavaObject>("getDefault");
					var lang = locale.Call<string>("getLanguage");
					
					switch(lang)
					{
					case "en":
						// Check country
						if(locale.Call<string>("getCountry") == "US")
							m_CurrentLanguage = LanguageType.EnglishUS;
						else
							m_CurrentLanguage = LanguageType.EnglishUK;
						break;
					case "fr":
						m_CurrentLanguage = LanguageType.French;
						break;
					case "es":
						m_CurrentLanguage = LanguageType.Spanish;
						break;
					case "pt":
						m_CurrentLanguage = LanguageType.Portugues;
						break;
					default:
						m_CurrentLanguage = LanguageType.EnglishUS;
						break;
					}
#endif
				}
				else
				{
	                // Get device setting
	                switch (Application.systemLanguage)
	                {
	                    case SystemLanguage.English:
	                        m_CurrentLanguage = LanguageType.EnglishUS;
	                        break;
	                    case SystemLanguage.Portuguese:
	                        m_CurrentLanguage = LanguageType.Portugues;
	                        break;
	                    case SystemLanguage.Spanish:
	                        m_CurrentLanguage = LanguageType.Spanish;
	                        break;
	                    case SystemLanguage.French:
	                        m_CurrentLanguage = LanguageType.French;
	                        break;
	                    default:
	                        m_CurrentLanguage = LanguageType.EnglishUS;
	                        break;
                	}
				}
				// We keep the not set value in prefs.
				PlayerPrefs.SetInt(LanguagePref, (int)language);
				if (NewLanguageSetEvent != null)
	                NewLanguageSetEvent();
				
            }
            else if(m_CurrentLanguage == language)
			{
				// If we are already on this language, don't fire events.
				return;
			}
			else
			{
				m_CurrentLanguage = language;
				
				PlayerPrefs.SetInt(LanguagePref, (int)m_CurrentLanguage);
	            if (NewLanguageSetEvent != null)
	                NewLanguageSetEvent();
			}
			
        }

        public string GetValueForKey(string key)
        {
            if (!Current.KeyValues.ContainsKey(key))
            {
                Debug.LogError("Key : " + key + " not found in " + m_CurrentLanguage);
                return "ERROR";
            }

            return Current.KeyValues[key];
        }
    }
}
