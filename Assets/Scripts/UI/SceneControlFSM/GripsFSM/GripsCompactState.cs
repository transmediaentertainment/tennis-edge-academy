﻿using TransTech.FiniteStateMachine;
using Holoville.HOTween;
using UnityEngine;
using MotionEdge.UI.Engines;

namespace MotionEdge.UI.States
{
    public class GripsCompactState : FSMState
    {
        private UIImageButton m_OpenButton;
        private GripsFSMEngine m_FSM;

        private readonly Vector3 m_SmallScale = new Vector3(0.001f, 0.001f, 1f);

        public GripsCompactState(GripsFSMEngine fsm, UIImageButton openButton)
        {
            m_FSM = fsm;
            m_OpenButton = openButton;
            m_OpenButton.transform.localScale = m_SmallScale;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);
            UIController.GripsOpenButtonPressedEvent += OpenButtonPressed;

            // Scale Button Up
            ScaleUp();
            m_FSM.UIPosUpdated += UpdatePos;
        }

        private void UpdatePos(Vector3 vec)
        {
            m_OpenButton.transform.position = vec;
        }

        public override void Exit()
        {
            base.Exit();
            UIController.GripsOpenButtonPressedEvent -= OpenButtonPressed;
            // Scale button down
            ScaleDown();
            m_FSM.UIPosUpdated -= UpdatePos;
        }

        public override void GainedFocus()
        {
            base.GainedFocus();

            UIController.GripsOpenButtonPressedEvent += OpenButtonPressed;
            // Scale Button Up
            ScaleUp();
            m_FSM.UIPosUpdated += UpdatePos;
        }

        public override void LostFocus()
        {
            base.LostFocus();
            UIController.GripsOpenButtonPressedEvent -= OpenButtonPressed;
            // Scale button down
            ScaleDown();
            m_FSM.UIPosUpdated -= UpdatePos;
        }

        private void ScaleUp()
        {
            Scale(Vector3.one);
        }

        private void ScaleDown()
        {
            Scale(m_SmallScale);
        }

        private void Scale(Vector3 targetScale)
        {
            HOTween.To(m_OpenButton.transform, UIController.TweenTime, new TweenParms().Prop("localScale", targetScale).Ease(EaseType.EaseInOutQuad));
        }

        private void OpenButtonPressed()
        {
            m_FSM.SetExpandedState();
        }
    }
}
