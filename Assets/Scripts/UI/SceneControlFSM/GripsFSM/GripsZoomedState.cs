﻿using TransTech.FiniteStateMachine;
using UnityEngine;
using TransTech.System.Debug;
using Holoville.HOTween;
using MotionEdge.UI.Engines;

namespace MotionEdge.UI.States
{
    public class GripsZoomedState : FSMState
    {
        private UIImageButton[] m_GripButtons;
        private int m_ZoomIndex;
        private GripsFSMEngine m_FSM;

        private readonly Vector3 m_ZoomedButtonSize = new Vector3(0.75f, 0.75f, 1f);
        private readonly Vector3 m_ShrunkButtonSize = new Vector3(0.1f, 0.1f, 1f);
        private readonly Vector3 m_RegularButtonSize = new Vector3(0.25f, 0.25f, 1f);

        public GripsZoomedState(GripsFSMEngine fsm)
        {
            m_FSM = fsm;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            // Check for index (of one to scale up) and array of UIImageButtons
            if (args == null || args.Length != 2 || !(args[0] is int) || !(args[1] is UIImageButton[]))
            {
                TTDebug.LogError("GripZoomedState.cs : Invalid arguments passed into Enter function");
                return;
            }

            m_ZoomIndex = (int)args[0];

            m_GripButtons = (UIImageButton[])args[1];

            for (int i = 0; i < m_GripButtons.Length; i++)
            {
                if (i == m_ZoomIndex)
                {
                    HOTween.To(m_GripButtons[i].transform, UIController.TweenTime, new TweenParms().Prop("localScale", m_ZoomedButtonSize).Ease(EaseType.EaseInOutQuad));
                }
                else
                {
                    HOTween.To(m_GripButtons[i].transform, UIController.TweenTime, new TweenParms().Prop("localScale", m_ShrunkButtonSize).Ease(EaseType.EaseInOutQuad));
                }
            }

            UIController.GripButtonPressedEvent += GripButtonPressed;
        }

        public override void Exit()
        {
            base.Exit();

            for (int i = 0; i < m_GripButtons.Length; i++)
            {
                HOTween.To(m_GripButtons[i].transform, UIController.TweenTime, new TweenParms().Prop("localScale", m_RegularButtonSize).Ease(EaseType.EaseInOutQuad));
            }
            UIController.GripButtonPressedEvent -= GripButtonPressed;
        }

        private void GripButtonPressed(GameObject go)
        {
            m_FSM.SetExpandedState();
        }
    }
}
