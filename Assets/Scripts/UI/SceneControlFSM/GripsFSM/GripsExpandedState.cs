﻿using MotionEdge.Metadata;
using TransTech.FiniteStateMachine;
using TransTech.System.Debug;
using UnityEngine;
using UnityExtensions;
using Holoville.HOTween;
using System.Collections.Generic;
using MotionEdge.UI.Engines;

namespace MotionEdge.UI.States
{
    public class GripsExpandedState : FSMState
    {
        private UIImageButton[] m_GripButtons;
        private List<Vector3> m_ButtonOffsets = new List<Vector3>();
        private UISprite m_Circle;
        private UIImageButton m_CloseButton;

        private float m_ButtonFullDistance = 0.4f;
        public float m_ButtonDistance = 0f;

        private readonly Vector3 m_CircleFullSize = new Vector3(300f, 300f, 1f);
        private readonly Vector3 m_CloseButtonFullSize = new Vector3(1f, 1f, 1f);
        private readonly Vector3 m_GripButtonMidSize = new Vector3(0.25f, 0.25f, 1f);
        private readonly Vector3 m_NearZero = new Vector3(0.001f, 0.001f, 1f);

        private GripsFSMEngine m_FSM;

        public GripsExpandedState(GripsFSMEngine fsm, UISprite circle, UIImageButton closeButton)
        {
            m_FSM = fsm;
            m_Circle = circle;
            m_CloseButton = closeButton;

            m_Circle.gameObject.SetActive(false);
            m_CloseButton.gameObject.SetActive(false);
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            if (args == null || args.Length != 2 || !(args[0] is UIImageButton[]) || !(args[1] is UISprite[]))
            {
                TTDebug.LogError("GripsExpandedState.cs : Image Buttons and sprites not passed into enter function");
                return;
            }

            m_GripButtons = (UIImageButton[])args[0];

            // Position the buttons on the circle
            m_ButtonOffsets.Clear();
            float rotation = 0f;
            float rotIncrement = (Mathf.PI * 2f) / m_GripButtons.Length;
            for (int i = 0; i < m_GripButtons.Length; i++)
            {
                var vec = Vector2.up.RotateVector(rotation);
                m_ButtonOffsets.Add(new Vector3(vec.x, vec.y));
                rotation += rotIncrement;
                // Scale the buttons up
                // TODO : try some timed offsets so it becomes a sequence
                m_GripButtons[i].gameObject.SetActive(true);
                // Because the sprite is getting set when the GameObject is not active the sprite
                // doesn't update correctly.  Diabling then enabling the UIImageButton script forces
                // a refresh of the sprite. - JM
                m_GripButtons[i].enabled = false;
                m_GripButtons[i].enabled = true;
                HOTween.To(m_GripButtons[i].transform, UIController.TweenTime, new TweenParms().Prop("localScale", m_GripButtonMidSize).Ease(EaseType.EaseOutQuad));
            }

            // Scale the offset vectors
            HOTween.To(this, UIController.TweenTime, new TweenParms().Prop("m_ButtonDistance", m_ButtonFullDistance).Ease(EaseType.EaseOutQuad));

            // Expand the circle
            m_Circle.gameObject.SetActive(true);
            HOTween.To(m_Circle.transform, UIController.TweenTime, new TweenParms().Prop("localScale", m_CircleFullSize).Ease(EaseType.EaseOutQuad));

            // Expand the Close button
            m_CloseButton.gameObject.SetActive(true);
            HOTween.To(m_CloseButton.transform, UIController.TweenTime, new TweenParms().Prop("localScale", m_CloseButtonFullSize).Ease(EaseType.EaseOutQuad));

            // Hook into the update
            m_FSM.UIPosUpdated += UpdatePos;

            UIController.GripButtonPressedEvent += GripButtonPressed;
            UIController.GripsCloseButtonPressedEvent += GripsCloseButtonPressed;
        }

        private void GripButtonPressed(GameObject button)
        {
            var btn = button.GetComponent<UIImageButton>();
            m_FSM.SetZoomedState(btn);
        }

        private void GripsCloseButtonPressed()
        {
            m_FSM.SetCompactState();
        }

        private void UpdatePos(Vector3 vec)
        {
            m_Circle.transform.position = vec;
            m_CloseButton.transform.position = vec;
            for (int i = 0; i < m_GripButtons.Length; i++)
            {
                m_GripButtons[i].transform.position = vec + (m_ButtonDistance * m_ButtonOffsets[i]);
            }
        }

        public override void Exit()
        {
            base.Exit();

            // Scale down the circle, call the disable function on completion
            HOTween.To(m_Circle.transform, UIController.TweenTime, new TweenParms().Prop("localScale", m_NearZero).Ease(EaseType.EaseOutQuad).OnComplete(DisableAfterScale));

            // Scale down the close button
            HOTween.To(m_CloseButton.transform, UIController.TweenTime, new TweenParms().Prop("localScale", m_NearZero).Ease(EaseType.EaseOutQuad));

            // Scale down the offset Vectors
            HOTween.To(this, UIController.TweenTime, new TweenParms().Prop("m_ButtonDistance", 0f).Ease(EaseType.EaseOutQuad));

            for (int i = 0; i < m_GripButtons.Length; i++)
            {
                // Scale the buttons down
                // TODO : try some timed offsets so it becomes a sequence
                HOTween.To(m_GripButtons[i].transform, UIController.TweenTime, new TweenParms().Prop("localScale", m_NearZero).Ease(EaseType.EaseOutQuad));
            }

            UIController.GripButtonPressedEvent -= GripButtonPressed;
            UIController.GripsCloseButtonPressedEvent -= GripsCloseButtonPressed;
        }

        private void DisableAfterScale()
        {
            m_Circle.gameObject.SetActive(false);
            m_CloseButton.gameObject.SetActive(false);
            for (int i = 0; i < m_GripButtons.Length; i++)
            {
                m_GripButtons[i].gameObject.SetActive(false);
            }
            m_FSM.UIPosUpdated -= UpdatePos;
        }

        public override void LostFocus()
        {
            base.LostFocus();
            UIController.GripButtonPressedEvent -= GripButtonPressed;

        }

        public override void GainedFocus()
        {
            base.GainedFocus();
            UIController.GripButtonPressedEvent += GripButtonPressed;
        }
    }
}
