﻿using TransTech.FiniteStateMachine;
using TransTech.Delegates.Unity;
using MotionEdge.UI.States;
using MotionEdge.Metadata;
using UnityEngine;
using TransTech.System.Debug;

namespace MotionEdge.UI.Engines
{
    public class GripsFSMEngine : FSM
    {
        private IdleFSMState m_IdleState;
        private GripsCompactState m_CompactState;
        private GripsExpandedState m_ExpandedState;
        private GripsZoomedState m_ZoomedState;

        private UIController m_UIController;

        private UIImageButton[] m_GripButtons;
        private UISprite[] m_GripNumbers;
        private GameObject m_GripButtonPrefab;
        private GameObject m_GripNumberPrefab;

        private UIImageButton m_OpenButton;
        private UIImageButton m_CloseButton;
        private UISprite m_Circle;

        private Transform m_HandTarget;
        private Camera m_WorldCamera;
        private Camera m_UICamera;
        private UIPanel m_GripsPanel;
        public Vector3 GripsUIPos { get; private set; }
        public event Vector3Delegate UIPosUpdated;

        private const string m_Continental = "Continental";
        private const string m_EasternBackhand = "Eastern Backhand";
        private const string m_EasternForehand = "Eastern Forehand";
        private const string m_ExtremeEastern = "Extreme or Semi-Western Backhand";
        private const string m_SemiWesternBackhand = "Extreme or Semi-Western Backhand";
        private const string m_SemiWesternForehand = "Semi-Western Forehand";
        private const string m_TwoHandedBackhand = "Two Handed Backhand";
        private const string m_WesternForehand = "Western Forehand";


        public GripsFSMEngine(UIController uiController, UIImageButton openButton, UIImageButton closeButton, UISprite circle, UIImageButton gripsButtonPrefab, 
            Camera worldCamera, Camera uiCamera, Transform handTransform, UIPanel gripsPanel, UISprite gripsNumberPrefab)
        {
            m_IdleState = new IdleFSMState();
            m_CompactState = new GripsCompactState(this, openButton);
            m_ExpandedState = new GripsExpandedState(this, circle, closeButton);
            m_ZoomedState = new GripsZoomedState(this);

            m_UIController = uiController;
            m_WorldCamera = worldCamera;
            m_UICamera = uiCamera;
            m_HandTarget = handTransform;
            m_GripsPanel = gripsPanel;

            m_OpenButton = openButton;
            m_CloseButton = closeButton;
            m_Circle = circle;

            NewState(m_IdleState);

            m_GripButtonPrefab = gripsButtonPrefab.gameObject;
            m_GripNumberPrefab = gripsNumberPrefab.gameObject;

            SystemEventCenter.Instance.EarlyUpdateEvent += UpdatePosition;
            UIFSMEngine.MainMenuStateEvent += MainMenuOpened;
            UIFSMEngine.SceneStateEvent += MainMenuClosed;
        }

        private void UpdatePosition(float deltaTime)
        {
            if (Peek() != m_IdleState && UIPosUpdated != null)
            {
                var screenPos = m_WorldCamera.WorldToScreenPoint(m_HandTarget.position);
                GripsUIPos = m_UICamera.ScreenToWorldPoint(new Vector3(screenPos.x, screenPos.y, 0f));
                UIPosUpdated(GripsUIPos);
            }
        }

        public void InitNewGrips(GripType[] grips)
        {
            if (m_GripButtons != null)
            {
                for (int i = 0; i < m_GripButtons.Length; i++)
                {
                    GameObject.Destroy(m_GripButtons[i].gameObject);
                }
            }

            if (m_GripNumbers != null)
            {
                for (int i = 0; i < m_GripNumbers.Length; i++)
                {
                    GameObject.Destroy(m_GripNumbers[i].gameObject);
                }
            }

            m_GripButtons = new UIImageButton[grips.Length];
            m_GripNumbers = new UISprite[grips.Length];

            for (int i = 0; i < m_GripButtons.Length; i++)
            {
                var go = GameObject.Instantiate(m_GripButtonPrefab) as GameObject;
                go.transform.parent = m_GripsPanel.transform;
                go.transform.localScale = Vector3.one * 0.001f;
                m_GripButtons[i] = go.GetComponent<UIImageButton>();
                var msg = go.GetComponent<UIButtonMessage>();
                msg.target = m_UIController.gameObject;
                msg.functionName = "GripButtonPressed";

                switch (grips[i])
                {
                    case GripType.Continental:
                        SetButtonSprite(i, m_Continental);
                        break;
                    case GripType.EasternBackhand:
                        SetButtonSprite(i, m_EasternBackhand);
                        break;
                    case GripType.EasternForehand:
                        SetButtonSprite(i, m_EasternForehand);
                        break;
                    case GripType.ExtremeEastern:
                        SetButtonSprite(i, m_ExtremeEastern);
                        break;
                    case GripType.SemiWesternBackhand:
                        SetButtonSprite(i, m_SemiWesternBackhand);
                        break;
                    case GripType.SemiWesternForehand:
                        SetButtonSprite(i, m_SemiWesternForehand);
                        break;
                    case GripType.TwoHandedBackhand:
                        SetButtonSprite(i, m_TwoHandedBackhand);
                        break;
                    case GripType.WesternForehand:
                        SetButtonSprite(i, m_WesternForehand);
                        break;
                }

                var num = GameObject.Instantiate(m_GripNumberPrefab) as GameObject;
                num.transform.parent = go.transform;
                num.transform.localPosition = new Vector3(-170f, 170f, 0f);
                num.transform.localScale = Vector3.one * 120f;
                m_GripNumbers[i] = num.GetComponent<UISprite>();
                m_GripNumbers[i].depth = 15;

                switch (i)
                {
                    case 0:
                        m_GripNumbers[i].spriteName = "One";
                        break;
                    case 1:
                        m_GripNumbers[i].spriteName = "Two";
                        break;
                    case 2:
                        m_GripNumbers[i].spriteName = "Three";
                        break;
                }
            }
        }

        private void SetButtonSprite(int index, string sprite)
        {
            m_GripButtons[index].normalSprite = sprite;
            m_GripButtons[index].hoverSprite = sprite;
            m_GripButtons[index].pressedSprite = sprite;
            m_GripButtons[index].disabledSprite = sprite;
        }

        public void SetIdleState()
        {
            while (Peek() != m_IdleState)
                PopState();
        }

        public void SetCompactState()
        {
            // If we are in Idle mode, we stack compact on top
            if (Peek() == m_IdleState)
            {
                PushState(m_CompactState);
            }
            // Otherwise we pop states until we are back at compact
            else
            {
                while (Peek() != m_CompactState)
                {
                    PopState();
                }
            }
        }

        public void SetExpandedState()
        {
            // If we are in the Zoomed state we need to pop off the stack
            if (Peek() == m_ZoomedState)
            {
                PopState();
            }
            // If we are in the compact state we need to push onto the stack
            else if (Peek() == m_CompactState)
            {
                PushState(m_ExpandedState, m_GripButtons, m_GripNumbers);
                // There is a visual glitch on the first frame, so we need to
                // Force refresh the position after siwtching to this state. - JM
                UpdatePosition(0f);
            }
            // Else we shouldn't be going into this state
        }

        public void SetZoomedState(UIImageButton zoomButton)
        {
            // We can only get here via the expanded state
            if (Peek() == m_ExpandedState)
            {
                int index = -1;
                for (int i = 0; i < m_GripButtons.Length; i++)
                {
                    if (zoomButton == m_GripButtons[i])
                    {
                        index = i;
                        break;
                    }
                }
                if (index == -1)
                    TTDebug.LogError("GripsFSMEngine.cs : No matching button found!");


                PushState(m_ZoomedState, index, m_GripButtons);
            }
        }

        private bool m_GripsWereActive;
        private bool m_OpenButtonActive;
        private bool m_CloseButtonActive;
        private bool m_CircleActive;

        private void MainMenuOpened()
        {
            m_GripsWereActive = (m_GripButtons != null && m_GripButtons.Length > 0) ? m_GripButtons[0].gameObject.activeInHierarchy : false;
            if (m_GripsWereActive)
            {
                for (int i = 0; i < m_GripButtons.Length; i++)
                {
                    m_GripButtons[i].gameObject.SetActive(false);
                }
            }
            m_OpenButtonActive = m_OpenButton.gameObject.activeInHierarchy;
            m_OpenButton.gameObject.SetActive(false);
            m_CloseButtonActive = m_CloseButton.gameObject.activeInHierarchy;
            m_CloseButton.gameObject.SetActive(false);
            m_CircleActive = m_Circle.gameObject.activeInHierarchy;
            m_Circle.gameObject.SetActive(false);
        }

        private void MainMenuClosed()
        {
            if (m_GripsWereActive)
            {
                for (int i = 0; i < m_GripButtons.Length; i++)
                {
                    m_GripButtons[i].gameObject.SetActive(true);
                }
            }
            if (m_OpenButtonActive)
                m_OpenButton.gameObject.SetActive(true);
            if (m_CloseButtonActive)
                m_CloseButton.gameObject.SetActive(true);
            if (m_CircleActive)
                m_Circle.gameObject.SetActive(true);
        }
    }
}
