using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.Engines;
using Holoville.HOTween;

namespace MotionEdge.UI.States
{
    public class CameraUIState : FSMState
    {
        protected UIPanel m_Panel;
        protected Vector3 m_ShowPos;
        protected Vector3 m_HidePos;

        public CameraUIState(UIPanel panel, Vector3 showPos, Vector3 hidePos)
        {
            m_Panel = panel;
            m_ShowPos = showPos;
            m_HidePos = hidePos;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);
            ShowPanel();
        }

        public override void Exit()
        {
            base.Exit();
            HidePanel();
        }

        public override void LostFocus()
        {
            base.LostFocus();
            HidePanel();
        }

        public override void GainedFocus()
        {
            base.GainedFocus();
            ShowPanel();
        }

        private void ShowPanel()
        {
            Tween(m_ShowPos);
        }

        private void HidePanel()
        {
            Tween(m_HidePos);
        }

        private void Tween(Vector3 toPos)
        {
            HOTween.To(m_Panel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", toPos).Ease(EaseType.EaseInOutQuad));
        }
    }
}
