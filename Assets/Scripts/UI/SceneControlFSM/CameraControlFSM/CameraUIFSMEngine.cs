using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.States;

namespace MotionEdge.UI.Engines
{
    public class CameraUIFSMEngine : HFSMState
    {
        private UIController m_Controller;
        private CameraUIState m_HiddenState;
        private CameraUIState m_ShowingState;
        private IdleFSMState m_IdleState;

        public Vector3 MainPanelShowingPos { get; private set; }
        public Vector3 MainPanelHidingPos { get; private set; }
        public Vector3 SmallPanelShowingPos { get; private set; }
        public Vector3 SmallPanelHidingPos { get; private set; }

        public CameraUIFSMEngine(UIController controller, UIPanel mainPanel, UIPanel smallPanel)
        {
            m_Controller = controller;

            MainPanelShowingPos = new Vector3(-133f, 0f, 0f);
            MainPanelHidingPos = new Vector3(165f, 0f, 0f);

            SmallPanelShowingPos = new Vector3(-45f, 0f, 0f);
            SmallPanelHidingPos = new Vector3(37f, 0f, 0f);

            m_HiddenState = new CameraUIState(smallPanel, SmallPanelShowingPos, SmallPanelHidingPos);
            m_ShowingState = new CameraUIState(mainPanel, MainPanelShowingPos, MainPanelHidingPos);
            m_IdleState = new IdleFSMState();

            

            // Force into position
            mainPanel.transform.localPosition = MainPanelHidingPos;
            smallPanel.transform.localPosition = SmallPanelHidingPos;

            // Register for events
            m_Controller.CameraPanelCrossPressedEvent += () => NewState(m_HiddenState);
            m_Controller.ShowCameraPanelPressedEvent += () => NewState(m_ShowingState);

            NewState(m_IdleState);
        }

        public void SetInitialState()
        {
            NewState(m_ShowingState);
        }

        public void PushIdle()
        {
            if(Peek() != m_IdleState)
                PushState(m_IdleState);
        }

        public void PopIdle()
        {
            if(Peek() == m_IdleState)
                PopState();
        }
    }
}
