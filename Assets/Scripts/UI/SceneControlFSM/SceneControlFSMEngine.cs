﻿using MotionEdge.Controls;
using TransTech.FiniteStateMachine;
using TransTech.System.Debug;
using UnityEngine;
using Holoville.HOTween;

namespace MotionEdge.UI.Engines
{
    public class SceneControlFSMEngine : HFSMState
    {
        private CameraUIFSMEngine m_CameraUIFSM;
        private ActionListUIFSMEngine m_ActionUIFSM;
        private AnimationControlUIFSMEngine m_AnimControlUIFSM;

        private UIImageButton m_MainMenuButton;
        private readonly Vector3 m_MainMenuButtonOnPos = new Vector3(-105f, 74f);
        private readonly Vector3 m_MainMenuButtonOffPos = new Vector3(105f, 74f);

        private bool m_First = true;

        public SceneControlFSMEngine(UIController controller, AnimationControl[] animationControls, EffectsControl effectsControl, PlayStateUI playStateUI, UIPanel mainCameraPanel, UIPanel smallCameraPanel, UIPanel mainActionsPanel, UIPanel smallActionsPanel,
            UILabel infoLabel, UILabel[] stepInfoLabels, UIImageButton playButton, UIImageButton pauseButton, UIImageButton slowMoButton, UIImageButton stepButton, UIPanel mainMenuPanel, UIPanel creditsPanel, UIPanel languagePanel, UIPanel linksPanel, 
            UIPanel playButtonsPanel, UIImageButton mainMenuButton, UIImageButton openGripsButton, UIImageButton closeGripsButton, UISprite gripsCircle, UIImageButton gripsButtonPrefab, Camera worldCamera, Camera uiCamera, Transform handTransform, UIPanel gripsPanel,
            UISprite gripsNumberPrefab, UIDraggablePanel infoDragPanel)
        {
            m_CameraUIFSM = new CameraUIFSMEngine(controller, mainCameraPanel, smallCameraPanel);
            m_ActionUIFSM = new ActionListUIFSMEngine(controller, mainActionsPanel, smallActionsPanel, infoLabel, stepInfoLabels, infoDragPanel);
            m_AnimControlUIFSM = new AnimationControlUIFSMEngine(controller, animationControls, playButton, pauseButton, slowMoButton, stepButton, playStateUI, stepInfoLabels, effectsControl, playButtonsPanel, 
                openGripsButton, closeGripsButton, gripsCircle, gripsButtonPrefab, worldCamera, uiCamera, handTransform, gripsPanel, gripsNumberPrefab);

            m_MainMenuButton = mainMenuButton;
            m_MainMenuButton.transform.localPosition = m_MainMenuButtonOffPos;
            m_First = true;
        }

        public override void Enter(params object[] args)
        {
            base.Enter();

            HOTween.To(m_MainMenuButton.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_MainMenuButtonOnPos).Ease(EaseType.EaseInOutQuad));

            if (m_First)
            {
                m_First = false;
                m_CameraUIFSM.SetInitialState();
                m_ActionUIFSM.SetInitialState();
                m_AnimControlUIFSM.SetInitialState();
            }
            else
            {
                m_CameraUIFSM.PopIdle();
                m_ActionUIFSM.PopIdle();
                m_AnimControlUIFSM.PopIdle();
            }
        }

        public override void Exit()
        {
            base.Exit();

            HOTween.To(m_MainMenuButton.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_MainMenuButtonOffPos).Ease(EaseType.EaseInOutQuad));

            m_CameraUIFSM.PushIdle();
            m_ActionUIFSM.PushIdle();
            m_AnimControlUIFSM.PushIdle();
        }
    }
}
