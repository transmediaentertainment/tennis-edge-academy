using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using Holoville.HOTween;
using MotionEdge.UI.Engines;

namespace MotionEdge.UI.States
{
    public class ActionListHiddenState : ActionListState
    {
        public ActionListHiddenState(ActionListUIFSMEngine fsm, UIPanel mainPanel, UIPanel smallPanel)
            : base(fsm, mainPanel, smallPanel)
        {
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            HOTween.To(m_MainPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.MainPanelHidingPos).Ease(EaseType.EaseInOutQuad));
            HOTween.To(m_SmallPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.SmallPanelShowingPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void LostFocus()
        {
            base.LostFocus();
            HOTween.To(m_SmallPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.SmallPanelHidingPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void GainedFocus()
        {
            base.GainedFocus();
            HOTween.To(m_SmallPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.SmallPanelShowingPos).Ease(EaseType.EaseInOutQuad));
        }
    }
}