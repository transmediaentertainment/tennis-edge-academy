﻿using MotionEdge.Metadata;
using MotionEdge.UI.Engines;
using TransTech.System.Debug;

namespace MotionEdge.UI.States
{
    public class AdditionalInfoOverviewShowingState : AdditionalInfoShowingState
    {
        private string m_CurrentMove;

        public AdditionalInfoOverviewShowingState(UIController uiController, ActionListUIFSMEngine fsm, UIPanel smallPanel, UILabel infoLabel, UIDraggablePanel dragPanel) :
            base(uiController, fsm, smallPanel, infoLabel, dragPanel)
        {
            m_UIController.ShowInfoButtonPressedEvent += m_FSM.SetInfoState;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            if(args == null || args.Length != 1 || !(args[0] is string))
            {
                TTDebug.LogError("AdditionalInfoOverviewShowingState.cs : Invalid arguments passed into Enter function");
                return;
            }

            m_CurrentMove = (string)args[0];
            m_FullText = MetaDataController.Instance.GetLocalizedOverview(m_CurrentMove);
            m_UIController.ShowInfoButtonPressedEvent -= m_FSM.SetInfoState;
            m_UIController.ShowInfoButtonPressedEvent += m_FSM.SetHiddenState;
        }

        public override void Exit()
        {
            base.Exit();

            m_UIController.ShowInfoButtonPressedEvent += m_FSM.SetInfoState;
            m_UIController.ShowInfoButtonPressedEvent -= m_FSM.SetHiddenState;
        }

        protected override void LanguageChanged()
        {
            base.LanguageChanged();
            m_FullText = MetaDataController.Instance.GetLocalizedOverview(m_CurrentMove);
        }
    }
}
