using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.States;
using MotionEdge.Metadata;

namespace MotionEdge.UI.Engines
{
    public class ActionListUIFSMEngine : FSM
    {
        private UIController m_Controller;
        private UIPanel m_MainPanel;
        private UIPanel m_SmallPanel;
        private UILabel m_InfoLabel;
        private UILabel[] m_StepInfoLabels;

        private ActionListShowingState m_ShowingState;
        private ActionListHiddenState m_HiddenState;
        //private AdditionalInfoShowingState m_InfoState;
        private AdditionalInfoOverviewShowingState m_OverviewState;
        private AdditionalInfoTipsShowingState m_TipsState;
        private IdleFSMState m_IdleState;

        public Vector3 MainPanelShowingPos { get; private set; }
        public Vector3 MainPanelHidingPos { get; private set; }
        public Vector3 SmallPanelShowingPos { get; private set; }
        public Vector3 SmallPanelHidingPos { get; private set; }

        private string m_LastActionButtonPressed;

        public ActionListUIFSMEngine(UIController controller, UIPanel mainPanel, UIPanel smallPanel, UILabel infoLabel, UILabel[] stepInfoLabels, UIDraggablePanel infoDragPanel)
        {
            // Cache items
            m_Controller = controller;
            m_MainPanel = mainPanel;
            m_SmallPanel = smallPanel;
            m_InfoLabel = infoLabel;
            m_StepInfoLabels = stepInfoLabels;

            // Create States
            m_ShowingState = new ActionListShowingState(controller, this, m_MainPanel, m_SmallPanel);
            m_HiddenState = new ActionListHiddenState(this, m_MainPanel, m_SmallPanel);
            //m_InfoState = new AdditionalInfoShowingState(controller, this, m_SmallPanel, m_InfoLabel);
            m_OverviewState = new AdditionalInfoOverviewShowingState(controller, this, m_SmallPanel, m_InfoLabel, infoDragPanel);
            m_TipsState = new AdditionalInfoTipsShowingState(controller, this, m_SmallPanel, m_InfoLabel, infoDragPanel);
            m_IdleState = new IdleFSMState();

            // Setup positions
            MainPanelShowingPos = new Vector3(212f, 0f, 0f);
            MainPanelHidingPos = new Vector3(-220f, 0f, 0f);

            SmallPanelShowingPos = new Vector3(52f, 0f, 0f);
            SmallPanelHidingPos = new Vector3(-395f, 0f, 0f);

            // Force into position
            m_MainPanel.transform.localPosition = MainPanelHidingPos;
            m_SmallPanel.transform.localPosition = SmallPanelHidingPos;

            foreach (var label in m_StepInfoLabels)
            {
                label.gameObject.SetActive(false);
            }
            m_InfoLabel.gameObject.SetActive(false);

            // register for events
            m_ShowingState.TimerFinishedEvent += SetHiddenState;
            m_Controller.ActionButtonPressedEvent += ActionButtonPressed;
            m_Controller.ShowActionPanelPressedEvent += () => NewState(m_ShowingState);
            //m_Controller.ShowInfoButtonPressedEvent += () => NewState(m_OverviewState, m_LastActionButtonPressed);
           // m_Controller.ShowTipsButtonPressedEvent += () => NewState(m_TipsState, m_LastActionButtonPressed);
            // If we go into step mode, we want to hide
            m_Controller.StepButtonPressedEvent += () => { if (Peek() == m_ShowingState) SetHiddenState(); };
            //m_Controller.FallthroughDragEvent += (v) => SetHiddenState();

            NewState(m_IdleState);
        }

        public void SetInfoState()
        {
            NewState(m_OverviewState, m_LastActionButtonPressed);
        }

        public void SetTipsState()
        {
            NewState(m_TipsState, m_LastActionButtonPressed);
        }

        private void ActionButtonPressed(string buttonName)
        {
            m_LastActionButtonPressed = buttonName;
        }

        public void SetHiddenStateWrapper(Vector2 vec)
        {
            SetHiddenState();
        }

        public void SetHiddenState()
        {
            if (Peek() != m_HiddenState)
                NewState(m_HiddenState);
        }

        public void PushIdle()
        {
            PushState(m_IdleState);
        }

        public void PopIdle()
        {
            if(Peek() == m_IdleState)
                PopState();
        }

        public void SetInitialState()
        {
            NewState(m_ShowingState);
        }
    }
}
