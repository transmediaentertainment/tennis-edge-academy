using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.Engines;
using MotionEdge.Metadata;
using Holoville.HOTween;
using TransTech.System.Debug;
using MotionEdge.Language;

namespace MotionEdge.UI.States
{
    public abstract class AdditionalInfoShowingState : FSMState
    {
        protected UIController m_UIController;
        protected ActionListUIFSMEngine m_FSM;
        private UILabel m_InfoLabel;
        private UIPanel m_SmallPanel;
        private UIDraggablePanel m_DraggablePanel;
        private BoxCollider m_DragHitBox;

        protected string m_FullText;
        private float m_Timer;
        private int m_CurrentIndex;
        private const float m_NewCharTime = 0.05f;

        public AdditionalInfoShowingState(UIController uiController, ActionListUIFSMEngine fsm, UIPanel smallPanel, UILabel infoLabel, UIDraggablePanel dragPanel)
        {
            m_UIController = uiController;
            m_FSM = fsm;
            m_InfoLabel = infoLabel;
            m_SmallPanel = smallPanel;
            m_DraggablePanel = dragPanel;
            m_DragHitBox = m_DraggablePanel.GetComponent<BoxCollider>();
            m_DragHitBox.enabled = false;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);
            m_InfoLabel.gameObject.SetActive(true);
            m_InfoLabel.text = "";
            m_Timer = 0f;
            m_CurrentIndex = 0;
            m_DragHitBox.enabled = false;
            m_DraggablePanel.ResetPosition();

            m_UIController.StepButtonPressedEvent += m_FSM.SetHiddenState;
            LanguageController.Instance.NewLanguageSetEvent += LanguageChanged;
        }

        public override void LostFocus()
        {
            base.LostFocus();
            m_UIController.StepButtonPressedEvent -= m_FSM.SetHiddenState;

            HOTween.To(m_SmallPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.SmallPanelHidingPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void GainedFocus()
        {
            base.GainedFocus();
            m_UIController.StepButtonPressedEvent += m_FSM.SetHiddenState;

            HOTween.To(m_SmallPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.SmallPanelShowingPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void Exit()
        {
            base.Exit();

            m_InfoLabel.gameObject.SetActive(false);
            m_UIController.StepButtonPressedEvent -= m_FSM.SetHiddenState;

            m_DragHitBox.enabled = false;

            LanguageController.Instance.NewLanguageSetEvent -= LanguageChanged;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            if (m_CurrentIndex < m_FullText.Length)
            {
                m_Timer += deltaTime;
                if (m_Timer >= m_NewCharTime)
                {
                    m_Timer -= m_NewCharTime;
                    m_CurrentIndex++;
                    while (m_CurrentIndex < m_FullText.Length && m_FullText[m_CurrentIndex] != ' ')
                        m_CurrentIndex++;
                    m_InfoLabel.text = m_FullText.Substring(0, m_CurrentIndex);
                    if (m_DraggablePanel.shouldMoveVertically)
                    {
                        m_DragHitBox.enabled = true;
                    }
                }
            }
        }

        protected virtual void LanguageChanged()
        {
            m_InfoLabel.text = "";
            m_CurrentIndex = 0;
            m_Timer = 0f;
        }
    }
}