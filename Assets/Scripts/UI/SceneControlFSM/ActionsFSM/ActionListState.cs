using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.Engines;

namespace MotionEdge.UI.States
{
    public abstract class ActionListState : FSMState
    {
        protected ActionListUIFSMEngine m_FSM;
        protected UIPanel m_MainPanel;
        protected UIPanel m_SmallPanel;

        public ActionListState(ActionListUIFSMEngine fsm, UIPanel mainPanel, UIPanel smallPanel)
        {
            m_FSM = fsm;
            m_MainPanel = mainPanel;
            m_SmallPanel = smallPanel;
        }
    }
}
