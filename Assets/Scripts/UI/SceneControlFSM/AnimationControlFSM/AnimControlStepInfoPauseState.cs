using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.Engines;
using MotionEdge.Controls;
using MotionEdge.Metadata;
using TransTech.System.Debug;
using MotionEdge.Language;

namespace MotionEdge.UI.States
{
    public class AnimControlStepInfoPauseState : AnimControlState
    {
        private MetaData m_MetaData;
        private UILabel[] m_Labels;
        private AnimationControl[] m_AnimationControls;
        private EffectsControl m_EffectsControl;

        private GripsFSMEngine m_GripsFSM;
        private float m_AnimSetTime;

        public AnimControlStepInfoPauseState(AnimationControl[] animationControls, UIController controller, AnimationControlUIFSMEngine fsm, 
            UIImageButton playButton, UIImageButton pauseButton, UILabel[] labels, EffectsControl effectsControl, UIPanel buttonsPanel, 
            UIImageButton openGripsButton, UIImageButton closeGripsButton, UISprite gripsCircle, UIImageButton gripsButtonPrefab, 
            Camera worldCamera, Camera uiCamera, Transform handTransform, UIPanel gripsPanel, UISprite gripsNumberPrefab)
            : base(controller, fsm, playButton, pauseButton, buttonsPanel)
        {
            m_Labels = labels;
            m_AnimationControls = animationControls;
            m_EffectsControl = effectsControl;

            m_GripsFSM = new GripsFSMEngine(controller, openGripsButton, closeGripsButton, gripsCircle, gripsButtonPrefab, worldCamera, uiCamera, handTransform, gripsPanel, gripsNumberPrefab);
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            if (args == null || args.Length != 1 || !(args[0] is float))
            {
                TTDebug.LogError("AnimControlStepInfoPauseState.cs : No valid pause reset time passed into enter function");
                return;
            }
            m_AnimSetTime = (float)args[0];

            AnimationControlUIFSMEngine.EnableAndFadeButton(m_PauseButton);
            AnimationControlUIFSMEngine.DisableAndFadeButton(m_PlayButton);

            m_FSM.SetIconsVisibility(false, false, true);

            m_Controller.PlayButtonPressedEvent += PlayButtonPressed;
            m_Controller.SlowMoButtonPressedEvent += SlowButtonPressed;
            m_Controller.StepButtonPressedEvent += StepButtonPressed;
            m_Controller.ShowInfoButtonPressedEvent += InfoButtonPressed;
            m_Controller.ShowTipsButtonPressedEvent += InfoButtonPressed;
            m_Controller.ActionButtonPressedEvent += ActionButtonPressed;
            m_Controller.RewindButtonPressedEvent += RewindButtonPressed;

            /*for (int i = 0; i < m_AnimationControls.Length; i++)
            {
                m_AnimationControls[i].SlowAnimationFinishedEvent += SetAbsoluteAnimationTime;
            }*/
            SpeedControl.Instance.SetPaused();

            SetAnimationLabel();

            m_EffectsControl.TurnOnLineEffect(m_AnimSetTime);

            // See if there are any grips
            var grips = MetaDataController.Instance.GetStepGrips(m_AnimationControls[0].CurrentAnimation, m_AnimSetTime);
            if (grips != null)
            {
                // Turn on Grips
                m_GripsFSM.InitNewGrips(grips);
                m_GripsFSM.SetCompactState();
            }

            LanguageController.Instance.NewLanguageSetEvent += SetAnimationLabel;
        }

        private void SetAnimationLabel()
        {
            var techniques = MetaDataController.Instance.GetStepTechniques(m_AnimationControls[0].CurrentAnimation, m_AnimSetTime);
            float verticalSize = 0f;
            for (int i = 0; i < techniques.Length; i++)
            {
                m_Labels[i].gameObject.SetActive(true);
                m_Labels[i].text = techniques[i];
                verticalSize += m_Labels[i].relativeSize.y * m_Labels[i].transform.localScale.y;
            }
            float verticalSpacing = 60f;
            verticalSize += (techniques.Length - 1) * verticalSpacing;
            float startPos = verticalSize * 0.5f;
            // reposition labels
            for (int i = 0; i < techniques.Length; i++)
            {
                var height = m_Labels[i].relativeSize.y * m_Labels[i].transform.localScale.y;
                var pos = m_Labels[i].transform.localPosition;
                pos.y = startPos - (height * 0.5f);
                m_Labels[i].transform.localPosition = pos;
                startPos -= height + verticalSpacing;
            }
        }
        /*
        private void SetAbsoluteAnimationTime(AnimationControl animControl)
        {
            animControl.SlowAnimationFinishedEvent -= SetAbsoluteAnimationTime;
            animControl.SetNormalizedAnimationTime(m_AnimSetTime);
        }
        */
        public override void Exit()
        {
            base.Exit();

            for (int i = 0; i < m_Labels.Length; i++)
            {
                m_Labels[i].gameObject.SetActive(false);
            }

            m_Controller.PlayButtonPressedEvent -= PlayButtonPressed;
            m_Controller.SlowMoButtonPressedEvent -= SlowButtonPressed;
            m_Controller.StepButtonPressedEvent -= StepButtonPressed;
            m_Controller.ShowInfoButtonPressedEvent -= InfoButtonPressed;
            m_Controller.ShowTipsButtonPressedEvent -= InfoButtonPressed;
            m_Controller.ActionButtonPressedEvent -= ActionButtonPressed;
            m_Controller.RewindButtonPressedEvent -= RewindButtonPressed;

            LanguageController.Instance.NewLanguageSetEvent -= SetAnimationLabel;

            m_EffectsControl.TurnOffLineEffect();

            m_GripsFSM.SetIdleState();
        }

        private void PlayButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.Play);
        }

        private void SlowButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.Slow);
        }

        private void StepButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.StepPlay);
        }

        private void InfoButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.Play);
        }

        private void ActionButtonPressed(string buttonName)
        {
            m_FSM.SetState(AnimControlStates.Play);
        }

        private bool m_CamRegistered = false;

        private void RewindButtonPressed()
        {
            if (m_CamRegistered)
                return;
            CameraFadeFSM.FadeOutCompleteEvent += CameraFadeOutComplete;
        }

        private void CameraFadeOutComplete()
        {
            CameraFadeFSM.FadeOutCompleteEvent -= CameraFadeOutComplete;
            m_CamRegistered = false;
            m_FSM.SetState(AnimControlStates.StepPlay);
        }

        // Radial Helper Func
        private Vector2 DegreesToXY(float degrees, float radius)
        {
            float radians = degrees * Mathf.Deg2Rad;
            Vector2 result = Vector2.zero;
            result.x = (float)Mathf.Cos((float)radians) * radius;
            result.y = (float)Mathf.Sin((float)radians) * radius;
            return result;
        }
    }
}