using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.Engines;
using UnityExtensions;

namespace MotionEdge.UI.States
{
    public class AnimControlSlowState : AnimControlState
    {
        public AnimControlSlowState(UIController controller, AnimationControlUIFSMEngine fsm, UIImageButton playButton, UIImageButton pauseButton, UIPanel buttonsPanel) : base(controller, fsm, playButton, pauseButton, buttonsPanel) { }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            AnimationControlUIFSMEngine.DisableAndFadeButton(m_PlayButton);
            AnimationControlUIFSMEngine.EnableAndFadeButton(m_PauseButton);

            m_FSM.SetIconsVisibility(true, false, false);

            m_Controller.SlowMoButtonPressedEvent += SlowButtonPressed;
            m_Controller.PauseButtonPressedEvent += PauseButtonPressed;
            m_Controller.StepButtonPressedEvent += StepButtonPressed;

            SpeedControl.Instance.SetSlowMotion();
        }

        public override void Exit()
        {
            base.Exit();

            m_Controller.SlowMoButtonPressedEvent -= SlowButtonPressed;
            m_Controller.PauseButtonPressedEvent -= PauseButtonPressed;
            m_Controller.StepButtonPressedEvent -= StepButtonPressed;
        }

        private void SlowButtonPressed()
        {
			if(SpeedControl.Instance.Speed.IsApproximately(0.5f, 0.01f))
			{
				SpeedControl.Instance.SetSlowMotion();
			}
			else
            	m_FSM.SetState(AnimControlStates.Play);
        }

        private void PauseButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.SlowPause);
        }

        private void StepButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.StepPlay);
        }
    }
}
