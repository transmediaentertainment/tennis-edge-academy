using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.Engines;

namespace MotionEdge.UI.States
{
    public class AnimControlStepPlayPauseState : AnimControlState
    {
        public AnimControlStepPlayPauseState(UIController controller, AnimationControlUIFSMEngine fsm, UIImageButton playButton, UIImageButton pauseButton, UIPanel buttonsPanel) : base(controller, fsm, playButton, pauseButton, buttonsPanel) { }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            m_FSM.SetIconsVisibility(false, false, true);

            AnimationControlUIFSMEngine.DisableAndFadeButton(m_PauseButton);
            AnimationControlUIFSMEngine.EnableAndFadeButton(m_PlayButton);

            m_Controller.PlayButtonPressedEvent += PlayButtonPressed;
            m_Controller.SlowMoButtonPressedEvent += SlowButtonPressed;
            m_Controller.StepButtonPressedEvent += StepButtonPressed;

            SpeedControl.Instance.SetPaused();
        }

        public override void Exit()
        {
            base.Exit();

            m_Controller.PlayButtonPressedEvent -= PlayButtonPressed;
            m_Controller.SlowMoButtonPressedEvent -= SlowButtonPressed;
            m_Controller.StepButtonPressedEvent -= StepButtonPressed;
        }

        private void PlayButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.StepPlay);
        }

        private void SlowButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.Slow);
        }

        private void StepButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.StepPlay);
        }
    }
}
