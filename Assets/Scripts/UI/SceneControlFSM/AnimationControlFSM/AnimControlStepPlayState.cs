using TransTech.FiniteStateMachine;
using TransTech.System.Debug;
using MotionEdge.UI.Engines;
using MotionEdge.Controls;
using MotionEdge.Metadata;

namespace MotionEdge.UI.States
{
    public class AnimControlStepPlayState : AnimControlState
    {
        private MetaData m_MetaData;
        private AnimationControl[] m_AnimationControls;
        private float m_NextTime;
        private bool m_IsRegistered;

        public AnimControlStepPlayState(AnimationControl[] animationControl, UIController controller, AnimationControlUIFSMEngine fsm, UIImageButton playButton, UIImageButton pauseButton, UIPanel buttonsPanel)
            : base(controller, fsm, playButton, pauseButton, buttonsPanel)
        {
            m_AnimationControls = animationControl;
        }

		void FindNextStepTime ()
		{
			var currentTime = m_AnimationControls[0].NormalizedTime;
        	bool done = false;
        	int stepIndex = 0;
        	m_NextTime = -1f;
        	while (!done)
        	{
        	    var data = MetaDataController.Instance.GetData(m_AnimationControls[0].CurrentAnimation);
        	    if(stepIndex >= data.TriggerTimes.Length)
        	    {
        	        // No more steps so let it play out.
        	        done = true;
        	        continue;
        	    }
        	    if (data.TriggerTimes[stepIndex] > currentTime)
        	    {
        	        m_NextTime = data.TriggerTimes[stepIndex];
        	        done = true;
        	    }
        	    stepIndex++;
        	}
		}
        

        public override void Enter(params object[] args)
        {
            base.Enter(args);
         
            AnimationControlUIFSMEngine.DisableAndFadeButton(m_PlayButton);
            AnimationControlUIFSMEngine.EnableAndFadeButton(m_PauseButton);

            m_FSM.SetIconsVisibility(false, false, true);

            m_Controller.PauseButtonPressedEvent += PauseButtonPressed;
            m_Controller.SlowMoButtonPressedEvent += SlowButtonPressed;
            m_Controller.RewindButtonPressedEvent += RewindButtonPressed;

            SpeedControl.Instance.SetSlowMotion();

            // Find the next Time
            FindNextStepTime ();
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            /*var cont = m_AnimationControls[0];
            TTDebug.Log("Current Speed : " + cont.CurrentSpeed);
            var slowTime = AnimationControl.SlowTime / cont.CurrentAnimationLength;
            var timeOffset = (cont.CurrentSpeed * slowTime) +
                (0.5f * (-cont.CurrentSpeed / slowTime) *
                (slowTime * slowTime));*/
            //timeOffset /= cont.CurrentAnimationLength;

            // If we don't have another instruction, we just want to loop
            if (m_NextTime < 0f)
            {
                if (m_AnimationControls[0].NormalizedTime >= 0.99f || m_AnimationControls[0].NormalizedTime <= 0.03f)
                    m_FSM.SetState(AnimControlStates.StepPlay);
            }
            else if (m_AnimationControls[0].NormalizedTime >= m_NextTime/* - timeOffset*/)
            {
                //TTDebug.Log("Next Time : " + m_NextTime + " : Offset : " + timeOffset);
                m_FSM.SetState(AnimControlStates.StepInfoPause, m_NextTime);
            }
        }

        public override void Exit()
        {
            base.Exit(); 

            m_Controller.PauseButtonPressedEvent -= PauseButtonPressed;
            m_Controller.SlowMoButtonPressedEvent -= SlowButtonPressed;
            m_Controller.RewindButtonPressedEvent -= RewindButtonPressed;
        }
		
		public override void GainedFocus ()
		{
			base.GainedFocus ();
			// The animation keeps playing if we go into main menu
			// so we need to re-find the time when we gain focus.
			FindNextStepTime();
		}

        private void PauseButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.StepPlayPause);
        }

        private void SlowButtonPressed()
        {
            m_FSM.SetState(AnimControlStates.Slow);
        }

        private void RewindButtonPressed()
        {
            if (m_IsRegistered)
                return;

            CameraFadeFSM.FadeOutCompleteEvent += FadeOutComplete;
            m_IsRegistered = true;
        }

        private void FadeOutComplete()
        {
            m_FSM.SetState(AnimControlStates.StepPlay);
            CameraFadeFSM.FadeOutCompleteEvent -= FadeOutComplete;
            m_IsRegistered = false;
        }
    }
}