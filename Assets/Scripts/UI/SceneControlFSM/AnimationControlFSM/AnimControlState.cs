using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.Engines;
using Holoville.HOTween;

namespace MotionEdge.UI.States
{
    public abstract class AnimControlState : FSMState
    {
        protected UIController m_Controller;
        protected AnimationControlUIFSMEngine m_FSM;
        protected UIImageButton m_PlayButton;
        protected UIImageButton m_PauseButton;
        private UIPanel m_ButtonsPanel;
        private Vector3 OnScreenPos = new Vector3(0f, 58f, 0f);
        private Vector3 OffScreenPos = new Vector3(0f, -110f, 0f);

        public AnimControlState(UIController controller, AnimationControlUIFSMEngine fsm, UIImageButton playButton, UIImageButton pauseButton, UIPanel buttonsPanel)
        {
            m_Controller = controller;
            m_FSM = fsm;
            m_PlayButton = playButton;
            m_PauseButton = pauseButton;
            m_ButtonsPanel = buttonsPanel;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);
            if(m_ButtonsPanel.transform.localPosition.y < 0f)
                HOTween.To(m_ButtonsPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", OnScreenPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void LostFocus()
        {
            base.LostFocus();
            HOTween.To(m_ButtonsPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", OffScreenPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void GainedFocus()
        {
            base.GainedFocus();
            HOTween.To(m_ButtonsPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", OnScreenPos).Ease(EaseType.EaseInOutQuad));
        }
    }
}
