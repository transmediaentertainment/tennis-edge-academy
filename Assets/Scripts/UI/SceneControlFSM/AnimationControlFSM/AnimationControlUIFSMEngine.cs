using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using Holoville.HOTween;
using MotionEdge.UI.States;
using MotionEdge.Controls;

namespace MotionEdge.UI.Engines
{
    public enum AnimControlStates
    {
        Idle,
        Play,
        Pause,
        Slow,
        SlowPause,
        StepPlay,
        StepPlayPause,
        StepInfoPause
    }

    public class AnimationControlUIFSMEngine : FSM
    {
        private const float FadeDuration = 0.2f;

        //private UIController m_Controller;
        private PlayStateUI m_PlayStateUI;

        private AnimControlPlayState m_PlayState;
        private AnimControlPauseState m_PauseState;
        private AnimControlSlowState m_SlowState;
        private AnimControlSlowPauseState m_SlowPauseState;
        private AnimControlStepPlayState m_StepPlayState;
        private AnimControlStepPlayPauseState m_StepPlayPauseState;
        private AnimControlStepInfoPauseState m_StepInfoPauseState;
        private IdleFSMState m_IdleState;

        private AnimationControl[] m_AnimationControls;

        public AnimationControlUIFSMEngine(UIController controller, AnimationControl[] animationControls, UIImageButton playButton, UIImageButton pauseButton, UIImageButton slowMoButton, UIImageButton stepButton, PlayStateUI playStateUI,
            UILabel[] stepInfoLabels, EffectsControl effectsControl, UIPanel buttonsPanel, UIImageButton openGripsButton, UIImageButton closeGripsButton, UISprite gripsCircle, UIImageButton gripsButtonPrefab, Camera worldCamera,
            Camera uiCamera, Transform handTransform, UIPanel gripsPanel, UISprite gripsNumberPrefab)
        {
            //m_Controller = controller;
            m_PlayStateUI = playStateUI;
            HideSprite(m_PlayStateUI.PausedSprite);
            HideSprite(m_PlayStateUI.SlowMoSprite);
            HideSprite(m_PlayStateUI.StepSprite);
            HideLabel(m_PlayStateUI.PausedLabel);
            HideLabel(m_PlayStateUI.SlowMoLabel);
            HideLabel(m_PlayStateUI.StepLabel);

            m_PlayState = new AnimControlPlayState(controller, this, playButton, pauseButton, buttonsPanel);
            m_PauseState = new AnimControlPauseState(controller, this, playButton, pauseButton, buttonsPanel);
            m_SlowState = new AnimControlSlowState(controller, this, playButton, pauseButton, buttonsPanel);
            m_SlowPauseState = new AnimControlSlowPauseState(controller, this, playButton, pauseButton, buttonsPanel);
            m_StepPlayState = new AnimControlStepPlayState(animationControls, controller, this, playButton, pauseButton, buttonsPanel);
            m_StepPlayPauseState = new AnimControlStepPlayPauseState(controller, this, playButton, pauseButton, buttonsPanel);
            m_StepInfoPauseState = new AnimControlStepInfoPauseState(animationControls, controller, this, playButton, pauseButton, stepInfoLabels, effectsControl, buttonsPanel, openGripsButton, closeGripsButton,
                gripsCircle, gripsButtonPrefab, worldCamera, uiCamera, handTransform, gripsPanel, gripsNumberPrefab);
            m_IdleState = new IdleFSMState();

            m_AnimationControls = animationControls;
            controller.RewindButtonPressedEvent += RewindButtonPressed;

            // Register for events
            SetState(AnimControlStates.Play);

            // Force them off screen for startup.
            buttonsPanel.transform.localPosition = new Vector3(0f, -110f, 0f);
        }

        private void RewindButtonPressed()
        {
            for (int i = 0; i < m_AnimationControls.Length; i++)
            {
                m_AnimationControls[i].RestartAnimation();
            }
        }

        public void SetState(AnimControlStates state, float animPauseTime = 0f)
        {
            switch (state)
            {
                case AnimControlStates.Idle:
                    NewState(m_IdleState);
                    break;
                case AnimControlStates.Play:
                    NewState(m_PlayState);
                    break;
                case AnimControlStates.Pause:
                    NewState(m_PauseState);
                    break;
                case AnimControlStates.Slow:
                    NewState(m_SlowState);
                    break;
                case AnimControlStates.SlowPause:
                    NewState(m_SlowPauseState);
                    break;
                case AnimControlStates.StepInfoPause:
                    NewState(m_StepInfoPauseState, animPauseTime);
                    break;
                case AnimControlStates.StepPlay:
                    NewState(m_StepPlayState);
                    break;
                case AnimControlStates.StepPlayPause:
                    NewState(m_StepPlayPauseState);
                    break;
                default:
                    // Nothing
                    break;
            }
        }

        public static void DisableAndFadeButton(UIImageButton button)
        {
            button.collider.enabled = false;
            var sprite = button.GetComponentInChildren<UISprite>();
            HOTween.To(sprite, FadeDuration, new TweenParms().Prop("color", Color.clear));
        }

        public static void EnableAndFadeButton(UIImageButton button)
        {
            button.collider.enabled = true;
            var sprite = button.GetComponentInChildren<UISprite>();
            HOTween.To(sprite, FadeDuration, new TweenParms().Prop("color", Color.white));
        }

        public void SetIconsVisibility(bool slowMo, bool paused, bool step)
        {
            SetIconEnabled(m_PlayStateUI.SlowMoSprite, m_PlayStateUI.SlowMoLabel, slowMo);
            SetIconEnabled(m_PlayStateUI.PausedSprite, m_PlayStateUI.PausedLabel, paused);
            SetIconEnabled(m_PlayStateUI.StepSprite, m_PlayStateUI.StepLabel, step);
        }

        private static void SetIconEnabled(UISprite sprite, UILabel label, bool enabled)
        {
            var col = sprite.color;
            col.a = enabled ? 1f : 0f;
            HOTween.To(sprite, FadeDuration, new TweenParms().Prop("color", col));
            HOTween.To(label, FadeDuration, new TweenParms().Prop("color", col));
        }

        public void PushIdle()
        {
            if (Peek() != m_IdleState)
                PushState(m_IdleState);
        }

        public void PopIdle()
        {
            if (Peek() == m_IdleState)
                PopState();
        }

        public void SetInitialState()
        {
            SetState(AnimControlStates.Play);
        }

        private void HideLabel(UILabel label)
        {
            var col = label.color;
            col.a = 0f;
            label.color = col;
        }

        private void HideSprite(UISprite sprite)
        {
            var col = sprite.color;
            col.a = 0f;
            sprite.color = col;
        }
    }
}