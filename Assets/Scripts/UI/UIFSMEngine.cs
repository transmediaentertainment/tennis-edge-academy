using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.States;
using MotionEdge.Controls;
using System;

namespace MotionEdge.UI.Engines
{
    public class UIFSMEngine : FSM
    {
        private SceneControlFSMEngine m_SceneControlFSM;
        private MainMenuFSMEngine m_MainMenuFSM;

        public static event Action MainMenuStateEvent;
        public static event Action SceneStateEvent;

        public UIFSMEngine(UIController controller, AnimationControl[] animationControls, EffectsControl effectsControl, PlayStateUI playStateUI, UIPanel mainCameraPanel, UIPanel smallCameraPanel, UIPanel mainActionsPanel, UIPanel smallActionsPanel,
            UILabel infoLabel, UILabel[] stepInfoLabels, UIImageButton playButton, UIImageButton pauseButton, UIImageButton slowMoButton, UIImageButton stepButton, UIPanel mainMenuPanel, UIPanel creditsPanel, UIPanel languagePanel, UIPanel linksPanel, UIPanel IAPPanel,
            UIPanel playButtonsPanel, UIImageButton mainMenuButton, UIImageButton openGripsButton, UIImageButton closeGripsButton, UISprite gripsCircle, UIImageButton gripsButtonPrefab, Camera worldCamera, Camera uiCamera, Transform handTarget, UIPanel gripsPanel,
            UILabel iapInfoLabel, BusyIconAnimator busyIcon, UIButton purchaseButton, UIButton restoreButton, Material cameraPlaneMat, UISprite gripsNumberPrefab, UIPanel instructionsPanel, UIDraggablePanel infoDragPanel)
        {
            m_SceneControlFSM = new SceneControlFSMEngine(controller, animationControls, effectsControl, playStateUI, mainCameraPanel, smallCameraPanel,
                mainActionsPanel, smallActionsPanel, infoLabel, stepInfoLabels, playButton, pauseButton, slowMoButton, stepButton, mainMenuPanel, creditsPanel, languagePanel, linksPanel, playButtonsPanel, mainMenuButton, openGripsButton, closeGripsButton,
                gripsCircle, gripsButtonPrefab, worldCamera, uiCamera, handTarget, gripsPanel, gripsNumberPrefab, infoDragPanel);
            m_MainMenuFSM = new MainMenuFSMEngine(controller, mainMenuPanel, creditsPanel, languagePanel, linksPanel, IAPPanel, instructionsPanel, iapInfoLabel, busyIcon, purchaseButton, restoreButton, cameraPlaneMat);
            controller.MainMenuButtonPressedEvent += MainMenuButtonPressed;
            controller.TrainButtonPressedEvent += TrainButtonPressed;
            controller.LockedActionButtonPressedEvent += LockedActionButtonPressed;
            NewState(m_MainMenuFSM);
        }

        private void MainMenuButtonPressed()
        {
            NewState(m_MainMenuFSM);

            if (MainMenuStateEvent != null)
                MainMenuStateEvent();
        }

        private void TrainButtonPressed()
        {
            NewState(m_SceneControlFSM);

            if (SceneStateEvent != null)
                SceneStateEvent();
        }

        private void LockedActionButtonPressed(GameObject button)
        {
            NewState(m_MainMenuFSM);
            m_MainMenuFSM.SetIAPState();

            if (MainMenuStateEvent != null)
                MainMenuStateEvent();
        }
    }
}
