using UnityEngine;
using System.Collections;
using TransTech.Delegates;
using TransTech.Delegates.Unity;
using MotionEdge.Language;
using System.Collections.Generic;
using MotionEdge.Metadata;
using MotionEdge.Controls;
using MotionEdge.UI.Engines;
using System;

namespace MotionEdge.UI
{
    public class PlayStateUI
    {
        public UISprite PausedSprite { get; set; }
        public UILabel PausedLabel { get; set; }
        public UISprite SlowMoSprite { get; set; }
        public UILabel SlowMoLabel { get; set; }
        public UISprite StepSprite { get; set; }
        public UILabel StepLabel { get; set; }

        public PlayStateUI(UISprite pausedSprite, UILabel pausedLabel, UISprite slowMoSprite, UILabel slowMoLabel, UISprite stepSprite, UILabel stepLabel)
        {
            PausedSprite = pausedSprite;
            PausedLabel = pausedLabel;
            SlowMoSprite = slowMoSprite;
            SlowMoLabel = slowMoLabel;
            StepSprite = stepSprite;
            StepLabel = stepLabel;
        }
    }

    public class UIController : MonoBehaviour
    {
        [SerializeField]
        private UIPanel m_MainCameraPanel;
        [SerializeField]
        private UIPanel m_SmallCameraPanel;

        [SerializeField]
        private UIButton m_CameraHighButton;
        [SerializeField]
        private UIButton m_CameraMidButton;
        [SerializeField]
        private UIButton m_CameraLowButton;

        [SerializeField]
        private UIButton m_CameraNorthButton;
        [SerializeField]
        private UIButton m_CameraSouthButton;
        [SerializeField]
        private UIButton m_CameraEastButton;
        [SerializeField]
        private UIButton m_CameraWestButton;

        [SerializeField]
        private UIPanel m_PlayButtonsPanel;
        [SerializeField]
        private UIImageButton m_PlayButton;
        [SerializeField]
        private UIImageButton m_PauseButton;
        [SerializeField]
        private UIImageButton m_StepButton;
        [SerializeField]
        private UIImageButton m_SlowMoButton;

        [SerializeField]
        private UISprite m_SlowMoCamIcon;
        [SerializeField]
        private UISprite m_PauseCamIcon;
        [SerializeField]
        private UISprite m_StepCamIcon;
        [SerializeField]
        private UILabel m_SlowMoCamLabel;
        [SerializeField]
        private UILabel m_PauseCamLabel;
        [SerializeField]
        private UILabel m_StepCamLabel;

        [SerializeField]
        private UIPanel m_MainActionsPanel;
        [SerializeField]
        private UIPanel m_SmallActionsPanel;
        [SerializeField]
        private UILabel m_InfoLabel;
        [SerializeField]
        private UILabel[] m_StepInfoLabels;
        [SerializeField]
        private UILabel m_VerticalLabel;
        [SerializeField]
        private UITable m_ActionsTable;
        [SerializeField]
        private UIDraggablePanel m_ActionsDraggablePanel;
        [SerializeField]
        private UIDraggablePanel m_InfoDraggablePanel;

        [SerializeField]
        private UIPanel m_MainMenuPanel;
        [SerializeField]
        private UIPanel m_CreditsPanel;
        [SerializeField]
        private UIPanel m_LanguagePanel;
        [SerializeField]
        private UIPanel m_LinksPanel;
		[SerializeField]
        private UIPanel m_IAPPanel;
        [SerializeField]
        private UIPanel m_InstructionsPanel;

        [SerializeField]
        private UIImageButton m_MainMenuButton;


        [SerializeField]
        private AnimationControl[] m_AnimationControls;

        [SerializeField]
        private EffectsControl m_EffectsControl;
        [SerializeField]
        private CameraFade m_CameraFade;
        [SerializeField]
        private Material m_CameraPanelMaterial;

        [SerializeField]
        private UIImageButton m_OpenGripsButton;
        [SerializeField]
        private UIImageButton m_CloseGripsButton;
        [SerializeField]
        private UISprite m_GripsCircle;
        [SerializeField]
        private UIImageButton m_GripsButtonPrefab;
        [SerializeField]
        private UISprite m_GripsNumberPrefab;
        [SerializeField]
        private UIPanel m_GripsPanel;

        [SerializeField]
        private GameObject m_ActionButtonPrefab;
        private Dictionary<string, UIButton> m_ActionListButtons = new Dictionary<string, UIButton>();
		
		[SerializeField]
		private Color m_ActionButtonLockedBackgroundColour;
		[SerializeField]
		private Color m_ActionButtonUnlockedBackgroundColour;
		[SerializeField]
		private Color m_ActionButtonLockedHoverColour;
		[SerializeField]
		private Color m_ActionButtonUnlockedHoverColour;
        [SerializeField]
        private Color m_ActionButtonLockedTextColour;
        [SerializeField]
        private Color m_ActionButtonUnlockedTextColour;
        [SerializeField]
        private UIDraggablePanel m_ActionButtonsDragPanel;

        [SerializeField]
        private Camera m_WorldCamera;
        [SerializeField]
        private Camera m_UICamera;
        [SerializeField]
        private Transform m_HandTarget;

        [SerializeField]
        private BusyIconAnimator m_BusyIcon;
        [SerializeField]
        private UILabel m_IAPInfoLabel;
        [SerializeField]
        private UIButton m_PurchaseButton;
        [SerializeField]
        private UIButton m_RestoreButton;

        //private UIFSMEngine m_UIFSM;

        public const float TweenTime = 0.25f;

        private const float m_ActionButtonLineHeight = 55f;
        private const float m_ActionButtonWidth = 340f;

        [SerializeField]
        private string m_DefaultStartingButton = "BackHandOneHandedFlat";

        void Awake()
        {
            Screen.autorotateToLandscapeLeft = true;
            Screen.autorotateToLandscapeRight = true;
            Screen.autorotateToPortrait = false;
            Screen.autorotateToPortraitUpsideDown = false;
            Screen.orientation = ScreenOrientation.AutoRotation;
			
			Application.targetFrameRate = 60;
			
			

            var playStateUI = new PlayStateUI(m_PauseCamIcon, m_PauseCamLabel, m_SlowMoCamIcon, m_SlowMoCamLabel, m_StepCamIcon, m_StepCamLabel);
            /*m_UIFSM = */new UIFSMEngine(this, m_AnimationControls, m_EffectsControl, playStateUI, m_MainCameraPanel, m_SmallCameraPanel, m_MainActionsPanel, m_SmallActionsPanel, m_InfoLabel, m_StepInfoLabels
                , m_PlayButton, m_PauseButton, m_SlowMoButton, m_StepButton, m_MainMenuPanel, m_CreditsPanel, m_LanguagePanel, m_LinksPanel, m_IAPPanel, m_PlayButtonsPanel, m_MainMenuButton,
                m_OpenGripsButton, m_CloseGripsButton, m_GripsCircle, m_GripsButtonPrefab, m_WorldCamera, m_UICamera, m_HandTarget, m_GripsPanel, m_IAPInfoLabel, m_BusyIcon, m_PurchaseButton, m_RestoreButton,
                m_CameraPanelMaterial, m_GripsNumberPrefab, m_InstructionsPanel, m_InfoDraggablePanel);

            //m_VerticalLabel.text = LanguageController.Instance.GetValueForKey("FakeChange");

            var localPos = m_VerticalLabel.transform.localPosition;
            localPos.z = -0.5f;
            m_VerticalLabel.transform.localPosition = localPos;

            var dataKeys = MetaDataController.Instance.GetOrderedDataKeys();
            // TODO : Check if button has Purchase ID
            // If it does, check if purchase is made (will be in IAP Controller)
            // If purchase is made, leave as regular button
            // Else, change colour scheme and UIButtonMessage target function to LockedActionButtonPressed
            for (int i = 0; i < dataKeys.Length; i++)
            {
                var go = (GameObject)Instantiate(m_ActionButtonPrefab);
                go.name = i + "|" + dataKeys[i]; // We put the number first so that UITable sorts in the correct order for us.
                var button = go.GetComponent<UIButton>();
                m_ActionListButtons.Add(dataKeys[i], button);
                var label = go.GetComponentInChildren<UILabel>();
                label.text = MetaDataController.Instance.GetLocalizedName(dataKeys[i]);
                var msg = go.GetComponent<UIButtonMessage>();
                msg.target = gameObject;
                var background = go.GetComponentInChildren<UISprite>();
                background.transform.localScale = new Vector3(m_ActionButtonWidth, m_ActionButtonLineHeight * label.relativeSize.y, 1f);
                var coll = (BoxCollider)go.collider;
                coll.size = new Vector3(m_ActionButtonWidth, m_ActionButtonLineHeight * label.relativeSize.y, 0f);
                var drag = go.GetComponent<UIDragPanelContents>();
                drag.draggablePanel = m_ActionButtonsDragPanel;
                go.transform.parent = m_ActionsTable.transform;
                go.transform.localPosition = Vector3.zero;
                go.transform.localScale = Vector3.one;
                // Highlight the first button.
                if(i == 0)
                {
                    // This is how we are highlighting the buttons.
                    button.isEnabled = false;
                }
            }
            UICamera.fallThrough = gameObject;
			
			SetActionButtonLockedState();

            // These will refresh the button states when we perform a purchase or restore.
            IAPController.Instance.PurchaseEvent += (b) => { if (b) { SetActionButtonLockedState(); } };
            IAPController.Instance.RestoreCompletedEvent += SetActionButtonLockedState;

            LanguageController.Instance.NewLanguageSetEvent += LanguageChanged;
        }

        private void LanguageChanged()
        {
            m_ActionsDraggablePanel.ResetPosition();
            string selectedKey = "";
            foreach (var kvp in m_ActionListButtons)
            {
                var label = kvp.Value.GetComponentInChildren<UILabel>();
                label.text = MetaDataController.Instance.GetLocalizedName(kvp.Key);

                var background = kvp.Value.GetComponentInChildren<UISprite>();
                background.transform.localScale = new Vector3(m_ActionButtonWidth, m_ActionButtonLineHeight * label.relativeSize.y, 1f);

                var coll = (BoxCollider)kvp.Value.collider;
                coll.size = new Vector3(m_ActionButtonWidth, m_ActionButtonLineHeight * label.relativeSize.y, 0f);

                var button = kvp.Value.GetComponent<UIButton>();
                if(!button.isEnabled)
                    selectedKey = kvp.Key;
            }


            m_MainActionsPanel.Refresh();
            
            m_ActionsTable.Reposition();

            foreach (var kvp in m_ActionListButtons)
            {
                var offset = kvp.Value.GetComponent<UIButtonOffset>();
                offset.ResetmPos();
            }

            m_VerticalLabel.text = MetaDataController.Instance.GetLocalizedName(selectedKey);
        }


        void Start()
        {
            // We kick start with a fake button press.
            // The function only uses the name of the button
            ActionButtonPressed(new GameObject(m_DefaultStartingButton));
        }

        private void OnDestroy()
        {
            if (LanguageController.Exists)
                LanguageController.Instance.NewLanguageSetEvent -= LanguageChanged;
        }
		
		private void SetActionButtonLockedState()
		{
			var dataKeys = MetaDataController.Instance.GetOrderedDataKeys();
            var unlockedPurchases = IAPController.Instance.GetPurchases();
			
			for( int i = 0; i < dataKeys.Length; i++ )
			{
				var data = MetaDataController.Instance.GetData( dataKeys[i] );
				var button = m_ActionListButtons[dataKeys[i]];
				
#if TEST_FLIGHT
                if(false)
#elif UNITY_IPHONE
				if( data.IAPID != "" )
#elif UNITY_ANDROID
                if( data.AndroidIAPID != "" )
#else
                if( data.IAPID != "" )
#endif
				{
                    var unlocked = false;
                    for (int j = 0; j < unlockedPurchases.Length; j++)
                    {
#if UNITY_IPHONE
                        if(unlockedPurchases[j] == data.IAPID)
#elif UNITY_ANDROID     
                        if (unlockedPurchases[j] == data.AndroidIAPID)
#else
                        // We just use iOS for testing on other platforms
                        if(unlockedPurchases[j] == data.IAPID)
#endif
                        {
                            unlocked = true;
                        }
                    }
					// If purchase is valid: Set buttons to normal function
					if( unlocked )
					{
                        SetupUnlockedButton(button);
					}
					// Else, lock 'em down
					else
					{
                        // Set up locked button
						button.GetComponent<UIButtonMessage>().functionName = "LockedActionButtonPressed"; 
                        button.defaultColor = m_ActionButtonLockedBackgroundColour;
						button.GetComponentInChildren<UISprite>().color = m_ActionButtonLockedBackgroundColour;
                        button.GetComponentInChildren<UILabel>().color = m_ActionButtonLockedTextColour;
						button.hover = m_ActionButtonLockedHoverColour;
					}
				}
				else
				{
                    SetupUnlockedButton(button);
				}
			}
		}

        private void SetupUnlockedButton(UIButton button)
        {
            button.GetComponent<UIButtonMessage>().functionName = "ActionButtonPressed";

            if (button.isEnabled)
            {
                button.GetComponentInChildren<UISprite>().color = m_ActionButtonUnlockedBackgroundColour;
            }
            else
            {
                button.GetComponentInChildren<UISprite>().color = button.disabledColor;
            }
            button.hover = m_ActionButtonUnlockedHoverColour;
            button.GetComponentInChildren<UILabel>().color = m_ActionButtonUnlockedTextColour;
        }

        #region Button Pressed Functions And Events
        // Camera Panel events
        public event Action CameraPanelCrossPressedEvent;
        public event Action CameraPanelHighPressedEvent;
        public event Action CameraPanelMidPressedEvent;
        public event Action CameraPanelLowPressedEvent;
        public event Action CameraPanelNorthPressedEvent;
        public event Action CameraPanelSouthPressedEvent;
        public event Action CameraPanelEastPressedEvent;
        public event Action CameraPanelWestPressedEvent;
        public event Action ShowCameraPanelPressedEvent;

        // Camera Panel Functions
        private void CameraPanelCrossPressed() { if (CameraPanelCrossPressedEvent != null) CameraPanelCrossPressedEvent(); }
        private void CameraPanelHighPressed()
        {
            if (CameraPanelHighPressedEvent != null)
                CameraPanelHighPressedEvent();

            SetCameraHeightButtonsHighlight(true, false, false);
        }

        private void CameraPanelMidPressed()
        {
            if (CameraPanelMidPressedEvent != null)
                CameraPanelMidPressedEvent();

            SetCameraHeightButtonsHighlight(false, true, false);
        }

        private void CameraPanelLowPressed()
        {
            if (CameraPanelLowPressedEvent != null)
                CameraPanelLowPressedEvent();

            SetCameraHeightButtonsHighlight(false, false, true);
        }

        private void CameraPanelNorthPressed()
        {
            if (CameraPanelNorthPressedEvent != null)
                CameraPanelNorthPressedEvent();

            SetCameraDirectionButtonsHighlight(true, false, false, false);
        }

        private void CameraPanelSouthPressed()
        {
            if (CameraPanelSouthPressedEvent != null)
                CameraPanelSouthPressedEvent();

            SetCameraDirectionButtonsHighlight(false, true, false, false);
        }

        private void CameraPanelEastPressed()
        {
            if (CameraPanelEastPressedEvent != null)
                CameraPanelEastPressedEvent();

            SetCameraDirectionButtonsHighlight(false, false, true, false);
        }

        private void CameraPanelWestPressed()
        {
            if (CameraPanelWestPressedEvent != null)
                CameraPanelWestPressedEvent();

            SetCameraDirectionButtonsHighlight(false, false, false, true);
        }

        private void ShowCameraPanelPressed() { if (ShowCameraPanelPressedEvent != null) ShowCameraPanelPressedEvent(); }

        // Actions Panel Events
        public event Action ShowActionPanelPressedEvent;
        public event Action ShowInfoButtonPressedEvent;
        public event Action ShowTipsButtonPressedEvent;

        // Actions Panel Functions
        private void ShowActionPanelPressed() { if (ShowActionPanelPressedEvent != null) ShowActionPanelPressedEvent(); }
        private void ShowInfoButtonPressed() { if (ShowInfoButtonPressedEvent != null) ShowInfoButtonPressedEvent(); }
        private void ShowTipsButtonPressed() { if (ShowTipsButtonPressedEvent != null) ShowTipsButtonPressedEvent(); }

        // Static Button Events
        public event Action MainMenuButtonPressedEvent;

        // Static Button Functions
        private void MainMenuButtonPressed()
        {
            if (MainMenuButtonPressedEvent != null)
                MainMenuButtonPressedEvent();
        }

        // Play control Events
        public event Action PlayButtonPressedEvent;
        public event Action PauseButtonPressedEvent;
        public event Action SlowMoButtonPressedEvent;
        public event Action StepButtonPressedEvent;
        public event Action RewindButtonPressedEvent;

        // Play Control Functions
        private void PlayButtonPressed() { if (!m_CameraFade.IsFading && PlayButtonPressedEvent != null) PlayButtonPressedEvent(); }
        private void PauseButtonPressed() { if (!m_CameraFade.IsFading && PauseButtonPressedEvent != null) PauseButtonPressedEvent(); }
        private void SlowMoButtonPressed() { if (!m_CameraFade.IsFading && SlowMoButtonPressedEvent != null) SlowMoButtonPressedEvent(); }
        private void StepButtonPressed() { if (!m_CameraFade.IsFading && StepButtonPressedEvent != null) { StepButtonPressedEvent(); } }
        private void RewindButtonPressed() { if (!m_CameraFade.IsFading && RewindButtonPressedEvent != null) { RewindButtonPressedEvent(); } }

        // Action button events
        public event StringDelegate ActionButtonPressedEvent;
        public event Action<GameObject> LockedActionButtonPressedEvent;

        // Action button functions
        private void ActionButtonPressed(GameObject go)
        {
            var nameSplit = go.name.Split('|');
            if (ActionButtonPressedEvent != null)
                ActionButtonPressedEvent(nameSplit[nameSplit.Length - 1]);

            foreach (var kvp in m_ActionListButtons)
            {
                kvp.Value.isEnabled = nameSplit[nameSplit.Length - 1] != kvp.Key;
            }

            // Set the vertical button's label text
            m_VerticalLabel.text = MetaDataController.Instance.GetLocalizedName(nameSplit[nameSplit.Length - 1]);
        }

        private void LockedActionButtonPressed(GameObject go)
        {
            if (LockedActionButtonPressedEvent != null)
                LockedActionButtonPressedEvent(go);
        }

        // Main Menu Button events
        public event Action TrainButtonPressedEvent;
        public event Action LanguageButtonPressedEvent;
        public event Action CreditsButtonPressedEvent;
        public event Action LinksButtonPressedEvent;
		public event Action IAPButtonPressedEvent;
        public event Action InstructionsButtonPressedEvent;

        // Main Menu button functions
        private void TrainButtonPressed()
        {
            if (TrainButtonPressedEvent != null)
                TrainButtonPressedEvent();
        }

        private void LanguageButtonPressed()
        {
            if (LanguageButtonPressedEvent != null)
                LanguageButtonPressedEvent();
        }

        private void CreditsButtonPressed()
        {
            if (CreditsButtonPressedEvent != null)
                CreditsButtonPressedEvent();
        }

        private void LinksButtonPressed()
        {
            if (LinksButtonPressedEvent != null)
                LinksButtonPressedEvent();
        }
		
		private void IAPButtonPressed()
        {
            if (IAPButtonPressedEvent != null)
                IAPButtonPressedEvent();
        }

        private void InstructionsButtonPressed()
        {
            if (InstructionsButtonPressedEvent != null)
                InstructionsButtonPressedEvent();
        }

        // Language Button Events
        public event Action DeviceButtonPressedEvent;
        public event Action EnglishButtonPressedEvent;
        public event Action FrenchButtonPressedEvent;
        public event Action SpanishButtonPressedEvent;
        public event Action LanguageBackButtonPressedEvent;

        // Language button functions
        private void DeviceButtonPressed()
        {
            if (DeviceButtonPressedEvent != null)
                DeviceButtonPressedEvent();
        }

        private void EnglishButtonPressed()
        {
            if (EnglishButtonPressedEvent != null)
                EnglishButtonPressedEvent();
        }

        private void FrenchButtonPressed()
        {
            if (FrenchButtonPressedEvent != null)
                FrenchButtonPressedEvent();
        }

        private void SpanishButtonPressed()
        {
            if (SpanishButtonPressedEvent != null)
                SpanishButtonPressedEvent();
        }

        private void LanguageBackButtonPressed()
        {
            if (LanguageBackButtonPressedEvent != null)
                LanguageBackButtonPressedEvent();
        }

        // Links button events
        public event Action MotionEdgeSiteButtonPressedEvent;
        public event Action MotionEdgeFBButtonPressedEvent;
        public event Action TransmediaSiteButtonPressedEvent;
        public event Action LinksBackButtonPressedEvent;

        // Links button functions
        private void MotionEdgeSiteButtonPressed()
        {
            if (MotionEdgeSiteButtonPressedEvent != null)
                MotionEdgeSiteButtonPressedEvent();
        }

        private void MotionEdgeFBButtonPressed()
        {
            if (MotionEdgeFBButtonPressedEvent != null)
                MotionEdgeFBButtonPressedEvent();
        }

        private void TransmediaSiteButtonPressed()
        {
            if (TransmediaSiteButtonPressedEvent != null)
                TransmediaSiteButtonPressedEvent();
        }

        private void LinksBackButtonPressed()
        {
            if (LinksBackButtonPressedEvent != null)
                LinksBackButtonPressedEvent();
        }

        // Credits button Events
        public event Action CreditsBackButtonPressedEvent;

        // Credits button Functions
        private void CreditsBackButtonPressed()
        {
            if (CreditsBackButtonPressedEvent != null)
                CreditsBackButtonPressedEvent();
        }
		
		// IAP Button Events
		public event Action PurchaseButtonPressedEvent;
		public event Action RestoreButtonPressedEvent;
		public event Action IAPBackButtonPressedEvent;
		
		// IAP Button Functions
		private void PurchaseButtonPressed()
		{
			if( PurchaseButtonPressedEvent != null)
				PurchaseButtonPressedEvent();
		}
		
		private void RestoreButtonPressed()
		{
			if( RestoreButtonPressedEvent != null)
				RestoreButtonPressedEvent();
		}
		
		private void IAPBackButtonPressed()
        {
            if (IAPBackButtonPressedEvent != null)
                IAPBackButtonPressedEvent();
        }

        // Instructions Button Events
        public event Action InstructionsBackButtonPressedEvent;

        // Instructions Button Functions
        private void InstructionsBackButtonPressed()
        {
            if (InstructionsBackButtonPressedEvent != null)
                InstructionsBackButtonPressedEvent();
        }
        
        public static event Vector2Delegate FallthroughDragEvent;

        // Catches drag events not used by UICamera
        private void OnDrag(Vector2 vec)
        {
            if (FallthroughDragEvent != null)
                FallthroughDragEvent(vec);
        }

        public static event Action GripsOpenButtonPressedEvent;

        private void GripsOpenButtonPressed()
        {
            if (GripsOpenButtonPressedEvent != null)
                GripsOpenButtonPressedEvent();
        }

        public static event Action GripsCloseButtonPressedEvent;

        private void GripsCloseButtonPressed()
        {
            if (GripsCloseButtonPressedEvent != null)
                GripsCloseButtonPressedEvent();
        }

        public static event GameObjectDelegate GripButtonPressedEvent;

        private void GripButtonPressed(GameObject go)
        {
            if (GripButtonPressedEvent != null)
                GripButtonPressedEvent(go);
        }

        #endregion

        private void SetCameraHeightButtonsHighlight(bool high, bool mid, bool low)
        {
            m_CameraHighButton.isEnabled = !high;
            m_CameraMidButton.isEnabled = !mid;
            m_CameraLowButton.isEnabled = !low;

            if (!m_CamHeightRegistered)
            {
                FallthroughDragEvent += CameraHeightButtonSwipe;
                m_CamHeightRegistered = true;
            }
        }

        private bool m_CamHeightRegistered = false;
        private void CameraHeightButtonSwipe(Vector2 vec)
        {
            if (vec.y != 0f)
            {
                m_CameraHighButton.isEnabled = true;
                m_CameraMidButton.isEnabled = true;
                m_CameraLowButton.isEnabled = true;
                FallthroughDragEvent -= CameraHeightButtonSwipe;
                m_CamHeightRegistered = false;
            }
        }

        private void SetCameraDirectionButtonsHighlight(bool north, bool south, bool east, bool west)
        {
            m_CameraNorthButton.isEnabled = !north;
            m_CameraSouthButton.isEnabled = !south;
            m_CameraEastButton.isEnabled = !east;
            m_CameraWestButton.isEnabled = !west;

            if (!m_CamDirectionRegistered)
            {
                FallthroughDragEvent += CamDirectionButtonSwipe;
                m_CamDirectionRegistered = true;
            }
        }

        private bool m_CamDirectionRegistered = false;
        private void CamDirectionButtonSwipe(Vector2 vec)
        {
            if (vec.x != 0f)
            {
                m_CameraNorthButton.isEnabled = true;
                m_CameraSouthButton.isEnabled = true;
                m_CameraEastButton.isEnabled = true;
                m_CameraWestButton.isEnabled = true;
                FallthroughDragEvent -= CamDirectionButtonSwipe;
                m_CamDirectionRegistered = false;
            }
        }
    }
}