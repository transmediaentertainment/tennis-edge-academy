using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using Holoville.HOTween;
using MotionEdge.UI.Engines;

namespace MotionEdge.UI.States
{
    public class MainMenuState : FSMState
    {
        private UIPanel m_MainMenuPanel;
        private MainMenuFSMEngine m_FSM;

        public MainMenuState( MainMenuFSMEngine fsm, UIPanel mainMenuPanel)
        {
            m_MainMenuPanel = mainMenuPanel;
            m_FSM = fsm;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            HOTween.To(m_MainMenuPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.OnScreenPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void Exit()
        {
            base.Exit();
            HOTween.To(m_MainMenuPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.RightOffScreenPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void LostFocus()
        {
            base.LostFocus();
            HOTween.To(m_MainMenuPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.LeftOffScreenPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void GainedFocus()
        {
            base.GainedFocus();
            HOTween.To(m_MainMenuPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.OnScreenPos).Ease(EaseType.EaseInOutQuad));
        }
    }
}
