using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.Engines;
using Holoville.HOTween;

namespace MotionEdge.UI.States
{
    public class ExternalLinksState : SubMainMenuState
    {
        public ExternalLinksState(UIController controller, MainMenuFSMEngine fsm, UIPanel linksPanel)
            : base(fsm, linksPanel)
        {
            controller.MotionEdgeFBButtonPressedEvent += MotionEdgeFBButtonPressed;
            controller.MotionEdgeSiteButtonPressedEvent += MotionEdgeSiteButtonPressed;
            controller.TransmediaSiteButtonPressedEvent += TransmediaSiteButtonPressed;
        }

        private void MotionEdgeSiteButtonPressed()
        {
            Application.OpenURL("http://motionedgeacademy.com");
        }

        private void MotionEdgeFBButtonPressed()
        {
			if (Application.platform == RuntimePlatform.Android)
                Application.OpenURL("fb://page/491552474254679");
			else if (Application.platform == RuntimePlatform.IPhonePlayer)
                Application.OpenURL("fb://profile/491552474254679");
            else
                Application.OpenURL("https://www.facebook.com/TennisEdgeAcademy");
        }

        private void TransmediaSiteButtonPressed()
        {
            Application.OpenURL("http://transmedia-entertainment.com");
        }
    }
}
