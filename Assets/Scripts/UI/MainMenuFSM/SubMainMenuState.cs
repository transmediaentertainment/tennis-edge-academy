using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using Holoville.HOTween;
using MotionEdge.UI.Engines;

namespace MotionEdge.UI.States
{
    public class SubMainMenuState : FSMState
    {
        protected UIPanel m_Panel;
        protected MainMenuFSMEngine m_FSM;

        public SubMainMenuState(MainMenuFSMEngine fsm, UIPanel panel)
        {
            m_Panel = panel;
            m_FSM = fsm;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);
            HOTween.To(m_Panel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.OnScreenPos).Ease(EaseType.EaseInOutQuad));
        }

        public override void Exit()
        {
            base.Exit();
            HOTween.To(m_Panel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.RightOffScreenPos).Ease(EaseType.EaseInOutQuad));
        }
    }
}
