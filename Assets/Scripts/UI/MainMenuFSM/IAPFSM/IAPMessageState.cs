﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using TransTech.System.Debug;

namespace MotionEdge.UI.States
{
    public class IAPMessageState : FSMState
    {
        private UILabel m_MessageLabel;
        private BusyIconAnimator m_BusyIcon;

        public IAPMessageState(UILabel messageLabel, BusyIconAnimator busyIcon)
        {
            m_MessageLabel = messageLabel;
            m_BusyIcon = busyIcon;

            m_MessageLabel.gameObject.SetActive(false);
            if (m_BusyIcon.IsAnimating)
                m_BusyIcon.StopAnimating();
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            // Requires a string and a boolean
            if (args == null || args.Length != 2 || !(args[0] is string) || !(args[1] is bool))
            {
                TTDebug.LogError("IAPMessageState.cs : Invalid arguments passed into enter function");
                return;
            }

            m_MessageLabel.gameObject.SetActive(true);
            m_MessageLabel.text = (string)args[0];

            if ((bool)args[1])
            {
                if (!m_BusyIcon.IsAnimating)
                    m_BusyIcon.BeginAnimating();
            }
        }

        public override void Exit()
        {
            base.Exit();

            m_MessageLabel.gameObject.SetActive(false);
            if(m_BusyIcon.IsAnimating)
                m_BusyIcon.StopAnimating();
        }
    }
}
