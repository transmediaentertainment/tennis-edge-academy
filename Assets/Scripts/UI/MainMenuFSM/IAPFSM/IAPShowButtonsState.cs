﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using TransTech.System.Debug;

namespace MotionEdge.UI.States
{
    public class IAPShowButtonsState : FSMState
    {
        private UIButton m_PurchaseButton;
        private UIButton m_RestoreButton;

        public IAPShowButtonsState(UIButton purchaseButton, UIButton restoreButton)
        {
            m_PurchaseButton = purchaseButton;
            m_RestoreButton = restoreButton;

            m_PurchaseButton.gameObject.SetActive(false);
            m_RestoreButton.gameObject.SetActive(false);
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            if (args == null || args.Length != 1 || !(args[0] is string))
            {
                TTDebug.LogError("IAPShowButtonsState.cs : Invalid arguments passed into Enter function");
                return;
            }

            m_PurchaseButton.gameObject.SetActive(true);
            m_RestoreButton.gameObject.SetActive(true);

            // In the future we will pass all pricing (multiple products) info through, but for now just the one
            var price = (string)args[0];
            m_PurchaseButton.GetComponentInChildren<UILabel>().text = "Purchase All Moves\n" + price;
        }

        public override void Exit()
        {
            base.Exit();

            m_PurchaseButton.gameObject.SetActive(false);
            m_RestoreButton.gameObject.SetActive(false);
        }
    }
}
