﻿using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.States;
using Holoville.HOTween;
using TransTech.System.Debug;
using MotionEdge.Language;

namespace MotionEdge.UI.Engines
{
    public class IAPFSMEngine : HFSMState
    {
        private IAPMessageState m_MessageState;
        private IAPShowButtonsState m_ShowButtonsState;
        private IdleFSMState m_IdleState;


        private MainMenuFSMEngine m_FSM;
        private UIPanel m_IAPPanel;
        private BusyIconAnimator m_BusyIcon;
        private UILabel m_InfoLabel;
        private UIButton m_PurchaseButton;
        private UIButton m_RestoreButton;

        public IAPFSMEngine(UIController controller, MainMenuFSMEngine fsm, UIPanel IAPPanel, UILabel iapInfoLabel,
            BusyIconAnimator busyIcon, UIButton purchaseButton, UIButton restoreButton)
        {
            m_MessageState = new IAPMessageState(iapInfoLabel, busyIcon);
            m_ShowButtonsState = new IAPShowButtonsState(purchaseButton, restoreButton);
            m_IdleState = new IdleFSMState();

            NewState(m_IdleState);

            m_FSM = fsm;
            m_IAPPanel = IAPPanel;

            controller.PurchaseButtonPressedEvent += () => Purchase();
			controller.RestoreButtonPressedEvent += () => Restore();
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            HOTween.To(m_IAPPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.OnScreenPos).Ease(EaseType.EaseInOutQuad));
       
            var iap = IAPController.Instance;
			
			iap.PurchaseEvent += PurchaseCompleted;

            if (CheckExistingPurchases())
                return;

            // If we haven't bought everything, check if it is initialized
            if (!iap.BillingChecked)
            {
                NewState(m_MessageState, LanguageController.Instance.GetValueForKey("IAPInitializing"), true);
                iap.BillingStatusEvent += BillingStatusEventFired;
                return;
            }

            if (!iap.BillingSupported)
            {
                SetBillingNotSupported();
                return;
            }

            // If we make it to here, get the product list.
            FetchProducts();
        }

        private void BillingStatusEventFired()
        {
            IAPController.Instance.BillingStatusEvent -= BillingStatusEventFired;
            // Check status
            if (IAPController.Instance.BillingChecked)
            {
                if (IAPController.Instance.BillingSupported)
                {
                    FetchProducts();
                }
                else
                {
                    SetBillingNotSupported();
                }
            }
            else
            {
                TTDebug.LogError("IAPFSMEngine.cs : Billing status event fired but the billing hasn't been checked!");
                SetBillingNotSupported();
            }
        }

        private void SetBillingNotSupported()
        {
            NewState(m_MessageState, LanguageController.Instance.GetValueForKey("IAPBillingNotSupported"), false);
        }

        private bool CheckExistingPurchases()
        {
            // Check for existing keys
            var iap = IAPController.Instance;
            var keys = iap.GetPurchases();
            for (int i = 0; i < keys.Length; i++)
            {
                Debug.Log("Checking key : " + keys[i]);
                if (keys[i] == iap.SKU)
                {
                    // We have purchased everything, so show the thankyou message
                    NewState(m_MessageState, LanguageController.Instance.GetValueForKey("IAPAlreadyPurchased"), false);
                    return true;
                }
            }
            return false;
        }

        private void FetchProducts()
        {
            if (IAPController.Instance.ProductDataAvailable)
            {
                Debug.Log("Showing products"); 
                ShowProducts();
            }
            else
            {
                IAPController.Instance.ProductDataEvent += ProductDataEventFired;
                NewState(m_MessageState, LanguageController.Instance.GetValueForKey("IAPFetchingInformation"), true);
                IAPController.Instance.GetProductData();
            }
        }

        private void ProductDataEventFired()
        {
            IAPController.Instance.ProductDataEvent -= ProductDataEventFired;
            if (IAPController.Instance.ProductDataAvailable)
            {
                ShowProducts();
            }
            else
            {
                NewState(m_MessageState, LanguageController.Instance.GetValueForKey("IAPInformationUnavailable"), false);
            }
        }

        private void ShowProducts()
        {
            NewState(m_ShowButtonsState, IAPController.Instance.LocalPrice);
        }

        public override void Exit()
        {
            base.Exit();
			
			IAPController.Instance.PurchaseEvent -= PurchaseCompleted;
			
            NewState(m_IdleState);

            HOTween.To(m_IAPPanel.transform, UIController.TweenTime, new TweenParms().Prop("localPosition", m_FSM.RightOffScreenPos).Ease(EaseType.EaseInOutQuad));
        }

        private void Purchase()
        {
            // Purchase Code

            NewState(m_MessageState, LanguageController.Instance.GetValueForKey("IAPContactingStore"), true);
            IAPController.Instance.PurchaseAllMoves();
        }

        private void PurchaseCompleted(bool success)
        {
            ;
            if (success)
            {
                NewState(m_MessageState, LanguageController.Instance.GetValueForKey("IAPPurchaseSuccessful"), false);
            }
            else
            {
                NewState(m_MessageState, LanguageController.Instance.GetValueForKey("IAPPurchaseError"), false);
            }
        }
		
		private void Restore()
		{
			// Restore Code
            NewState(m_MessageState, LanguageController.Instance.GetValueForKey("IAPRestoring"), true);
            IAPController.Instance.RestoreCompletedEvent += RestoreCompleted;
            IAPController.Instance.RestorePurchases();
            
		}

        private void RestoreCompleted()
        {
            IAPController.Instance.RestoreCompletedEvent -= RestoreCompleted;
            Debug.Log("Restore completed!");
            if (!CheckExistingPurchases())
            {
                Debug.Log("No existing purchases, Fetching Products");
                FetchProducts();
            }
        }
    }
}
