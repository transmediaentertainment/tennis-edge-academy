using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.UI.States;
using Holoville.HOTween;

namespace MotionEdge.UI.Engines
{
    public class MainMenuFSMEngine : HFSMState
    {
        private IdleFSMState m_IdleState;
        private MainMenuState m_MainMenuState;
        private LanguageSelectState m_LanguageState;
        private ExternalLinksState m_ExternalLinksState;
        private SubMainMenuState m_CreditsState;
		private IAPFSMEngine m_IAPState;
        private SubMainMenuState m_InstructionsState;

        public Vector3 OnScreenPos { get; private set; }
        public Vector3 LeftOffScreenPos { get; private set; }
        public Vector3 RightOffScreenPos { get; private set; }

        private Material m_CameraPlaneMat;
        private readonly Color m_FadeInColour = new Color(12f/255f, 49f/255f, 122f/255f, 156f/255f);

        public MainMenuFSMEngine(UIController controller, UIPanel mainMenuPanel, UIPanel creditsPanel,
            UIPanel languagePanel, UIPanel linksPanel, UIPanel IAPPanel, UIPanel instructionsPanel, UILabel iapInfoLabel, BusyIconAnimator busyIcon,
            UIButton purchaseButton, UIButton restoreButton, Material cameraPlaneMat)
        {
            OnScreenPos = Vector3.zero;
            LeftOffScreenPos = new Vector3(-1300f, 0f);
            RightOffScreenPos = new Vector3(1300f, 0f);

            m_CameraPlaneMat = cameraPlaneMat;
            m_CameraPlaneMat.color = m_FadeInColour;

            // Create states
            m_IdleState = new IdleFSMState();
            m_MainMenuState = new MainMenuState(this, mainMenuPanel);
            m_LanguageState = new LanguageSelectState(controller, this, languagePanel);
            m_ExternalLinksState = new ExternalLinksState(controller, this, linksPanel);
            m_CreditsState = new SubMainMenuState(this, creditsPanel);
			m_IAPState = new IAPFSMEngine(controller, this, IAPPanel, iapInfoLabel, busyIcon, purchaseButton, restoreButton);
            m_InstructionsState = new SubMainMenuState(this, instructionsPanel);

            // Register for events
            controller.LinksButtonPressedEvent += SetLinksState;
            controller.LanguageButtonPressedEvent += SetLanguageState;
            controller.CreditsButtonPressedEvent += SetCreditsState;
			controller.IAPButtonPressedEvent += SetIAPState;
            controller.InstructionsButtonPressedEvent += SetInstructionsState;

            controller.LinksBackButtonPressedEvent += PopSecondaryMenu;
            controller.LanguageBackButtonPressedEvent += PopSecondaryMenu;
            controller.CreditsBackButtonPressedEvent += PopSecondaryMenu;
			controller.IAPBackButtonPressedEvent += PopSecondaryMenu;
            controller.InstructionsBackButtonPressedEvent += PopSecondaryMenu;

            NewState(m_MainMenuState);
        }

        public void SetLanguageState()
        {
            if (Peek() == m_MainMenuState)
                PushState(m_LanguageState);
        }

        public void SetLinksState()
        {
            if (Peek() == m_MainMenuState)
                PushState(m_ExternalLinksState);
        }

        public void SetCreditsState()
        {
            if (Peek() == m_MainMenuState)
                PushState(m_CreditsState);
        }
		
		public void SetIAPState()
        {
            if (Peek() == m_MainMenuState)
                PushState(m_IAPState);
        }

        public void SetInstructionsState()
        {
            if (Peek() == m_MainMenuState)
                PushState(m_InstructionsState);
        }

        public void PopSecondaryMenu()
        {
            var top = Peek();
            // We check to see if we are in one of the secondary states
            if (top != m_MainMenuState && top != m_IdleState)
                PopState();
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);
            HOTween.To(m_CameraPlaneMat, 0.2f, "color", m_FadeInColour);
            NewState(m_MainMenuState);
        }

        public override void Exit()
        {
            base.Exit();
            HOTween.To(m_CameraPlaneMat, 0.2f, "color", Color.clear);
            NewState(m_IdleState);
        }
    }
}
