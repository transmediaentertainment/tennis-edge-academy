using UnityEngine;
using System.Collections;
using TransTech.FiniteStateMachine;
using MotionEdge.Language;
using MotionEdge.UI.Engines;
using Holoville.HOTween;

namespace MotionEdge.UI.States
{
    public class LanguageSelectState : SubMainMenuState
    {

        public LanguageSelectState(UIController controller, MainMenuFSMEngine fsm, UIPanel languagePanel)
            : base(fsm, languagePanel)
        {
            controller.DeviceButtonPressedEvent += () => SetLanguageAndReturn(LanguageType.NotSet);
            controller.EnglishButtonPressedEvent += () => SetLanguageAndReturn(LanguageType.EnglishUS);
            controller.FrenchButtonPressedEvent += () => SetLanguageAndReturn(LanguageType.French);
            controller.SpanishButtonPressedEvent += () => SetLanguageAndReturn(LanguageType.Spanish);
        }

        private void SetLanguageAndReturn(LanguageType lang)
        {
            LanguageController.Instance.SetLanguage(lang);
            m_FSM.PopSecondaryMenu();
        }
    }
}
