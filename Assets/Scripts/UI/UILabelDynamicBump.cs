using UnityEngine;
using System.Collections;

public class UILabelDynamicBump : MonoBehaviour 
{
    private UIDraggablePanel m_DragPanel;

    private IEnumerator Start()
    {
        m_DragPanel = (UIDraggablePanel)FindObjectOfType(typeof(UIDraggablePanel));
        m_DragPanel.onDragFinished += () => StartCoroutine(WaitAndBump());
        yield return null;
        yield return null;
        Bump();
    }

    private IEnumerator WaitAndBump()
    {
        yield return new WaitForEndOfFrame();
        Bump();
    }

    public void Bump()
    {
        transform.localPosition = new Vector3(0f, 0f, -1f);
    }
}
