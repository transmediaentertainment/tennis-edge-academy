// A texture is stretched over the entire screen. The color of the pixel is set each frame until it reaches its target color.

using UnityEngine;
using Holoville.HOTween;
using MotionEdge.UI.Engines;

public class CameraFade : MonoBehaviour
{   
	public GUIStyle m_BackgroundStyle = new GUIStyle();		// Style for background tiling
	public Texture2D m_FadeTexture;				// 1x1 pixel texture used for fading
	
	// modify script to use this GUITexture rather than a script instantiaited GUILabel (as was originally)
	public GUITexture CameraFadeGUITextureObject_GUITexture;
	
	// small register for if currently fading (I will use to prevent playback operation changes while fading)
	public bool IsFading { get { return IsFadingIn || IsFadingOut; } }
    public bool IsFadingIn { get { return m_FSM.IsFadingIn; } }
    public bool IsFadingOut { get { return m_FSM.IsFadingOut; } } 
    public Color m_CurrentColor;
	
	private CameraFadeFSM m_FSM;
    
	// initialize the texture, background-style and initial color:
	private void Awake()
	{		
		m_FadeTexture = new Texture2D(1, 1);        
        m_BackgroundStyle.normal.background = m_FadeTexture;
		
		// assign the fade texture to the GUITexture
		CameraFadeGUITextureObject_GUITexture.guiTexture.texture = m_FadeTexture;
		
		m_FSM = new CameraFadeFSM(m_FadeTexture);
		m_FSM.FadeIn();
	}

    public void FadeOut()
    {
		m_FSM.FadeOut();
    }
}
