// Handles revolving the camera
// Both using the switch buttons, or by swiping

using UnityEngine;
using System.Collections;
using MotionEdge.UI;

// Indicate that the camera is currently driving through the camera switch mechanism

public enum CameraMotor
{
	On,
	Off
}

public class CameraPivot : MonoBehaviour {
	
	private Transform myTransform;
	
	private float thresholdAngle = 30f;		// angle through which the stop condition can be caught on any frame through that arc
	
	private float lastTriggeredTime;			// an extra failsafe for stop condition
	private float failsafeTimePeriod = 1f;		// maximum time that the camera can revolve automatically. Failsafe.
	
	private float rotateSwipeSpeed = 12f;
	private float rotateSpeed = 500f;
	
	// communicate with GUI Control

	//private GameObject GUIControl;
	//private GUIControl GUIControlClass;
    public UIController m_UIController;
	
	private float snapShotAngle; // get the current angle, when a camera switch is triggered
	private float targetAngle; // angle which is the desired local angle, at time of camera switch
	
	private bool triggerShiftLeft;
	private bool triggerShiftRight;
	
	private float currentAngle;
	
	public float swipeDeltaHorizontal; // the value from swiping
	
	public float storeRotateDelta;
	
	private float rotateDegredation = 5f;
	
	public CameraMotor _cameraMotor;

    private bool m_TriggerCameraSwitch = false;

	void Start () {
		
		myTransform = transform;
		
		// Initialise communication

		//GUIControl = GameObject.FindGameObjectWithTag("GUIControl");
		//GUIControlClass = (GUIControl)GUIControl.GetComponent("GUIControl");
		
		// inits
		
		_cameraMotor = CameraMotor.Off;
		
		triggerShiftLeft = false;
		triggerShiftRight = false;
		
		swipeDeltaHorizontal = 0;

        //EasyTouch.On_Swipe += On_Swipe;
        //EasyTouch.On_SwipeEnd += On_SwipeEnd;
        UIController.FallthroughDragEvent += OnDrag;

        RegisterButtonPresses();
	}

    private void RegisterButtonPresses()
    {
        if (m_UIController == null)
            return;
        m_UIController.CameraPanelNorthPressedEvent += NorthPressed;
        m_UIController.CameraPanelSouthPressedEvent += SouthPressed;
        m_UIController.CameraPanelEastPressedEvent += EastPressed;
        m_UIController.CameraPanelWestPressedEvent += WestPressed;
    }

    private void DeregisterButtonPresses()
    {
        if (m_UIController == null)
            return;
        m_UIController.CameraPanelNorthPressedEvent -= NorthPressed;
        m_UIController.CameraPanelSouthPressedEvent -= SouthPressed;
        m_UIController.CameraPanelEastPressedEvent -= EastPressed;
        m_UIController.CameraPanelWestPressedEvent -= WestPressed;
    }

    private void On_Swipe(Gesture g)
    {
        swipeDeltaHorizontal = g.deltaPosition.x;
    }

    private void On_SwipeEnd(Gesture g)
    {
        swipeDeltaHorizontal = 0f;
    }

    private bool m_DidGetSwipe = false;

    private void OnDrag(Vector2 vec)
    {
        swipeDeltaHorizontal = vec.x;
        m_DidGetSwipe = true;
    }
	
	void Update () {

        if (m_DidGetSwipe)
        {
            m_DidGetSwipe = false;
        }
        else
        {
            swipeDeltaHorizontal = 0f;
        }
		// current angle of the pivot Y local rotation
		
		currentAngle = myTransform.localEulerAngles.y;
		
		// Stop conditions
		
		if (triggerShiftLeft)
		{
			if ((currentAngle < targetAngle) && (currentAngle > targetAngle - thresholdAngle))
			{
				triggerShiftLeft = false;
				_cameraMotor = CameraMotor.Off;
                RegisterButtonPresses();
			}
		}
		
		if (triggerShiftRight)
		{
			if ((currentAngle > targetAngle) && (currentAngle < targetAngle + thresholdAngle))
			{
				triggerShiftRight = false;
				_cameraMotor = CameraMotor.Off;
                RegisterButtonPresses();
			}
		}
		
		// special case stop condition for passing zero boundary (from zero to 359.99 rec)
		
		if (triggerShiftLeft)
		{
			if ((targetAngle == 0) && (currentAngle > (360f - thresholdAngle)))
			{
				triggerShiftLeft = false;
				_cameraMotor = CameraMotor.Off;
                RegisterButtonPresses();
			}
		}
		
		// Failsafe - if for any reason the device fails to update frame and catch stop condition, Stop the revolution.
		
		if (((triggerShiftLeft) || (triggerShiftRight)) && (Time.time > lastTriggeredTime + failsafeTimePeriod))
		{
			triggerShiftLeft = false;
			triggerShiftRight = false;
			_cameraMotor = CameraMotor.Off;
            RegisterButtonPresses();
		}
		
		// rotate
		
		if (triggerShiftLeft)
		{
			myTransform.Rotate(Vector3.up * Time.deltaTime * -1 * rotateSpeed, Space.World);
		}
		
		if (triggerShiftRight)
		{
			myTransform.Rotate(Vector3.up * Time.deltaTime * rotateSpeed, Space.World);
			
		}
		
		// Camera Switching is triggered from GUIControl
		
		if (m_TriggerCameraSwitch)
		{
			lastTriggeredTime = Time.time;
			
			// set the state
			
			_cameraMotor = CameraMotor.On;
			
			// get the current angle when triggered
			
			snapShotAngle = myTransform.localEulerAngles.y;
			
			// reset the trigger source

            m_TriggerCameraSwitch = false;
			
			// Find which is the target angle. Decide whether to turn left or right, to get there
			
			GetDesiredAngle();

            DeregisterButtonPresses();
		}
		
		// Camera Revolve calculations about pivot
		// Includes momentum
		
		if (storeRotateDelta != 0)
		{
			if (storeRotateDelta > 0)
				storeRotateDelta = storeRotateDelta - (Time.deltaTime * rotateDegredation * storeRotateDelta);
			else if (storeRotateDelta < 0)
				storeRotateDelta = storeRotateDelta + (Time.deltaTime * rotateDegredation * (-1f * storeRotateDelta));
		
		}
		
		if (swipeDeltaHorizontal != 0f){
			storeRotateDelta = swipeDeltaHorizontal;
			myTransform.Rotate(Vector3.up * Time.deltaTime * swipeDeltaHorizontal * rotateSwipeSpeed, Space.World);
		}
		else {
			
			myTransform.Rotate(Vector3.up * Time.deltaTime * storeRotateDelta * rotateSwipeSpeed, Space.World);
		}
		
		
	}
	
	private void GetDesiredAngle()
	{
				
		// magnitude the angle has to traverse to reach the target angle. On a scale from 0 to 360
		// Assume angles loop over boundaries 0, 360
		
		float shiftLeft = 0;
		float shiftRight = 0;
		
		// Calculate the angle value based on the camera switch button that has been pressed
		
		//if (GUIControlClass._cameraSwitchState == CameraSwitchState.Front) targetAngle = 0f;
		//if (GUIControlClass._cameraSwitchState == CameraSwitchState.Right) targetAngle = 90f;
		//if (GUIControlClass._cameraSwitchState == CameraSwitchState.Back) targetAngle = 180f;
		//if (GUIControlClass._cameraSwitchState == CameraSwitchState.Left) targetAngle = 270f;
		
		if (targetAngle > snapShotAngle)
		{
			shiftLeft = snapShotAngle + (360f - targetAngle);
			shiftRight = targetAngle - snapShotAngle;
		}
		
		if (targetAngle < snapShotAngle)
		{
			shiftLeft = snapShotAngle - targetAngle;
			shiftRight = (360f - snapShotAngle) + targetAngle;
		}
		
		if (shiftLeft <= shiftRight)	// note if reaching target is 180 degrees in both directions, default to 'left'.
		{
			triggerShiftLeft = true;
		}
			
		if (shiftRight < shiftLeft)
		{
			triggerShiftRight = true;
		}
	}

    private void NorthPressed()
    {
        m_TriggerCameraSwitch = true;
        targetAngle = 0f;
        GetDesiredAngle();
    }

    private void SouthPressed()
    {
        m_TriggerCameraSwitch = true;
        targetAngle = 180f;
        GetDesiredAngle();
    }

    private void EastPressed() // Right
    {
        m_TriggerCameraSwitch = true;
        targetAngle = 90f;
        GetDesiredAngle();
    }

    private void WestPressed() // Left
    {
        m_TriggerCameraSwitch = true;
        targetAngle = 270f;
        GetDesiredAngle();
    }
}
