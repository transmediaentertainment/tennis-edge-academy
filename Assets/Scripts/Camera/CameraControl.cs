// This class is for the camera control

using UnityEngine;
using System.Collections;
using TransTech.System.Debug;
using MotionEdge.UI;
using MotionEdge.UI.Engines;
using MotionEdge.Metadata;

namespace MotionEdge.Controls
{
    public class CameraControl : MonoBehaviour
    {
        // Coupla switches to turn on/off transforms for cam - for development
        private bool rotationOn = true;
        private bool positionOn = true;

        // holds the parameters for smoothing magnitude
        public float smoothPositionCam;
        public float smoothRotationCam;

        private Vector3 relativePosition; // Used for the Unity API LookRotation function. It wants a vector based on source and target position

        // cache the rigging transforms for the camera. Location and Target.

        private GameObject cameraLocation;
        private GameObject cameraTarget;

        private Transform myTransform;
        public Transform locationTransform;
        public Transform targetTransform;

        // parameters for camera adjustments
        [SerializeField]
        private float cameraHigh = 4f;
        [SerializeField]
        private float cameraMid = 2.1f;
        [SerializeField]
        private float cameraLow = 0.2f;

        private float cameraBack = 2.92f;
        private float cameraForwards = 2f;

        [SerializeField]
        private float cameraTargetHigh = 0.9f;
        [SerializeField]
        private float cameraTargetMid = 1.1f;
        [SerializeField]
        private float cameraTargetLow = 1.2f;

        public UIController m_Controller;

        // interface for CameraFade component attached to this game object

        public CameraFade CameraFadeComponent;

        // some parameters for camera fading

        public const float CameraFadeDuration = 0.25f;

        // parameter for the camera field of view. It's modified by pinch gestures on the mobile device

        public float cameraFOV = 50f;

        // Extents for FOV

        public float cameraFOVMinimum = 30f;
        public float cameraFOVMaximum = 50f;

        // The actual Camera component

        public Camera CameraComponent;

        // parameters for camera target adjustment

        public float percentage = 0f;

        // local store for verticle delta of swipe

        public float localSwipeVerticalDelta;

        // value to hold momentum

        public float swipeVerticalMomentum;

        // degredation

        public float swipeVerticalMomentumDegredation = 20f;

        // trigger camera elevation changes due to GUI buttons

        public bool triggerCameraElevationGUI = false;

        // Bit of a hack while converting/cleaning code
        private int m_ButtonPress = -1;

        public void HighButtonPressed()
        {
            m_ButtonPress = 2;
        }

        public void MidButtonPressed()
        {
            m_ButtonPress = 1;
        }

        public void LowButtonPressed()
        {
            m_ButtonPress = 0;
        }

        void Start()
        {
            if (m_Controller != null)
            {
                m_Controller.CameraPanelHighPressedEvent += HighButtonPressed;
                m_Controller.CameraPanelMidPressedEvent += MidButtonPressed;
                m_Controller.CameraPanelLowPressedEvent += LowButtonPressed;
                m_Controller.ActionButtonPressedEvent += ActionButtonPressed;
            }

            //EasyTouch.On_Swipe += On_Swipe;
            //EasyTouch.On_SwipeEnd += On_SwipeEnd;
            UIController.FallthroughDragEvent += OnDrag;

            // get the camera locator and target GameObjects

            //cameraLocation = GameObject.FindGameObjectWithTag("CameraLocation");
            //cameraTarget = GameObject.FindGameObjectWithTag("CameraTarget");

            // cache transforms

            myTransform = transform;
            //locationTransform = cameraLocation.transform;
            //targetTransform = cameraTarget.transform;

            // Initialise communication

            //GUIControl = GameObject.FindGameObjectWithTag("GUIControl");
            //GUIControlClass = (GUIControl)GUIControl.GetComponent("GUIControl");

            //	PlayerControl = GameObject.FindGameObjectWithTag("PlayerControl");
            //	PlayerControlClass = (PlayerControl)PlayerControl.GetComponent("PlayerControl");

            CameraFadeComponent = GetComponent<CameraFade>();

            // cache Camera component

            CameraComponent = GetComponent<Camera>();

            // set camera FOV initialise

            CameraComponent.fieldOfView = cameraFOV;

        }

        private bool m_DidGetDrag = false;

        private void OnDrag(Vector2 vec)
        {
            m_DidGetDrag = true;
            localSwipeVerticalDelta = -vec.y;
        }

        private string m_CurrentAnim;

        private void ActionButtonPressed(string anim)
        {
            if (anim == null)
            {
                TTDebug.LogError("AnimationControl.cs : No string passed in");
                return;
            }

            m_CurrentAnim = anim;
            RegisterForFadeOut();
        }

        private bool m_IsRegistered = false;

        private void RegisterForFadeOut()
        {
            if (m_IsRegistered)
                return;
            CameraFadeFSM.FadeOutCompleteEvent += SetCameraHeightsFromMetadata;
            m_IsRegistered = true;
        }

        private void SetCameraHeightsFromMetadata()
        {
            var data = MetaDataController.Instance.GetData(m_CurrentAnim);

            var pos = data.CamTargetLowMidHighPos;
            cameraTargetLow = pos.x;
            cameraTargetMid = pos.y;
            cameraTargetHigh = pos.z;

            pos = data.CamLowMidHighPos;
            cameraLow = pos.x;
            cameraMid = pos.y;
            cameraHigh = pos.z;

            CameraFadeFSM.FadeOutCompleteEvent -= SetCameraHeightsFromMetadata;
            m_IsRegistered = false;
        }

        void Update()
        {
            if (m_DidGetDrag)
            {
                m_DidGetDrag = false;
            }
            else
            {
                localSwipeVerticalDelta = 0f;
            }

            // BEGIN NOTE : This is not used at runtime? ---------------------------------------------------------------------------------------------------------------
            // set camera FOV dynamically

            CameraComponent.fieldOfView = cameraFOV;

            // Adjust camera targets to suit the change in FOV. For aesthetics.
            // This is just a slight tweak of parameters for appearance, that doesn't affect the rest of the camera logic

            float rangeFOV = cameraFOVMaximum - cameraFOVMinimum;
            percentage = (cameraFOV - cameraFOVMinimum) / rangeFOV;

            //if (GUIControlClass._cameraState == CameraState.Mid)
            // {
           // cameraTargetMid = 0.4f - (0.4f * (1 - percentage));
            //}

            //if (GUIControlClass._cameraState == CameraState.Low)
            //{
            //cameraTargetLow = 0.63f - (0.5f * (0.63f * (1 - percentage)));
            //}

            // END NOTE ------------------------------------------------------------------------------------------------------------------------------------------------------

            // CAMERA ELEVATION

            Vector3 location = locationTransform.localPosition;
            Vector3 locationTarget = targetTransform.localPosition;

            // Camera elevation adjusted, based on swipe

            if (localSwipeVerticalDelta > 0) // swiping up
            {
                if (location.y < cameraHigh)
                {
                    location.y = Mathf.Lerp(location.y, location.y + (localSwipeVerticalDelta * 0.01f), 1f);
                }
            }

            if (localSwipeVerticalDelta < 0) // swiping down
            {
                if (location.y > cameraLow)
                {
                    location.y = Mathf.Lerp(location.y, location.y + (localSwipeVerticalDelta * 0.01f), 1f);
                }
            }

            // no delta, but upwards momentum

            if ((localSwipeVerticalDelta == 0) && (swipeVerticalMomentum > 0))
            {
                if (location.y < cameraHigh)
                {
                    location.y = Mathf.Lerp(location.y, location.y + (swipeVerticalMomentum * 0.01f), 1f);
                }
            }

            // no delta, but downwards momentum

            if ((localSwipeVerticalDelta == 0) && (swipeVerticalMomentum < 0))
            {
                if (location.y > cameraLow)
                {
                    location.y = Mathf.Lerp(location.y, location.y + (swipeVerticalMomentum * 0.01f), 1f);
                }
            }

            // handle extents cases

            if (location.y > cameraHigh)
            {
                location.y = Mathf.Lerp(location.y, cameraHigh, 0.1f);
            }


            if (location.y < cameraLow)
            {
                location.y = Mathf.Lerp(location.y, cameraLow, 0.2f);
            }

            // store momentum

            if (localSwipeVerticalDelta != 0)
            {
                swipeVerticalMomentum = localSwipeVerticalDelta;
            }

            // momentum degredation

            if (swipeVerticalMomentum > 0)
                swipeVerticalMomentum = swipeVerticalMomentum - (Time.deltaTime * swipeVerticalMomentumDegredation * swipeVerticalMomentum);
            if (swipeVerticalMomentum < 0)
                swipeVerticalMomentum = swipeVerticalMomentum + (Time.deltaTime * swipeVerticalMomentumDegredation * (Mathf.Abs(swipeVerticalMomentum)));


            // CALCULATE new camera position z, based on the elevation (and the initial value parameters)
            // ALSO CALCULATE new camera target elevation, based on the elevation (and initial value parameters)

            if ((location.y > cameraLow) && (location.y < cameraMid))
            {
                location.z = MapValue(location.y, cameraLow, cameraMid, cameraBack, cameraBack);
                locationTarget.y = Mathf.Lerp(cameraTargetLow, cameraTargetMid, (location.y - cameraLow) / Mathf.Abs(cameraMid - cameraLow)); 
            }

            if ((location.y > cameraMid) && (location.y < cameraHigh))
            {
                location.z = MapValue(location.y, cameraMid, cameraHigh, cameraBack, cameraForwards);
                locationTarget.y = Mathf.Lerp(cameraTargetMid, cameraTargetHigh, (location.y - cameraMid) / Mathf.Abs(cameraHigh - cameraMid)); 
            }

            // Apply calculations to camera location object

            locationTransform.localPosition = location;

            // Apply calculations to the target location object

            targetTransform.localPosition = locationTarget; 


            // IF THE GUI BUTTONS ARE PRESSED FOR THE THREE CAMERA PRESETS .. apply them here

            if (m_ButtonPress >= 0)
            {
                switch (m_ButtonPress)
                {
                    case 0: // Low
                        // LOCATION
                        // Move camera down
                        location.y = cameraLow;
                        // camera forwards (closer to player)
                        location.z = cameraBack;
                        // Apply the transforms
                        locationTransform.localPosition = location;
                        // TARGET
                        locationTarget.y = cameraTargetLow;
                        // Apply the transforms
                        targetTransform.localPosition = locationTarget;
                        break;
                    case 1: // Mid
                        // LOCATION
                        location.y = cameraMid;
                        // move camera back
                        location.z = cameraBack;
                        // Apply the transforms
                        locationTransform.localPosition = location;
                        // TARGET
                        locationTarget.y = cameraTargetMid;
                        // Apply the transforms
                        targetTransform.localPosition = locationTarget;
                        break;
                    case 2: // High
                        // LOCATION
                        location.y = cameraHigh;
                        // move camera forward
                        location.z = cameraForwards;
                        // Apply the transforms
                        locationTransform.localPosition = location;
                        // TARGET
                        locationTarget.y = cameraTargetHigh;
                        // Apply the transforms
                        targetTransform.localPosition = locationTarget;
                        break;
                    default:
                        TTDebug.LogWarning("CameraControl.cs : Unknown button press : " + m_ButtonPress);
                        break;
                }
                m_ButtonPress = -1;
            }

            // Camera transforms
            if (positionOn)
            {
                // Set the cameras position based on the locator object (which I just parent to part of the player character)
                myTransform.position = Vector3.Slerp(myTransform.position, locationTransform.position, Time.deltaTime * smoothPositionCam);
            }

            if (rotationOn)
            {

                // The rotation calculation uses relative position, so calculate it here.

                relativePosition = (targetTransform.position - locationTransform.position);

                // Smooth cameras rotation, which tends towards a target

                //Quaternion rotation = Quaternion.Slerp(myTransform.rotation,Quaternion.LookRotation(relativePosition),smoothRotationCam);
                Quaternion rotation = Quaternion.LookRotation(relativePosition);
                myTransform.rotation = rotation;
            }

        }

        // Function to map a value in one range, to a corresponding value in another range.

        private float MapValue(float x, float a1, float a2, float b1, float b2)
        {
            float range1 = a2 - a1;
            float range2 = b2 - b1;

            float xNormalised = (x - a1) / range1;
            return b1 + (xNormalised * range2);
        }

        // NEW FUNCTIONS - JM

        public void FadeOut()
        {
            CameraFadeComponent.FadeOut();
        }
    }
}
