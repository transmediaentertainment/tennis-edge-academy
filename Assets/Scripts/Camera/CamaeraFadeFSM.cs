using TransTech.FiniteStateMachine;
using TransTech.Delegates;
using UnityEngine;
using MotionEdge.UI.States;

namespace MotionEdge.UI.Engines
{
    public class CameraFadeFSM : FSM
    {
        private CameraFadeState m_FadeInState;
        private CameraFadeState m_FadeOutState;
        private IdleFSMState m_IdleState;

        public static event VoidDelegate FadeOutCompleteEvent;
        public static event VoidDelegate FadeInCompleteEvent;

        public bool IsFadingIn { get { return Peek() == m_FadeInState; } }
        public bool IsFadingOut { get { return Peek() == m_FadeOutState; } }

        public CameraFadeFSM(Texture2D tex)
        {
            m_FadeInState = new CameraFadeState(tex, Color.black, Color.clear);
            m_FadeOutState = new CameraFadeState(tex, Color.clear, Color.black);
            m_IdleState = new IdleFSMState();

            m_FadeInState.FadeCompleteEvent += FadeInComplete;
            m_FadeOutState.FadeCompleteEvent += FadeOutComplete;

            NewState(m_IdleState);
        }

        private void FadeInComplete()
        {
            NewState(m_IdleState);
            if (FadeInCompleteEvent != null)
                FadeInCompleteEvent();
        }

        private void FadeOutComplete()
        {
            NewState(m_FadeInState);
            if (FadeOutCompleteEvent != null)
                FadeOutCompleteEvent();

        }

        public void FadeOut()
        {
            if (Peek() != m_FadeOutState)
                NewState(m_FadeOutState);
        }

        public void FadeIn()
        {
            if (Peek() != m_FadeInState)
                NewState(m_FadeInState);
        }
    }
}
