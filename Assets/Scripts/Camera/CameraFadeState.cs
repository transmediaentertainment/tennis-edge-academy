using TransTech.FiniteStateMachine;
using TransTech.Delegates;
using UnityEngine;
using MotionEdge.Controls;
using MotionEdge.UI.Engines;

namespace MotionEdge.UI.States
{
    public class CameraFadeState : FSMState
    {
        public event VoidDelegate FadeCompleteEvent;

        private float m_Timer;
        private Texture2D m_Tex;
        private Color m_CurrentColor;
        private Color m_StartColor;
        private Color m_EndColor;

        public CameraFadeState(Texture2D texture, Color startColor, Color endColor)
        {
            m_Tex = texture;
            m_StartColor = startColor;
            m_EndColor = endColor;
        }

        public override void Enter(params object[] args)
        {
            base.Enter(args);

            m_Timer = 0f;
        }

        public override void Update(float deltaTime)
        {
            base.Update(deltaTime);

            m_Timer += deltaTime;
            m_CurrentColor = Color.Lerp(m_StartColor, m_EndColor, Mathf.Clamp01(m_Timer / CameraControl.CameraFadeDuration));
            m_Tex.SetPixel(0, 0, m_CurrentColor);
            m_Tex.Apply();

            if (m_Timer >= CameraControl.CameraFadeDuration)
            {
                if (FadeCompleteEvent != null)
                    FadeCompleteEvent();
            }
        }
    }
}