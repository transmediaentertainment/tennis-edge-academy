using UnityEngine;
using System.Collections;

/// <summary>
/// Class to animate the network busy icon
/// </summary>
public class BusyIconAnimator : MonoBehaviour 
{
    public bool IsAnimating { get; private set; }
    private float m_CurrentRotation;
    
    public GameObject m_PlaneObject;
    private const float m_Speed = 0.08f;

    void Awake()
    {
        m_PlaneObject.renderer.enabled = false;
    }

    void OnSpawned()
    {
        BeginAnimating();
    }

    void OnDespawned()
    {
        StopAnimating();
    }

    public void BeginAnimating()
    {
        m_CurrentRotation = 0f;
        m_PlaneObject.renderer.enabled = true;
        IsAnimating = true;
        StartCoroutine(Animate());
    }

    public void StopAnimating()
    {
        IsAnimating = false;
        m_PlaneObject.renderer.enabled = false;
    }

    private IEnumerator Animate()
    {
        while (IsAnimating)
        {
            yield return new WaitForSeconds(m_Speed);
            m_CurrentRotation += 45f;
            transform.rotation = Quaternion.AngleAxis(m_CurrentRotation, Vector3.forward);
        }
    }
}
