using UnityEngine;
using System.Collections;
using UnityEditor;
using MotionEdge;
using MotionEdge.Language;

public class LanguageSelection : Editor
{
    [MenuItem("Edit/Motion Edge Language/English UK")]
    public static void SetEnglishUK()
    {
        PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.EnglishUK);
    }

    [MenuItem("Edit/Motion Edge Language/English US")]
    public static void SetEnglishUS()
    {
        PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.EnglishUS);
    }

    [MenuItem("Edit/Motion Edge Language/French")]
    public static void SetFrench()
    {
        PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.French);
    }

    [MenuItem("Edit/Motion Edge Language/Spanish")]
    public static void SetSpanish()
    {
        PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.Spanish);
    }

    [MenuItem("Edit/Motion Edge Language/Portugese")]
    public static void SetPortugese()
    {
        PlayerPrefs.SetInt(LanguageController.LanguagePref, (int)LanguageType.Portugues);
    }
}
