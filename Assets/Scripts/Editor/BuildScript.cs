﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class BuildScript : Editor 
{
    private readonly static string[] Levels = new string[] { "Assets/Scenes/LaunchScene.unity", "Assets/Scenes/MainScene.unity" };
    private const string iOSBuildPath = "XCode Project";
    private static string AndroidBuildPath = "Builds/Android/Tennis Edge Academy v" + PlayerSettings.bundleVersion + ".apk";


    [MenuItem("Tools/Transmedia/Build/Android Release")]
    public static void BuildAndroidRelease()
    {
        BuildAndroid(BuildOptions.AutoRunPlayer);
    }

    [MenuItem("Tools/Transmedia/Build/Android Debug")]
    public static void BuildAndroidDebug()
    {
        BuildAndroid(BuildOptions.AllowDebugging | BuildOptions.AutoRunPlayer | BuildOptions.ConnectWithProfiler | BuildOptions.Development);
    }

    private static void BuildAndroid(BuildOptions options)
    {
        var projectPath = Application.dataPath.Replace("/Assets", "");
        var path = Path.Combine(projectPath, "Builds");
        if(!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        path = Path.Combine(path, "Android");
        if(!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
        var streamingPath = Path.Combine(Application.dataPath, "StreamingAssets");
        if(!Directory.Exists(streamingPath))
            Directory.CreateDirectory(streamingPath);

        FileUtil.CopyFileOrDirectory(Path.Combine(Path.Combine(Application.dataPath, "Videos"), "TennisIntro.mp4"),Path.Combine(Path.Combine(Application.dataPath, "StreamingAssets"), "TennisIntro.mp4"));

        PlayerSettings.keyaliasPass = "plazmarelay";
        PlayerSettings.keystorePass = "plazmarelay";

        BuildPipeline.BuildPlayer(Levels, AndroidBuildPath, BuildTarget.Android, options);

        File.Delete(Path.Combine(streamingPath, "TennisIntro.mp4"));
        File.Delete(Path.Combine(streamingPath, "TennisIntro.mp4.meta"));
        Directory.Delete(streamingPath);
        File.Delete(streamingPath + ".meta");
    }

    [MenuItem("Tools/Transmedia/Build/iOS Release")]
    public static void BuildiOSRelease()
    {
        BuildiOS(BuildOptions.AutoRunPlayer | BuildOptions.AcceptExternalModificationsToPlayer);
    }

    [MenuItem("Tools/Transmedia/Build/iOS Debug")]
    public static void BuildiOSDebug()
    {
        BuildiOS(BuildOptions.AutoRunPlayer | BuildOptions.AllowDebugging | BuildOptions.AcceptExternalModificationsToPlayer | BuildOptions.ConnectWithProfiler | BuildOptions.Development);
    }

    private static void BuildiOS(BuildOptions options)
    {
        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            Debug.LogWarning("Can't build iOS on Windows");
            return;
        }

        var streamingPath = Path.Combine(Application.dataPath, "StreamingAssets");
        if (!Directory.Exists(streamingPath))
            Directory.CreateDirectory(streamingPath);

        FileUtil.CopyFileOrDirectory(Path.Combine(Path.Combine(Application.dataPath, "Videos"), "TennisIntro.mov"), Path.Combine(Path.Combine(Application.dataPath, "StreamingAssets"), "TennisIntro.mov"));

        BuildPipeline.BuildPlayer(Levels, iOSBuildPath, BuildTarget.iPhone, options);

        File.Delete(Path.Combine(streamingPath, "TennisIntro.mov"));
        File.Delete(Path.Combine(streamingPath, "TennisIntro.mov.meta"));
        Directory.Delete(streamingPath);
        File.Delete(streamingPath + ".meta");
    }
}
