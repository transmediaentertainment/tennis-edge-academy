﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using MotionEdge.Metadata;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
using System;
using System.Text;

namespace MotionEdge.Editors
{
    public static class MoveRefresh
    {
        [MenuItem("Window/Moves/Force Refresh Moves")]
        public static void ForceRefresh()
        {
            MetaDataController.Instance.LoadData();
        }
    }

    public static class EditorMovesLock
    {
        private const string m_PurchaseKey = "Purchases";
        private const string m_PurchaseSalt = "NHDOi40hasd";

        [MenuItem("Window/Moves/Unlock IAP Moves")]
        public static void UnlockAllMoves()
        {
            // NOTE : This is just copied from the IAP Controller, and will need 
            // to be updated if the IAP code changes
            var id = SystemInfo.deviceUniqueIdentifier;
            var pw = id + m_PurchaseSalt;
            if (pw.Length < 16)
            {
                while (pw.Length < 16)
                    pw = pw + "F";
            }
            else if (pw.Length > 16)
            {
                pw = pw.Substring(0, 16);
            }
            var combinedKeys = "tennis_test_purchase_1|com.motionedgeacademy.tennisedgeacademy.unlockalltennismoves";
            var encryptedKeys = Encryption.Encrypt(combinedKeys, pw);
            Debug.Log("Setting Player Prefs to : " + encryptedKeys);
            PlayerPrefs.SetString(m_PurchaseKey, encryptedKeys);
        }

        [MenuItem("Window/Moves/Lock IAP Moves")]
        public static void LockIAPMoves()
        {
            Debug.Log("Setting Player Prefs to : ");
            PlayerPrefs.SetString(m_PurchaseKey, "");
        }
    }

    public class MoveDataEditor : EditorWindow
    {
        private static MoveDataEditor m_Window;

        [MenuItem("Window/Moves/Move Data Editor")]
        public static void OpenWindow()
        {
            if (m_Window == null)
                m_Window = EditorWindow.GetWindow<MoveDataEditor>();
            m_Window.Show();
        }

        [MenuItem("Window/Moves/Export Moves to CSV")]
        public static void ExportMovesToCSV()
        {
            var folderPath = Path.Combine(Application.dataPath, "Resources");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            folderPath = Path.Combine(folderPath, "MoveDataSets");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
                AssetDatabase.Refresh();
            }

            var data = new Dictionary<string, MoveMetadata>();

            var files = Directory.GetFiles(folderPath);
            if (files == null || files.Length == 0)
            {
                data.Add("First Move", new MoveMetadata());
                data["First Move"].AddNewEmptyDataAtEnd();
            }
            else
            {
                var serializer = new XmlSerializer(typeof(MoveMetadata));
                foreach (var file in files)
                {
                    if (file.Contains(".meta") || file.Contains(".csv") || file.Contains(".xlsx") || file.Contains(".zip"))
                        continue;
                    var splits = file.Split('.', '\\', '/');
                    var k = splits[splits.Length - 2];
                    string encryptedString;
                    using (var textReader = new StreamReader(file))
                    {
                        encryptedString = textReader.ReadToEnd();
                        textReader.Close();
                    }
                    string xmlString;
                    try
                    {
                        xmlString = Encryption.Decrypt(encryptedString, Encryption.m_Salt);
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("Decryption error : " + k + " : " + e.Message);
                        // If we hit the exception, it is already decrypted, so just assign the value
                        xmlString = encryptedString;
                    }
                    using (var sr = new StringReader(xmlString))
                    {
                        MoveMetadata move = (MoveMetadata)serializer.Deserialize(sr);
                        data.Add(k, move);
                        sr.Close();
                    }
                }
            }

            
            var sb = new StringBuilder();
                //writer.Write("Data Key,English Name,French Name,Spanish Name,English Overview,French Overview,Spanish Overview,English Tips,French Tips,Spanish Tips\n");
            foreach (var kvp in data)
            {
                //writer.Write("{0},\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\"\n", kvp.Key, kvp.Value.EnglishName, kvp.Value.FrenchName, kvp.Value.SpanishName, kvp.Value.EnglishOverviewInfo,
                // kvp.Value.FrenchOverviewInfo, kvp.Value.SpanishOverviewInfo, kvp.Value.EnglishTipsInfo, kvp.Value.FrenchTipsInfo, kvp.Value.SpanishTipsInfo);
                var csvPath = Path.Combine(folderPath, kvp.Key + ".csv");
                using (var writer = new StreamWriter(csvPath, false, Encoding.UTF8))
                {
                    writer.Write("Key,Data\n");
                    writer.Write("{0},\"{1}\"\n", "English Name", kvp.Value.EnglishName);
                    writer.Write("{0},\"{1}\"\n", "French Name", kvp.Value.FrenchName);
                    writer.Write("{0},\"{1}\"\n", "Spanish Name", kvp.Value.SpanishName);
                    writer.Write("{0},\"{1}\"\n", "English Overview", kvp.Value.EnglishOverviewInfo);
                    writer.Write("{0},\"{1}\"\n", "French Overview", kvp.Value.FrenchOverviewInfo);
                    writer.Write("{0},\"{1}\"\n", "Spanish Overciew", kvp.Value.SpanishOverviewInfo);
                    writer.Write("{0},\"{1}\"\n", "English Tips", kvp.Value.EnglishTipsInfo);
                    writer.Write("{0},\"{1}\"\n", "French Tips", kvp.Value.FrenchTipsInfo);
                    writer.Write("{0},\"{1}\"\n", "Spanish Tips", kvp.Value.SpanishTipsInfo);
                    writer.Write("\n");
                    writer.Write("Pause Time,Node 1,Node 2,Grip 1, Grip 2,Grip 3,English 1,French 1,Spanish 1,English 2,French 2,Spanish 2\n");
                    sb.Length = 0;
                    for (int i = 0; i < kvp.Value.Nodes.Length; i++)
                    {
                        sb.Append(string.Format("{0}", kvp.Value.TriggerTimes[i]));
                        for (int j = 0; j < 2; j++)
                            AppendAtIndex<CurrentNodeType>(kvp.Value.Nodes[i], j, sb);
                        for (int j = 0; j < 3; j++)
                        {
                            //Debug.Log("Grips Length : " + kvp.Key + " : " + kvp.Value.Grips.Length + " i : " + i + " : " + kvp.Value.Nodes.Length);
                            AppendAtIndex<GripType>(kvp.Value.Grips[i], j, sb);
                        }
                        for (int j = 0; j < 2; j++)
                        {
                            AppendAtIndex<string>(kvp.Value.EnglishInfo[i], j, sb);
                            AppendAtIndex<string>(kvp.Value.FrenchInfo[i], j, sb);
                            AppendAtIndex<string>(kvp.Value.SpanishInfo[i], j, sb);
                        }
                        sb.Append("\n");
                    }
                    writer.Write(sb.ToString());
                    writer.Close();
                }
            }
            
            Debug.Log("Completed CSV Export");
        }

        private static void AppendAtIndex<T>(List<T> list, int index, StringBuilder sb)
        {
            if (index < list.Count)
                sb.Append(string.Format(",\"{0}\"", list[index]));
            else
                AppendEmpty(sb);
        }

        private static void AppendEmpty(StringBuilder sb)
        {
            sb.Append(",");
        }
        
        [MenuItem("Window/Moves/Decrypt Moves Files")]
        public static void DecryptMovesFiles()
        {
            var folderPath = Path.Combine(Application.dataPath, "Resources");
            folderPath = Path.Combine(folderPath, "MoveDataSets");
            var files = Directory.GetFiles(folderPath);
            foreach (var file in files)
            {
                Debug.Log("File : " + file);
                if (file.Contains(".meta") || file.Contains(".csv") || file.Contains(".xlsx") || file.Contains(".zip"))
                    continue;
                var splits = file.Split('.', '\\', '/');
                var k = splits[splits.Length - 2];
                Debug.Log("Loaded : " + k);
                using (var textReader = new StreamReader(file))
                {
					string encryptedString = textReader.ReadToEnd();
                    try
                    {
                        string xmlString = Encryption.Decrypt(encryptedString, Encryption.m_Salt);
                        using (var sr = new StringReader(xmlString))
                        {

                            sr.Close();
                            textReader.Close();
                        }
                        var path = Path.Combine(folderPath, k + ".txt");
                        using (var textWriter = new StreamWriter(path))
                        {
                            textWriter.Write(xmlString);
                            textWriter.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Log("Decryption Exception : " + e.Message);
                        continue;
                    }
                }
            }
            AssetDatabase.Refresh();
        }

        [MenuItem("Window/Moves/Encrypt Moves Files")]
        public static void EncryptMovesFiles()
        {
            var folderPath = Path.Combine(Application.dataPath, "Resources");
            folderPath = Path.Combine(folderPath, "MoveDataSets");
            var files = Directory.GetFiles(folderPath);
            foreach (var file in files)
            {
                Debug.Log("File : " + file);
                if (file.Contains(".meta") || file.Contains(".csv") || file.Contains(".xlsx") || file.Contains(".zip"))
                    continue;
                var splits = file.Split('.', '\\', '/');
                var k = splits[splits.Length - 2];
                Debug.Log("Loaded : " + k);
                string xmlString;
                using (var textReader = new StreamReader(file))
                {
                    xmlString = textReader.ReadToEnd();
                    textReader.Close();
                }
                try
                {
                    string encryptedString = Encryption.Encrypt(xmlString, Encryption.m_Salt);
                    var path = Path.Combine(folderPath, k + ".txt");
                    using (var textWriter = new StreamWriter(path))
                    {
                        textWriter.Write(encryptedString);
                        textWriter.Close();
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("Decryption Exception : " + e.Message);
                    continue;
                }
            }
            AssetDatabase.Refresh();
        }

        private bool m_IsInit = false;
        private Vector2 m_ScrollPos1;
        private Vector2 m_ScrollPos2;
        private Vector2 m_ScrollPos3;
        private Dictionary<string, MoveMetadata> m_Data;
        private string m_FolderPath;

        private string m_SelectedKey = "";
        private int m_CurrentStep;
        private string m_NewKey = "";

        private const float ColOneMaxWidth = 500f;
        private const float ColTwoMaxWidth = 150f;

        private void Init()
        {
            m_FolderPath = Path.Combine(Application.dataPath, "Resources");
            if (!Directory.Exists(m_FolderPath))
            {
                Directory.CreateDirectory(m_FolderPath);
            }
            m_FolderPath = Path.Combine(m_FolderPath, "MoveDataSets");
            if (!Directory.Exists(m_FolderPath))
            {
                Directory.CreateDirectory(m_FolderPath);
                AssetDatabase.Refresh();
            }

            m_Data = new Dictionary<string, MoveMetadata>();

            var files = Directory.GetFiles(m_FolderPath);
            if (files == null || files.Length == 0)
            {
                m_Data.Add("First Move", new MoveMetadata());
                m_SelectedKey = "First Move";
                m_Data[m_SelectedKey].AddNewEmptyDataAtEnd();
            }
            else
            {
                var serializer = new XmlSerializer(typeof(MoveMetadata));
                foreach (var file in files)
                {
                    if (file.Contains(".meta") || file.Contains(".csv") || file.Contains(".xlsx") || file.Contains(".zip"))
                        continue;
                    var splits = file.Split('.', '\\', '/');
                    var k = splits[splits.Length - 2];
                    if (m_SelectedKey == null || m_SelectedKey == "")
                        m_SelectedKey = k;
                    string encryptedString;
                    using (var textReader = new StreamReader(file))
                    {
						encryptedString = textReader.ReadToEnd();
                        textReader.Close();
                    }
                    string xmlString;
                    try
                    {
                        xmlString = Encryption.Decrypt(encryptedString, Encryption.m_Salt);
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("Decryption error : " + e.Message);
                        // If we hit the exception, it is already decrypted, so just assign the value
                        xmlString = encryptedString;
                    }
                    using (var sr = new StringReader(xmlString))
                    {
						try 
						{
                        	MoveMetadata move = (MoveMetadata)serializer.Deserialize(sr);
                        	m_Data.Add(k, move);
                        	//sr.Close();
						}
						catch(Exception e) 
						{
							Debug.LogWarning("Error deserializing : " + e.Message);
						}
						finally
						{
							sr.Close();
						}
                    }
                }
            }

            m_IsInit = true;
        }


        private void SaveMoves()
        {
            var serializer = new XmlSerializer(typeof(MoveMetadata));
            Debug.Log("Saving data...");
            foreach(var kvp in m_Data)
            {
                var path = Path.Combine(m_FolderPath, kvp.Key + ".txt");

                // Perform a serialization to check we don't have any issues
                var xmlString = kvp.Value.ToXmlString();
                bool serialized = false;
                using (var sr = new StringReader(xmlString))
                {
                    try
                    {
                        serializer.Deserialize(sr);
                        serialized = true;
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("File NOT Saved : " + kvp.Key + " : " + e.Message); 
                    }
                    finally
                    {
                        sr.Close();
                    }
                }
                if (serialized)
                {
                    using (var textWriter = new StreamWriter(path))
                    {
                        string encryptedString = Encryption.Encrypt(xmlString, Encryption.m_Salt);
                        textWriter.Write(encryptedString);
                        textWriter.Close();
                    }
                }
            }
            Debug.Log("Finished saving metadata.");

            AssetDatabase.Refresh();
        }

        private void OnGUI() 
        {
            if (!m_IsInit || m_Data == null)
                Init();

            EditorGUILayout.BeginHorizontal();// Initial Horizontal

            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(ColOneMaxWidth)); // Left scroll window

            m_ScrollPos1 = GUILayout.BeginScrollView(m_ScrollPos1);
            string removeKey = "";

            // First get all the ordered/unordered moves
            // -1 is the default value before an order value is given
            var orderedMoves = m_Data.Where(kv => kv.Value.MoveOrder != -1).ToList().OrderBy(kv => kv.Value.MoveOrder).ToList();
            var unorderedMoves = m_Data.Where(kv => kv.Value.MoveOrder == -1).ToList();

            // Order them all at the end.
            int lastMove = -1; // We set to -1 so that the numbering begins at 0
            if (orderedMoves.Count > 0)
            {
                lastMove = orderedMoves[orderedMoves.Count - 1].Value.MoveOrder;
            }
            for (int i = 0; i < unorderedMoves.Count; i++)
            {
                Debug.Log("Setting new Order for : " + unorderedMoves[i].Key + " : " + (lastMove + i + 1).ToString());
                unorderedMoves[i].Value.MoveOrder = lastMove + i + 1;
                orderedMoves.Add(unorderedMoves[i]);
            }

            if (m_Data != null)
            {
                for(int i = 0; i < orderedMoves.Count; i++)
                {
                    var kvp = orderedMoves[i];
                    GUILayout.BeginHorizontal();

                    if (GUILayout.Button("▲", GUILayout.MaxWidth(35f), GUILayout.MinWidth(35f)))
                    {
                        // We don't want it to go lower than zero
                        if (i > 0)
                        {
                            // We just switch the numbers, they will be resorted at the start of the loop again anyway
                            orderedMoves[i].Value.MoveOrder -= 1;
                            orderedMoves[i - 1].Value.MoveOrder += 1;
                        }
                    }

                    if (GUILayout.Button("▼", GUILayout.MaxWidth(35f), GUILayout.MinWidth(35f)))
                    {
                        // We don't want to go past the end
                        if (i < orderedMoves.Count - 2)
                        {
                            orderedMoves[i].Value.MoveOrder += 1;
                            orderedMoves[i + 1].Value.MoveOrder -= 1;
                        }
                    }

                    var col = GUI.color;
                    if (kvp.Key == m_SelectedKey)
                        GUI.color = Color.green;
                    if (GUILayout.Button(kvp.Key))
                    {
                        m_SelectedKey = kvp.Key;
                        m_CurrentStep = 0;
                        // This resets the gui focus so we don't get old data remaining in text fields
                        GUIUtility.keyboardControl = 0;
                        Debug.Log("Setting key to : " + m_SelectedKey);
                    }

                    GUI.color = Color.red;
                    if (GUILayout.Button("X", GUILayout.MaxWidth(30f), GUILayout.MinWidth(30f)))
					{
                        removeKey = kvp.Key;
					}
                    GUI.color = col;

                    GUILayout.EndHorizontal();
                }
            }

            if (removeKey != "")
            {
                m_Data.Remove(removeKey);

                if (!m_Data.ContainsKey(removeKey))
                {
                    // We aren't using .net 3.5 so we can't use the .First function :(
                    var enumerator = m_Data.Keys.GetEnumerator();
                    if (enumerator.MoveNext())
                    {
                        m_SelectedKey = enumerator.Current;
                        m_CurrentStep = 0;
                        // This resets the gui focus so we don't get old data remaining in text fields
                        GUIUtility.keyboardControl = 0;
                    }
                }
            }

            EditorGUILayout.Space();
            m_NewKey = XmlCheckedTextField("New Move Name", m_NewKey);
            if (GUILayout.Button("Add New Move") && m_NewKey != null && m_NewKey != "")
            {
                m_Data.Add(m_NewKey, new MoveMetadata());
                m_Data[m_NewKey].AddNewEmptyDataAtEnd();
				m_Data[m_NewKey].AnimationName = m_NewKey;
                m_NewKey = "";
            }

            EditorGUILayout.Space();

            if (GUILayout.Button("Save Moves"))
            {
                SaveMoves();
            }

            GUILayout.EndScrollView();

            EditorGUILayout.EndVertical(); // End Left scroll window

            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(ColTwoMaxWidth)); // Step Selection section

            GUILayout.Label("Steps");

            m_ScrollPos2 = GUILayout.BeginScrollView(m_ScrollPos2);
            int removeIndex = -1;
            if (m_Data != null && m_Data.ContainsKey(m_SelectedKey) && m_Data[m_SelectedKey] != null && m_Data[m_SelectedKey].Nodes != null)
            {
                for (int i = 0; i < m_Data[m_SelectedKey].Nodes.Length; i++)
                {
                    EditorGUILayout.BeginHorizontal();

                    var col = GUI.color;

                    if (m_CurrentStep == i)
                        GUI.color = Color.green;
					if (GUILayout.Button((i + 1).ToString() + " (" + m_Data[m_SelectedKey].TriggerTimes[i] + ")", GUILayout.MaxWidth(100f)) )
                    {
                        m_CurrentStep = i;
                        // This resets the gui focus so we don't get old data remaining in text fields
                        GUIUtility.keyboardControl = 0;
                    }
                    GUI.color = Color.red;
					if (GUILayout.Button("X", GUILayout.MaxWidth(20f)) )
                    {
                        removeIndex = i;
                    }
                    GUI.color = col;
                    EditorGUILayout.EndHorizontal();
                }
            }

            if (removeIndex > -1)
            {
                m_Data[m_SelectedKey].RemoveDataAtIndex(removeIndex);
                // This resets the gui focus so we don't get old data remaining in text fields
                GUIUtility.keyboardControl = 0;
            }

            EditorGUILayout.Space();

            if (GUILayout.Button("Sort"))
            {
                m_Data[m_SelectedKey].SortData();
            }

            if (GUILayout.Button("+"))
            {
                m_Data[m_SelectedKey].AddNewEmptyDataAtEnd();
            }

            GUILayout.EndScrollView();

            EditorGUILayout.EndVertical(); // End Step Selection Section

            EditorGUILayout.BeginVertical(); // Main Data Section

            var data = m_Data[m_SelectedKey];

            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Move : " + m_SelectedKey + " Step : " + (m_CurrentStep + 1).ToString());

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            data.Position = EditorGUILayout.Vector3Field("Position", data.Position);
            data.Rotation = EditorGUILayout.Vector3Field("Rotation", data.Rotation);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            var cam = data.CamLowMidHighPos;
            cam.x = EditorGUILayout.FloatField("Camera Low", cam.x);
            cam.y = EditorGUILayout.FloatField("Camera Mid", cam.y);
            cam.z = EditorGUILayout.FloatField("Camera High", cam.z);
            data.CamLowMidHighPos = cam;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            cam = data.CamTargetLowMidHighPos;
            cam.x = EditorGUILayout.FloatField("Camera Target Low", cam.x);
            cam.y = EditorGUILayout.FloatField("Camera Target Mid", cam.y);
            cam.z = EditorGUILayout.FloatField("Camera Target High", cam.z);
            data.CamTargetLowMidHighPos = cam;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            data.IAPID = XmlCheckedTextField("iOS InApp Purchase ID", data.IAPID);
            data.AndroidIAPID = XmlCheckedTextField("Android InApp Purchase ID", data.AndroidIAPID);

            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(); // Move names line

			GUILayout.Label("English Name", GUILayout.MaxWidth(80f));
			data.EnglishName = XmlCheckedTextArea(data.EnglishName, GUILayout.MaxWidth(200f) );

			GUILayout.Label("French Name", GUILayout.MaxWidth(80f));
			data.FrenchName = XmlCheckedTextArea(data.FrenchName, GUILayout.MaxWidth(200f) );

			GUILayout.Label("Spanish Name", GUILayout.MaxWidth(80f));
			data.SpanishName = XmlCheckedTextArea(data.SpanishName, GUILayout.MaxWidth(200f) );

            GUILayout.EndHorizontal(); // End move names line

            m_ScrollPos3 = GUILayout.BeginScrollView(m_ScrollPos3);

            GUILayout.BeginHorizontal(); // Nodes line
            GUILayout.Label("Nodes");

            removeIndex = -1;
            if (data.Nodes[m_CurrentStep].Count > 0)
            {
                for (int i = 0; i < data.Nodes[m_CurrentStep].Count; i++)
                {
                    data.Nodes[m_CurrentStep][i] = (CurrentNodeType)EditorGUILayout.EnumPopup(data.Nodes[m_CurrentStep][i]);
                    var col = GUI.color;
                    GUI.color = Color.red;
                    if (GUILayout.Button("X"))
                        removeIndex = i;
                    GUI.color = col;
                    EditorGUILayout.Space();
                }
            }

            if (GUILayout.Button("+"))
                data.AddNodeForStep(m_CurrentStep);

            if (removeIndex > -1)
                data.RemoveNodeForStepAtIndex(m_CurrentStep, removeIndex);

            GUILayout.EndHorizontal(); // End Nodes Line

            GUILayout.BeginHorizontal(); // Grips Line

            GUILayout.Label("Grips");

            removeIndex = -1;
            if (data.Grips.Length > 0)
            {
                for (int i = 0; i < data.Grips[m_CurrentStep].Count; i++)
                {
                    data.Grips[m_CurrentStep][i] = (GripType)EditorGUILayout.EnumPopup(data.Grips[m_CurrentStep][i]);
                    var col = GUI.color;
                    GUI.color = Color.red;
                    if (GUILayout.Button("X"))
                        removeIndex = i;
                    GUI.color = col;
                    EditorGUILayout.Space();
                }
            }

            if (GUILayout.Button("+"))
                data.AddGripForStep(m_CurrentStep);

            if (removeIndex > -1)
                data.RemoveGripForStepAtIndex(m_CurrentStep, removeIndex);

            GUILayout.EndHorizontal(); // End Grips Line

            data.TriggerTimes[m_CurrentStep] = Mathf.Clamp01(EditorGUILayout.FloatField("Normalized Time", data.TriggerTimes[m_CurrentStep]));

            GUILayout.BeginHorizontal();
            GUILayout.Label("English Description/s");
            if(GUILayout.Button("Add New English Description"))
                data.AddEnglishDescriptionForStep(m_CurrentStep);
            GUILayout.EndHorizontal();

            removeIndex = -1;
            for (int i = 0; i < data.EnglishInfo[m_CurrentStep].Count; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label((i + 1).ToString(), GUILayout.MaxWidth(40f));
                data.EnglishInfo[m_CurrentStep][i] = XmlCheckedTextArea(data.EnglishInfo[m_CurrentStep][i], GUILayout.MaxWidth(m_BottomTextWidth));
                var col = GUI.color;
                GUI.color = Color.red;
                if (GUILayout.Button("X", GUILayout.MaxWidth(40f)))
                    removeIndex = i;
                GUI.color = col;
                GUILayout.EndHorizontal();
            }
            if (removeIndex > -1)
                data.RemoveEnglishDescriptionForStepAtIndex(m_CurrentStep, removeIndex);


            GUILayout.BeginHorizontal();
            GUILayout.Label("French Description/s");
            if(GUILayout.Button("Add New French Description"))
                data.AddFrenchDescriptionForStep(m_CurrentStep);
            GUILayout.EndHorizontal();

            removeIndex = -1;
            for (int i = 0; i < data.FrenchInfo[m_CurrentStep].Count; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label((i + 1).ToString(), GUILayout.MaxWidth(40f));
                data.FrenchInfo[m_CurrentStep][i] = XmlCheckedTextArea(data.FrenchInfo[m_CurrentStep][i], GUILayout.MaxWidth(m_BottomTextWidth));
                var col = GUI.color;
                GUI.color = Color.red;
                if (GUILayout.Button("X", GUILayout.MaxWidth(40f)))
                    removeIndex = i;
                GUI.color = col;
                GUILayout.EndHorizontal();
            }
            if (removeIndex > -1)
                data.RemoveFrenchDescriptionForStepAtIndex(m_CurrentStep, removeIndex);

            GUILayout.BeginHorizontal();
            GUILayout.Label("Spanish Description/s");
            if(GUILayout.Button("Add New Spanish Description"))
                data.AddSpanishDescriptionForStep(m_CurrentStep);
            GUILayout.EndHorizontal();

            removeIndex = -1;
            for (int i = 0; i < data.SpanishInfo[m_CurrentStep].Count; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label((i + 1).ToString(), GUILayout.MaxWidth(40f));
                data.SpanishInfo[m_CurrentStep][i] = XmlCheckedTextArea(data.SpanishInfo[m_CurrentStep][i], GUILayout.MaxWidth(m_BottomTextWidth));
                var col = GUI.color;
                GUI.color = Color.red;
                if (GUILayout.Button("X", GUILayout.MaxWidth(40f)))
                    removeIndex = i;
                GUI.color = col;
                GUILayout.EndHorizontal();
            }
            if (removeIndex > -1)
                data.RemoveSpanishDescriptionForStepAtIndex(m_CurrentStep, removeIndex);

            GUILayout.Space(50f);
            // Tips and general section
            GUILayout.BeginHorizontal();
            GUILayout.Label("English General Info", GUILayout.MaxWidth(120f));
            data.EnglishOverviewInfo = XmlCheckedTextArea(data.EnglishOverviewInfo, GUILayout.MaxWidth(m_BottomTextWidth));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
			GUILayout.Label("French General Info", GUILayout.MaxWidth(120f));
            data.FrenchOverviewInfo = XmlCheckedTextArea(data.FrenchOverviewInfo, GUILayout.MaxWidth(m_BottomTextWidth));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
			GUILayout.Label("Spanish General Info", GUILayout.MaxWidth(120f));
            data.SpanishOverviewInfo = XmlCheckedTextArea(data.SpanishOverviewInfo, GUILayout.MaxWidth(m_BottomTextWidth));
            GUILayout.EndHorizontal();

            GUILayout.Space(50f);

            GUILayout.BeginHorizontal();
			GUILayout.Label("English Tips", GUILayout.MaxWidth(120f));
            data.EnglishTipsInfo = XmlCheckedTextArea(data.EnglishTipsInfo, GUILayout.MaxWidth(m_BottomTextWidth));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
			GUILayout.Label("French Tips", GUILayout.MaxWidth(120f));
            data.FrenchTipsInfo = XmlCheckedTextArea(data.FrenchTipsInfo, GUILayout.MaxWidth(m_BottomTextWidth));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
			GUILayout.Label("Spanish Tips", GUILayout.MaxWidth(120f));
            data.SpanishTipsInfo = XmlCheckedTextArea(data.SpanishTipsInfo, GUILayout.MaxWidth(m_BottomTextWidth));
            GUILayout.EndHorizontal();

            GUILayout.EndScrollView();

            EditorGUILayout.EndVertical(); // End Main Data Section.

            EditorGUILayout.EndHorizontal();// End Initial Horizontal
        } 
        private float m_BottomTextWidth = 600f;

        private static string XmlCheckedTextField(string label, string text, params GUILayoutOption[] options)
        {
            EditorGUI.BeginChangeCheck();
            var returnText = EditorGUILayout.TextField(label, text, options);
            if (EditorGUI.EndChangeCheck())
            {
                bool didStrip;
                returnText = StripInvalidXMLCharacters(returnText, out didStrip);
                if (didStrip)
                {
                    Debug.LogWarning("Character was stripped from XML String.\nOriginal : " + text + "\nStripped : " + returnText);
                }
            }
            return returnText;
        }

        private static string XmlCheckedTextArea(string text, params GUILayoutOption[] options)
        {
            EditorGUI.BeginChangeCheck();
            var returnText = EditorGUILayout.TextArea(text, options);
            if (EditorGUI.EndChangeCheck())
            {
                bool didStrip;
                returnText = StripInvalidXMLCharacters(returnText, out didStrip);
                if (didStrip)
                {
                    Debug.LogWarning("Character was stripped from XML String.\nOriginal : " + text + "\nStripped : " + returnText);
                }
            }
            return returnText;
        }

        private static StringBuilder textOut = new StringBuilder();

        private static string StripInvalidXMLCharacters(string textIn, out bool didStripChar)
        {
            textOut.Length = 0;
            char current;
            didStripChar = false;

            if (textIn == null || textIn == string.Empty) return string.Empty;
            for (int i = 0; i < textIn.Length; i++)
            {
                current = textIn[i];
                if ((current == 0x9 || current == 0xA || current == 0xD) ||
                    ((current >= 0x20) && (current <= 0xD7FF)) ||
                    ((current >= 0xE000) && (current < 0xFFFD)) ||
                    ((current >= 0x1000) && (current <= 0x10FFFF)))
                {
                    textOut.Append(current);
                }
                else
                {
                    didStripChar = true;
                }
            }
            return textOut.ToString();
        }
    }


}
