using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using MotionEdge.Language;
using System.IO;
using System.Xml.Serialization;

public class LanguageEditorWindow : EditorWindow
{
    private static LanguageEditorWindow m_Window;

    [MenuItem("Window/Language Editor")]
    public static void Open()
    {
        if (m_Window == null)
            m_Window = EditorWindow.GetWindow<LanguageEditorWindow>();
        m_Window.Show();
    }

    private bool m_IsInit = false;
    private Dictionary<string, LanguageUISet> m_LanguageSets;
    private string m_FolderPath;
	private Vector2 m_ScrollPos;

    private void Init()
    {
        m_FolderPath = Path.Combine(Application.dataPath, "Resources");
        m_FolderPath = Path.Combine(m_FolderPath, "LanguageUISets");

        m_LanguageSets = new Dictionary<string, LanguageUISet>();

        InitFile(LanguageController.EnglishFileName);
        InitFile(LanguageController.FrenchFileName);
        InitFile(LanguageController.SpanishFileName);
        InitFile(LanguageController.PortugeseFileName);

        m_SelectedKey = LanguageController.EnglishFileName;

        m_IsInit = true;
    }

    private void InitFile(string fileName)
    {
        var filePath = Path.Combine(m_FolderPath, fileName);
        if (File.Exists(filePath))
        {
            var serializer = new XmlSerializer(typeof(LanguageUISet));
            
            using (var textReader = new StreamReader(filePath))
            {
                LanguageUISet language = (LanguageUISet)serializer.Deserialize(textReader);
                m_LanguageSets.Add(fileName, language);
                textReader.Close();
            }
        }
        else
        {
            m_LanguageSets.Add(fileName, new LanguageUISet());
        }
    }

    private void SaveFile(string key)
    {
        var serializer = new XmlSerializer(typeof(LanguageUISet));
        
        using (var textWriter = new StreamWriter(Path.Combine(m_FolderPath, key)))
        {
            serializer.Serialize(textWriter, m_LanguageSets[key]);
            textWriter.Close();
        }
    }

    private string m_SelectedKey;
    private string m_NewKey;
    private Dictionary<string, string> m_UpdateValues = new Dictionary<string, string>();

    private void OnGUI()
    {
        if (!m_IsInit)
            Init();

        EditorGUILayout.BeginHorizontal();
        DrawLanguageButton(LanguageController.EnglishFileName);
        DrawLanguageButton(LanguageController.FrenchFileName);
        DrawLanguageButton(LanguageController.SpanishFileName);
        DrawLanguageButton(LanguageController.PortugeseFileName);
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Save"))
        {
            SaveFile(LanguageController.EnglishFileName);
            SaveFile(LanguageController.FrenchFileName);
            SaveFile(LanguageController.SpanishFileName);
            SaveFile(LanguageController.PortugeseFileName);
            AssetDatabase.Refresh();
        }

        // Quick hack
        if (m_LanguageSets == null)
            Init();

        var lang = m_LanguageSets[m_SelectedKey];
        string removeKey = null;
		
		m_ScrollPos = EditorGUILayout.BeginScrollView(m_ScrollPos);
        foreach (var kvp in lang.KeyValues)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(kvp.Key);
            var val = EditorGUILayout.TextArea(kvp.Value, GUILayout.MaxWidth(600f));
            if (val != kvp.Value)
            {
                m_UpdateValues.Add(kvp.Key, val);
            }

            if (GUILayout.Button("-"))
            {
                removeKey = kvp.Key;
                // This resets the gui focus so we don't get old data remaining in text fields
                GUIUtility.keyboardControl = 0;
            }
            EditorGUILayout.EndHorizontal();
        }

        foreach (var kvp in m_UpdateValues)
        {
            lang.KeyValues[kvp.Key] = kvp.Value;
        }

        m_UpdateValues.Clear();

        if (removeKey != null)
            RemoveKey(removeKey);

        EditorGUILayout.BeginHorizontal();
        m_NewKey = EditorGUILayout.TextArea(m_NewKey);
        if (GUILayout.Button("Add Key") && m_NewKey != "")
        {
            AddKey(m_NewKey);
            m_NewKey = "";
            // This resets the gui focus so we don't get old data remaining in text fields
            GUIUtility.keyboardControl = 0;
        }
        EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndScrollView();
    }

    private void AddKey(string key)
    {
        m_LanguageSets[LanguageController.EnglishFileName].KeyValues.Add(key, "");
        m_LanguageSets[LanguageController.FrenchFileName].KeyValues.Add(key, "");
        m_LanguageSets[LanguageController.SpanishFileName].KeyValues.Add(key, "");
        m_LanguageSets[LanguageController.PortugeseFileName].KeyValues.Add(key, "");
    }

    private void RemoveKey(string key)
    {
        m_LanguageSets[LanguageController.EnglishFileName].KeyValues.Remove(key);
        m_LanguageSets[LanguageController.FrenchFileName].KeyValues.Remove(key);
        m_LanguageSets[LanguageController.SpanishFileName].KeyValues.Remove(key);
        m_LanguageSets[LanguageController.PortugeseFileName].KeyValues.Remove(key);
    }

    private bool DrawLanguageButton(string fileName)
    {
        var col = GUI.color;
        if (m_SelectedKey == fileName)
            GUI.color = Color.red;
        var splits = fileName.Split('.');
        if (GUILayout.Button(splits[0]))
        {
            m_SelectedKey = fileName;
            GUI.color = col;
            // This resets the gui focus so we don't get old data remaining in text fields
            GUIUtility.keyboardControl = 0;
            return true;
        }
        GUI.color = col;
        return false;
    }
}
