﻿using System.Collections.Generic;
using UnityEngine;

namespace MotionEdge.Metadata
{
    public enum CurrentNodeType
    {
        None,
        RightFoot,
        LeftFoot,
        RightKnee,
        LeftKnee,
        RightHand,
        LeftHand,
		RightElbow,
		LeftElbow,
		RightShoulder,
        LeftShoulder,
        Head,
        Hips,
        Ball,
        Body,
        Racket,
		RacketTop,
		RacketBase
    }

    public enum GripType
    {
        None,
        Continental,
        EasternForehand,
        SemiWesternForehand,
        WesternForehand,
        EasternBackhand,
        ExtremeEastern,
        SemiWesternBackhand,
        TwoHandedBackhand
    }

    public class MoveMetadata
    {
		public string AnimationName;
        public string EnglishName;
        public string FrenchName;
        public string SpanishName;
        public string EnglishOverviewInfo;
        public string FrenchOverviewInfo;
        public string SpanishOverviewInfo;
        public string EnglishTipsInfo;
        public string FrenchTipsInfo;
        public string SpanishTipsInfo;
        public List<CurrentNodeType>[] Nodes;
        public List<GripType>[] Grips;
        public List<string>[] EnglishInfo;
        public List<string>[] FrenchInfo;
        public List<string>[] SpanishInfo;
        public float[] TriggerTimes;
        public string IAPID;
        public string AndroidIAPID;
        public int MoveOrder;
        public Vector3 Position;
        public Vector3 Rotation;
        public Vector3 CamLowMidHighPos;
        public Vector3 CamTargetLowMidHighPos;

        public MoveMetadata()
        {
			AnimationName = "";
            EnglishName = "";
            FrenchName = "";
            SpanishName = "";
            EnglishOverviewInfo = "";
            FrenchOverviewInfo = "";
            SpanishOverviewInfo = "";
            EnglishTipsInfo = "";
            FrenchTipsInfo = "";
            SpanishTipsInfo = "";
            Nodes = new List<CurrentNodeType>[0];
            Grips = new List<GripType>[0];
            EnglishInfo = new List<string>[0];
            FrenchInfo = new List<string>[0];
            SpanishInfo = new List<string>[0];
            TriggerTimes = new float[0];
            IAPID = "";
            AndroidIAPID = "";
            MoveOrder = -1;
            Position = Vector3.zero;
            Rotation = Vector3.zero;
            // low = x, mid = y, high = z
            CamLowMidHighPos = new Vector3(0.2f, 2.1f, 4f);
            CamTargetLowMidHighPos = new Vector3(0.9f, 1.1f, 1.2f);
        }

        
#if UNITY_EDITOR // We don't need all this code at runtime.

        public void RemoveDataAtIndex(int index)
        {
            int newCount = Nodes.Length - 1;
            var oldNodes = Nodes;
            var oldGrips = Grips;
            var oldEnglish = EnglishInfo;
            var oldFrench = FrenchInfo;
            var oldSpanish = SpanishInfo;
            var oldTimes = TriggerTimes;
            Nodes = new List<CurrentNodeType>[newCount];
            Grips = new List<GripType>[newCount];
            EnglishInfo = new List<string>[newCount];
            FrenchInfo = new List<string>[newCount];
            SpanishInfo = new List<string>[newCount];
            TriggerTimes = new float[newCount];

            bool skipped = false;
            for (int i = 0; i < oldEnglish.Length; i++)
            {
                if (i == index)
                {
                    skipped = true;
                    continue;
                }
                Nodes[skipped ? i - 1 : i] = oldNodes[i];
                Grips[skipped ? i - 1 : i] = oldGrips[i];
                EnglishInfo[skipped ? i - 1 : i] = oldEnglish[i];
                FrenchInfo[skipped ? i - 1 : i] = oldFrench[i];
                SpanishInfo[skipped ? i - 1 : i] = oldSpanish[i];
                TriggerTimes[skipped ? i - 1 : i] = oldTimes[i];
            }
        }

        public void AddNewEmptyDataAtEnd()
        {
            int newCount = Nodes.Length + 1;
            var oldNodes = Nodes;
            var oldGrips = Grips;
            var oldEnglish = EnglishInfo;
            var oldFrench = FrenchInfo;
            var oldSpanish = SpanishInfo;
            var oldTimes = TriggerTimes;
            Nodes = new List<CurrentNodeType>[newCount];
            Grips = new List<GripType>[newCount];
            EnglishInfo = new List<string>[newCount];
            FrenchInfo = new List<string>[newCount];
            SpanishInfo = new List<string>[newCount];
            TriggerTimes = new float[newCount];

            for (int i = 0; i < oldEnglish.Length; i++)
            {
                Nodes[i] = oldNodes[i];
                Grips[i] = oldGrips[i];
                EnglishInfo[i] = oldEnglish[i];
                FrenchInfo[i] = oldFrench[i];
                SpanishInfo[i] = oldSpanish[i];
                TriggerTimes[i] = oldTimes[i];
            }
            Nodes[newCount - 1] = new List<CurrentNodeType>();
            Grips[newCount - 1] = new List<GripType>();
            EnglishInfo[newCount - 1] = new List<string>();
            EnglishInfo[newCount - 1].Add("");
            FrenchInfo[newCount - 1] = new List<string>();
            FrenchInfo[newCount - 1].Add("");
            SpanishInfo[newCount - 1] = new List<string>();
            SpanishInfo[newCount - 1].Add("");
            TriggerTimes[newCount - 1] = 1f;
        }

        public void AddNodeForStep(int stepIndex)
        {
            Nodes[stepIndex].Add(CurrentNodeType.None);
        }

        public void RemoveNodeForStepAtIndex(int stepIndex, int nodeIndex)
        {
            Nodes[stepIndex].RemoveAt(nodeIndex);
        }

        public void AddGripForStep(int stepIndex)
        {
            Grips[stepIndex].Add(GripType.None);
        }

        public void RemoveGripForStepAtIndex(int stepIndex, int nodeIndex)
        {
            Grips[stepIndex].RemoveAt(nodeIndex);
        }

        public void AddEnglishDescriptionForStep(int stepIndex)
        {
            EnglishInfo[stepIndex].Add("");
        }

        public void RemoveEnglishDescriptionForStepAtIndex(int stepIndex, int nodeIndex)
        {
            EnglishInfo[stepIndex].RemoveAt(nodeIndex);
        }

        public void AddFrenchDescriptionForStep(int stepIndex)
        {
            FrenchInfo[stepIndex].Add("");
        }

        public void RemoveFrenchDescriptionForStepAtIndex(int stepIndex, int nodeIndex)
        {
            FrenchInfo[stepIndex].RemoveAt(nodeIndex);
        }

        public void AddSpanishDescriptionForStep(int stepIndex)
        {
            SpanishInfo[stepIndex].Add("");
        }

        public void RemoveSpanishDescriptionForStepAtIndex(int stepIndex, int nodeIndex)
        {
            SpanishInfo[stepIndex].RemoveAt(nodeIndex);
        }

        public void SortData()
        {
            for (int i = 0; i < TriggerTimes.Length; i++)
            {
                var j1 = i;
                var j2 = i + 1;
                if (j2 >= TriggerTimes.Length)
                    continue;

                while(j1 >= 0 && TriggerTimes[j1] > TriggerTimes[j2])
                {
                    SwitchIndices(j1, j2);
                    j1--;
                    j2--;
                }
            }
        }

        private void SwitchIndices(int i1, int i2)
        {
            var nodes = Nodes[i1];
            var grips = Grips[i1];
            var eng = EnglishInfo[i1];
            var fre = FrenchInfo[i1];
            var spa = SpanishInfo[i1];
            var time = TriggerTimes[i1];

            Nodes[i1] = Nodes[i2];
            Grips[i1] = Grips[i2];
            EnglishInfo[i1] = EnglishInfo[i2];
            FrenchInfo[i1] = EnglishInfo[i2];
            SpanishInfo[i1] = SpanishInfo[i2];
            TriggerTimes[i1] = TriggerTimes[i2];

            Nodes[i2] = nodes;
            Grips[i2] = grips;
            EnglishInfo[i2] = eng;
            FrenchInfo[i2] = fre;
            SpanishInfo[i2] = spa;
            TriggerTimes[i2] = time;
        }
#endif
    }
}
