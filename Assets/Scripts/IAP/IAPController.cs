﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using TransTech.System.Debug;

public class IAPController : MonoSingleton<IAPController>
{
    private const string AndroidSKU = "tennis_test_purchase_1";
    private const string iOSSKU = "com.motionedgeacademy.tennisedgeacademy.unlockalltennismoves";
    public string SKU
    {
        get
        {
            if (Application.platform == RuntimePlatform.Android)
                return AndroidSKU;
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
                return iOSSKU;
            else
                return "No Valid SKU for this platform";
        }
    }

    public string LocalPrice { get; private set; }

    private string m_CurrentPurchaseSKU;
    private const string m_PurchaseKey = "Purchases";
    private const string m_PurchaseSalt = "NHDOi40hasd";

    private bool m_BillingChecked = false;
    public bool BillingChecked
    {
        get
        {
            return m_BillingChecked;
        }
        private set 
        {
            if (m_BillingChecked != value)
            {
                m_BillingChecked = value;
                if (BillingCheckedChangedEvent != null)
                    BillingCheckedChangedEvent();
            }
        }
    }
    public event Action BillingCheckedChangedEvent;

    private bool m_BillingSupported = false;
    public bool BillingSupported
    {
        get
        {
            return m_BillingSupported;
        }
        private set
        {
            if (m_BillingSupported != value)
            {
                m_BillingSupported = value;
                if (BillingSupportedChangedEvent != null)
                    BillingSupportedChangedEvent();
            }
        }
    }
    public event Action BillingSupportedChangedEvent;

    private bool m_ProductDataFetched = false;
    public bool ProductDataFetched
    {
        get
        {
            return m_ProductDataFetched;
        }
        private set
        {
            if (m_ProductDataFetched != value)
            {
                m_ProductDataFetched = value;
                if (ProductDataFetchedChangedEvent != null)
                    ProductDataFetchedChangedEvent();
            }
        }
    }
    public event Action ProductDataFetchedChangedEvent;

    private bool m_ProductDataAvailable = false;
    public bool ProductDataAvailable
    {
        get { return m_ProductDataAvailable; }
        private set
        {
            if (m_ProductDataAvailable != value)
            {
                m_ProductDataAvailable = value;
                if (ProductDataAvailableChangedEvent != null)
                    ProductDataAvailableChangedEvent();
            }
        }
    }
    public event Action ProductDataAvailableChangedEvent;


    public event Action BillingStatusEvent;
    public event Action ProductDataEvent;
    public event Action<bool> PurchaseEvent;
    public event Action RestoreCompletedEvent;
	
	public override void Init()
	{
        base.Init();
        LocalPrice = "Unavailable";
#if UNITY_ANDROID
        if (!BillingChecked && !BillingSupported)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                // IAP init with Android license keys
                IAP.init("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoP6F+QkBQ4y/y6MGuuBbLGYwRJRpsKbX3bm/jaRUNKXP/frdisSZcI7A1HE63x92HU8cy9JShQ0CSb4b4sKk85+D8bLc6cyVBiCyoqehGoVWrHKrd6Go3zlr8DPA1e0xZ44v7iC+ukFGoODXaHDwpfPwxfU8OpUUPZIV8Z+qAR6+pDZzv9zMDbauA98EJ6uNgVuw0BCg4H8cVwGP4+KVlA+D6qZIIZb+pVRN+WhzkOGwS8RaGhBtqfcWdJkLIaq92ncio6rUSWir3g1ZTxfWjcZtqWGqWkHgkhLbrI6i5nUXwm5bLJW7e1JoyU2QlO2BPjCqriBXPyiCb7KR34VFpwIDAQAB");

                GoogleIABManager.billingNotSupportedEvent += BillingNotSupportedEventFired;
                GoogleIABManager.billingSupportedEvent += BillingSupportedEventFired;
            }
            else
            {
                BillingChecked = true;
                BillingSupported = false;
            }
        }

#elif UNITY_IPHONE
		BillingSupported = StoreKitBinding.canMakePayments();
        BillingChecked = true;
		// On ios, we just hook up to the successful purchase event
		StoreKitManager.purchaseSuccessfulEvent += iOSPurchaseSuccessful;
#endif
	}
	
	void BillingSupportedEventFired()
	{
		Debug.Log( "IAPTest : Billing is supported!" );
        BillingChecked = true;
        BillingSupported = true;
        if (BillingStatusEvent != null)
            BillingStatusEvent();
        //IAP.requestProductData(new string[] { }, new string[] { AndroidSKU }, ProductListFetchCompleted);
	}

    public void GetProductData()
    {
        if (!BillingSupported)
        {
            Debug.Log("Billing isn't supported, not getting product data."); 
            return;
        }
        // If we have successfully fetched data, return.
        if (ProductDataFetched && ProductDataAvailable)
        {
            Debug.Log("Data was already available");
            return;
        }
        ProductDataFetched = false;
        ProductDataAvailable = false;
        Debug.Log("Performing Data Request");
		// Billing is only supported on iOS and Android
#if UNITY_IPHONE || UNITY_ANDROID
        IAP.requestProductData(new string[] { iOSSKU }, new string[] { AndroidSKU }, ProductListFetchCompleted);
#endif
    }

    public void PurchaseAllMoves()
    {
#if UNITY_ANDROID
        if(Application.platform == RuntimePlatform.Android)
            PurchaseItem(AndroidSKU);
        else
            TTDebug.LogWarning("IAP not available on this platform. Cannot purchase all moves");
#elif UNITY_IPHONE
        if(Application.platform == RuntimePlatform.IPhonePlayer)            
			PurchaseItem(iOSSKU);
        else
            TTDebug.LogWarning("IAP not available on this platform. Cannot purchase all moves");
#else
        TTDebug.LogWarning("IAP not available on this platform. Cannot purchase all moves");
#endif
    }

    private void PurchaseItem(string productID)
    {
        if (!BillingSupported)
            return;
        m_CurrentPurchaseSKU = productID;
		// IAP only works on iOS and Android
#if UNITY_IPHONE || UNITY_ANDROID
        IAP.purchaseNonconsumableProduct(productID, PurchaseCompleted);
#endif
    }

    public void RestorePurchases()
    {
        
#if UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            if (!BillingSupported)
                return;
            GoogleIABManager.queryInventoryFailedEvent += QueryGoogleInventoryFailed;
            GoogleIABManager.queryInventorySucceededEvent += QueryGoogleInventorySucceeded;
            GoogleIAB.queryInventory(new string[] { AndroidSKU });
        }
        else
            TTDebug.LogWarning("IAP not available on this platform.  Cannot restore purchases.");
#elif UNITY_IPHONE
        if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
            //IAP.restoreCompletedTransactions(RestorePurchasesiOSFailed);
			StoreKitManager.restoreTransactionsFailedEvent += RestorePurchasesiOSFailed;
			StoreKitManager.restoreTransactionsFinishedEvent += RestorePurchasesiOSCompleted;
			StoreKitBinding.restoreCompletedTransactions();
		}
        else
            TTDebug.LogWarning("IAP not available on this platform.  Cannot restore purchases.");
#else
        TTDebug.LogWarning("IAP not available on this platform.  Cannot restore purchases.");
#endif
    }
	
	void BillingNotSupportedEventFired( string s )
	{
		Debug.Log( "IAPTest : Billing is NOT supported : " + s );
        BillingSupported = false;
        BillingChecked = true;
        if (BillingStatusEvent != null)
            BillingStatusEvent();
	}

#if UNITY_IPHONE || UNITY_ANDROID
    void ProductListFetchCompleted(List<IAPProduct> products)
    {
        if (products == null)
        {
            Debug.LogError("No products returned!");
            ProductDataAvailable = false;
            ProductDataFetched = true;
        }
        else
        {
            foreach (var p in products)
            {
                if (p.productId == iOSSKU || p.productId == AndroidSKU)
                {
                    LocalPrice = p.price;
                }
            }
            ProductDataAvailable = true;
            ProductDataFetched = true;
        }
        if (ProductDataEvent != null)
            ProductDataEvent();
    }
#endif

    void PurchaseCompleted(bool b)
    {
        if (b)
        {
            AddKeysToPurchases(new string[] { m_CurrentPurchaseSKU });
            m_CurrentPurchaseSKU = "";
            Debug.Log("Purchase completed!");
        }
        else
        {
            Debug.LogError("Purchase was not successful : " + m_CurrentPurchaseSKU);
        }

        if (PurchaseEvent != null)
            PurchaseEvent(b);
    }

#if UNITY_IPHONE
    private void RestorePurchasesiOSFailed(string msg)
    {
		StoreKitManager.restoreTransactionsFailedEvent -= RestorePurchasesiOSFailed;
		
        Debug.LogError("Restore iOS Purchases failed : " + msg);

        if (RestoreCompletedEvent != null)
            RestoreCompletedEvent();
    }

	
	private void RestorePurchasesiOSCompleted()
	{
		StoreKitManager.restoreTransactionsFinishedEvent -= RestorePurchasesiOSCompleted;
		
		if (RestoreCompletedEvent != null)
            RestoreCompletedEvent();
	}
#endif

#if UNITY_IPHONE
	private void iOSPurchaseSuccessful(StoreKitTransaction transaction)
	{
		AddKeysToPurchases(new string[] { transaction.productIdentifier });
		
		if (PurchaseEvent != null)
            PurchaseEvent(true);
	} 
#endif

#if UNITY_ANDROID
    private void QueryGoogleInventoryFailed(string error)
    {
        Debug.LogError("Query Inventory Failed : " + error);

        GoogleIABManager.queryInventoryFailedEvent -= QueryGoogleInventoryFailed;
        GoogleIABManager.queryInventorySucceededEvent -= QueryGoogleInventorySucceeded;

        if (RestoreCompletedEvent != null)
            RestoreCompletedEvent();
    }

    private void QueryGoogleInventorySucceeded(List<GooglePurchase> purchases, List<GoogleSkuInfo> skuInfos)
    {
        var products = new List<string>();
        foreach (var p in purchases)
        {
            if(p.purchaseState == GooglePurchase.GooglePurchaseState.Purchased)
                products.Add(p.productId);
        }

        AddKeysToPurchases(products.ToArray());

        GoogleIABManager.queryInventoryFailedEvent -= QueryGoogleInventoryFailed;
        GoogleIABManager.queryInventorySucceededEvent -= QueryGoogleInventorySucceeded;

        if (RestoreCompletedEvent != null)
            RestoreCompletedEvent();
    }
#endif

    private void AddKeysToPurchases(string[] keys)
    {
        // Get the existing keys unlocked
        var combinedKeys = GetDecryptedKeysFromPrefs();
        foreach (var k in keys)
        {
            if (k != "" && !combinedKeys.Contains(k))
            {
                if (combinedKeys.Length > 0)
                    combinedKeys += "|";
                combinedKeys += k;
            }
        }
        EncryptAndStoreKeys(combinedKeys);
    }

    private string GetDecryptedKeysFromPrefs()
    {
        var encryptedKeys = PlayerPrefs.GetString(m_PurchaseKey, "");
        var combinedKeys = "";

        if (encryptedKeys.Length > 0)
        {
            combinedKeys = Encryption.Decrypt(encryptedKeys, GetPassword());
        }
        return combinedKeys;
    }

    private void EncryptAndStoreKeys(string combinedKeys)
    {
        // Re-encrypt and store
        var encryptedKeys = Encryption.Encrypt(combinedKeys, GetPassword());
        PlayerPrefs.SetString(m_PurchaseKey, encryptedKeys);
    }

    private string GetPassword()
    {
        var id = SystemInfo.deviceUniqueIdentifier;
        var pw = id + m_PurchaseSalt;
        if (pw.Length < 16)
        {
            while (pw.Length < 16)
                pw = pw + "F";
        }
        else if (pw.Length > 16)
        {
            pw = pw.Substring(0, 16);
        }
        return pw;
    }

    public string[] GetPurchases()
    {
        var combinedKeys = GetDecryptedKeysFromPrefs();
        return combinedKeys.Split('|');
    }
}