﻿namespace TransTech.FiniteStateMachine
{
    public abstract class FSMState : IFSMState
    {
        public virtual void Enter(params object[] args)
        {
        }

        public virtual void Update(float deltaTime)
        {
        }

        public virtual void UnfocusedUpdate(float deltaTime)
        {
        }

        public virtual void Exit()
        {
        }

        public virtual void LostFocus()
        {
        }

        public virtual void GainedFocus()
        {
        }
    }
}