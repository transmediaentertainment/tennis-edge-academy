using UnityEngine;

namespace TransTech.Delegates.Unity
{
    public delegate void Vector3Delegate(Vector3 vec);

    public delegate void Vector2Delegate(Vector2 vec);

    public delegate void TouchDelegate(Touch touch);

    public delegate void GameObjectDelegate(GameObject go);
}